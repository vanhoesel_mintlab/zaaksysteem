#!/usr/bin/perl

use Moose;
use Data::Dumper;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw/ConfigLoader/;

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;

my $log = Catalyst::Log->new();

error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar @ARGV >= 3;

my ($dsn, $user, $password, $commit) = @ARGV;
my $dbic = database($dsn, $user, $password);

$dbic->txn_do(sub {
    eval {
        fill_triggers($dbic);
        die "rollback" unless $commit;
    };
});

$log->info('All done.');

sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn => $dsn,
            pg_enable_utf8 => 1,
            user => $user,
            password => $password
        }
    );
    
    Catalyst::Model::DBIC::Schema->new->schema;
}

sub error {
    $log->error(shift);
    $log->_flush;

    exit;
}

sub fill_triggers {
    my $dbic = shift;

    my $notes = $dbic->resultset('BetrokkeneNotes');
    my $event = $dbic->resultset('Logging');

    while(my $note = $notes->next) {
        $log->info(sprintf('Creating trigger for note %d from %s', $note->id, $note->betrokkene_from));
        $log->_flush;

        unless($note->betrokkene_type && $note->betrokkene_exid) {
            $log->error('   Lacking enough data to import (missing subject\'s type or exid)');
            next;
        }

        my $event = $event->trigger('subject/note/create', {
            component => 'betrokkene',
            created_for => sprintf('betrokkene-%s-%d', $note->betrokkene_type, $note->betrokkene_exid),
            created_by => $note->betrokkene_from,
            data => {
                content => $note->message
            }
        });

        # Fix created timestamp, can't override on trigger.
        $event->created($note->created);
        $event->update;
    }
}
