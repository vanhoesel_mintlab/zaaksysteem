import navigate from './../../../functions/common/navigate';
import {
    getAboutValue
} from './../../../functions/intern/caseView/caseMenu';
import {
    getWidgetCases,
    getIntakeIconType,
    getWidgetCasetypes,
    filterWidget,
    setWidgetMaxResults,
    navigateWidgetPages,
    addFavoriteCasetype,
    removeFavoriteCasetype,
    startFavoriteCasetype,
    refuseCase,
    acceptCase
} from './../../../functions/intern/dashboard';

describe('when viewing the dashboard', () => {

    let intake = $('[data-name="intake"]');

    beforeAll(() => {

        navigate.as('dashboardfull');

    });

    it('there should be an intake widget with case 44 and case 74', () => {

        let widgetTitle = 'Zaakintake',
            expectedResults = ['74', '44'];

        expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

    });

    it('there should be a my open cases widget with case 45', () => {

        let widgetTitle = 'Mijn openstaande zaken',
            expectedResults = ['45'];

        expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

    });

    it('there should be a personal search widget with case 46', () => {

        let widgetTitle = 'Dashboard',
            expectedResults = ['46'];

        expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

    });

    it('there should be a favorite casetype widget with favorite casetypes', () => {

        let expectedResults = ['Dashboard', 'Dashboard Delete'];

        expect(getWidgetCasetypes()).toEqual(expectedResults);

    });

    it('the case assigned for the person should have a personal icon', () => {

        expect(getIntakeIconType('74')).toEqual('account');

    });

    it('the case assigned for the department should have a department icon', () => {

        expect(getIntakeIconType('44')).toEqual('account-multiple');

    });

    describe('when text filtering the items in the intake widget', () => {
    
        beforeAll(() => {
        
            filterWidget('Zaakintake', 'department');
        
        });
    
        it('the intake widget should only contain case 44', () => {

            let widgetTitle = 'Zaakintake',
                expectedResults = ['44'];

            expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

        });
    
        afterAll(() => {
        
            intake.$('.mdi-close-circle').click();
        
        });
    
    });

    describe('when changing the items per page of the intake widget', () => {
    
        beforeAll(() => {
        
            setWidgetMaxResults('Zaakintake', '1');
        
        });
    
        it('only the first case should still be displayed', () => {

            let widgetTitle = 'Zaakintake',
                expectedResults = ['74'];

            expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

        });

        describe('and when navigating to the next page', () => {
        
            beforeAll(() => {
            
                navigateWidgetPages('Zaakintake', 'next');
            
            });
        
            it('only the last case should be displayed', () => {

                let widgetTitle = 'Zaakintake',
                    expectedResults = ['44'];

                expect(getWidgetCases(widgetTitle)).toEqual(expectedResults);

            });
        
        });

        afterAll(() => {
        
            setWidgetMaxResults('Zaakintake', '10');
        
        });
    
    });

    describe('and when adding an item to the favorite casetype', () => {
    
        beforeAll(() => {
        
            addFavoriteCasetype('Dashboard Add');
        
        });
    
        it('the added item should be present', () => {

            let expectedResults = ['Dashboard', 'Dashboard Delete', 'Dashboard Add'];

            expect(getWidgetCasetypes()).toEqual(expectedResults);

        });
    
        afterAll(() => {
        
            removeFavoriteCasetype('Dashboard Add');
        
        });
    
    });

    describe('and when removing an item from the favorite casetype', () => {
    
        beforeAll(() => {
        
            removeFavoriteCasetype('Dashboard Delete');
        
        });
    
        it('the removed item should not be present', () => {

            let expectedResults = ['Dashboard'];

            expect(getWidgetCasetypes()).toEqual(expectedResults);

        });
    
        afterAll(() => {
        
            addFavoriteCasetype('Dashboard Delete');
        
        });
    
    });

    // a testscenario for changing the order of favorite casetypes should be included here

    // this was postponed because I was unable to figure out how to drag and drop with the 'Dragula' implementation
    // when dragging is initiated (by mousedown and moving), a duplicate mirror element is created
    // this mirror element is then moved around to be dropped somewhere
    // it is, as far as I can tell at this point in time, not possible to let protractor switch from the origin element to the mirror element mid-drag
    // I've tried to execute the drag event seperately, to at least test the end-result, but was unable to get this working

    describe('and when starting a casetype from the favorite casetypes', () => {
    
        beforeAll(() => {
        
            startFavoriteCasetype('Dashboard Delete');
        
        });
    
        it('the new case modal should be opened', () => {

            expect($('create-case-registration').isPresent()).toBe(true);

        });

        it('the casetype should be prefilled with the started favorite casetype', () => {

            expect($('create-case-registration [data-name="casetype"] .value-item').getText()).toEqual('Dashboard Delete');

        });
    
        afterAll(() => {
        
            $('zs-contextual-action-form .contextual-action-form-close .mdi-close').click();
        
        });
    
    });

    describe('and when refusing a case from the intake and inspecting the case', () => {
    
        beforeAll(() => {
        
            refuseCase('74');

            navigate.as('dashboardfull', 74);
        
        });
    
        it('the case should be assigned to the department of refused cases', () => {

            expect(getAboutValue('Afdeling')).toEqual('Refused cases');

        });

        afterAll(() => {

            navigate.to();

        });
    
    });

    describe('and when accepting a case from the intake', () => {
    
        beforeAll(() => {
        
            acceptCase('44');
        
        });

        it('the caseview for that case should be opened', () => {

            expect(browser.getCurrentUrl()).toContain('intern/zaak/44');

        });
    
        it('the assignee of the case should be the current user', () => {

            expect(getAboutValue('Behandelaar')).toEqual('Dashboard Full');

        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
