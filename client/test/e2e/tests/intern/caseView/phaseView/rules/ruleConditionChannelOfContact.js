import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const testData = [
    {
        channelOfContact: 'behandelaar',
        caseNumber: 164
    },
    {
        channelOfContact: 'balie',
        caseNumber: 165
    },
    {
        channelOfContact: 'telefoon',
        caseNumber: 166
    },
    {
        channelOfContact: 'post',
        caseNumber: 167
    },
    {
        channelOfContact: 'email',
        caseNumber: 168
    },
    {
        channelOfContact: 'webformulier',
        caseNumber: 169
    },
    {
        channelOfContact: 'sociale media',
        caseNumber: 170
    }
];

for ( const index in testData ) {

    describe(`when opening case ${testData[index].caseNumber} with contactchannel ${testData[index].channelOfContact}`, () => {

        beforeAll(() => {

            navigate.as('admin', testData[index].caseNumber);

            openPhase('1');

            choice.$('[value="Ja"]').click();

        });

        for ( const attribute in testData ) {

            const dataName = testData[attribute].channelOfContact.replace(' ', '_');
            const state = testData[index].channelOfContact === testData[attribute].channelOfContact ? 'True' : 'False';

            it(`the attribute for ${testData[attribute].channelOfContact} should be ${state}`, () => {

                expect(caseAttribute.getClosedValue($(`[data-name="contactkanaal_${dataName}"]`))).toEqual(state);

            });

        }


    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
