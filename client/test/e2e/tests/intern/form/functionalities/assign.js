import navigate from './../../../../functions/common/navigate';
import waitForElement from './../../../../functions/common/waitForElement';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    openRegisteredCase,
    assign
} from './../../../../functions/common/form';
import {
    getSummaryValue,
    getAboutValue
} from './../../../../functions/intern/caseView/caseMenu';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';

const testData = [
    {
        type: 'without assignment options',
        caseType: 'Toewijzing bij registratie geen opties',
        performAssignment: () => {
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'In behandeling nemen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice'
        }
    },
    {
        type: 'to myself',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('me');
            goNext();
        },
        results: {
            behandelaar: 'A. admin',
            coordinator: 'admin',
            status: 'In behandeling',
            afdeling: 'Backoffice'
        }
    },

    // ZS-15771
    // {
    //     type: 'to a coworker',
    //     caseType: 'Toewijzing bij registratie alle opties',
    //     performAssignment: () => {
    //         assign('coworker', { assignee: 'Toewijzing wijzigen' });
    //         goNext();
    //         openRegisteredCase();
    //     },
    //     results: {
    //         behandelaar: 'T. Wijzigen',
    //         coordinator: '-',
    //         status: 'Nieuw',
    //         afdeling: 'Backoffice'
    //     }
    // },

    // ZS-15772
    // {
    //     type: 'to a coworker and changing department',
    //     caseType: 'Toewijzing bij registratie alle opties',
    //     performAssignment: () => {
    //         assign('coworker', { assignee: 'Toewijzing wijzigen', changeDepartment: true });
    //         goNext();
    //         openRegisteredCase();
    //     },
    //     results: {
    //         behandelaar: 'T. Wijzigen',
    //         coordinator: '-',
    //         status: 'Nieuw',
    //         afdeling: 'Zaakacties'
    //     }
    // },
    {
        type: 'to a department',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('coworker', { assignee: 'Toewijzing wijzigen', sendEmail: true });
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice',
            emailed: true
        }
    }
];

for ( const index in testData ) {

    describe(`when starting a registration form and assigning ${testData[index].type}`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: testData[index].caseType,
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            goNext();

            testData[index].performAssignment();

        });

        it('the case should not have an assignee', () => {

            expect(getSummaryValue('Behandelaar')).toEqual(testData[index].results.behandelaar);

        });

        it('the case should not have a coordinator', () => {

            expect(getAboutValue('Coordinator')).toEqual(testData[index].results.coordinator);

        });

        it('the case should have the status "Nieuw"', () => {

            expect(getSummaryValue('Status')).toEqual(testData[index].results.status);

        });

        it('the department should not have changed', () => {

            expect(getSummaryValue('Afdeling')).toEqual(testData[index].results.afdeling);

        });

        if ( testData[index].results.emailed ) {

            it('the assignee should have been emailed', () => {

                browser.ignoreSynchronization = true;

                openTab('timeline');

                waitForElement('[data-event-type="email/send"]');

                expect($('[data-event-type="email/send"]').isPresent()).toBe(testData[index].results.emailed);

                browser.get('/intern/');

                browser.ignoreSynchronization = false;

            });

        }

    });
    
}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
