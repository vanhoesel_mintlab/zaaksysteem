package Zaaksysteem::View::API::v1;

use Moose;

use Zaaksysteem::API::v1::Serializer;
use BTTW::Tools;

BEGIN { extends 'Catalyst::View' }

=head1 NAME

Zaaksysteem::View::API::v1 - Seperate view class for API v1 infrastructure

=head1 SYNOPSIS

    sub my_action : Private {
        my ($self, $c) = @_;

        $c->stash->{ result } = ...;

        $c->detach($c->view('API::v1'));
    }

=head1 DESCRIPTION

This view handles generating a view for API v1 actions.

It uses the L<Zaaksysteem::API::v1::Serializer> class to serialize the
contents of the C<results> in L<Catalyst/c-stash>.

=head1 METHODS

=head2 process

Implements the interface required by L<Catalyst> for processing a view.

=cut

sub _is_valid_serialized_object {
    my $self   = shift;
    my $object = shift;

    return 1 if (ref $object eq 'HASH' && $object->{type} && $object->{instance});
    return;
}

sub process {
    my $self = shift;
    my $c = shift;

    my $serializer = Zaaksysteem::API::v1::Serializer->new(
        'has_full_access'       => ($c->stash->{has_full_access} || 0),
        'v1_serializer_options' => ($c->stash->{v1_serializer_options} || {}),
    );

    if ($c->stash->{file_download}) {
        return ;
    }

    my $data;

    if ($self->_is_valid_serialized_object($c->stash->{result})) {
        $data = $c->stash->{result};
    } else {
        $data = try {
            return $serializer->read(
                $c->stash->{ result },
                $c->stash->{ serializer_opts }
            );
        } catch {
            $c->log->info(sprintf("Error '%s' while reading", $_));

            $c->log->debug(dump_terse($c->stash->{result}))
                if ($c->log->is_debug);

            $c->res->status(500);

            # Handcrafted exception, for all we know, it's an exception
            # object that's causing the fault.
            return {
                type => 'exception',
                instance => {
                    type => 'api/v1/serializer/read_error',
                    message => 'Serialization error.'
                }
            };
        };
    }

    my $request_id = $c->stash->{ request_id };

    # In-the-wild configurations have 'development' as well as 'dev' values
    my $development = ($c->config->{ otap } =~ m[^dev]) ? \1 : \0;

    my $response = {
        request_id => $request_id,
        development => $development,
        api_version => 1,
        status_code => $c->res->status,
        result => $data
    };

    my $result = try { $serializer->encode($response) } catch {
        $c->log->error($_);
        $c->res->status(500);

        my $dev = ${ $development } ? 'true' : 'false';

        # Handcrafted exception JSON serialization, for all we know, it's the
        # actual encoder that's causing the fault.
        return <<"RESULT";
{
    "request_id": "$request_id",
    "development": $dev,
    "api_version": 1,
    "status_code": 500,
    "result": {
        "type": "exception",
        "instance": {
            "type": "api/v1/serializer/encode_error",
            "message": "Serialization error."
        }
    }
}
RESULT
    };

    $c->res->content_type('application/json; charset=utf-8');
    $c->res->body($result);

    if (my $transaction = $c->stash->{request_event}) {
        my $status = $c->res->status;
        my $success = ($status =~ /^2/) ? "Yes" : "No";

        my $input = $transaction->input_data;
        $input =~ s/^Success: UNKNOWN$/Success: $success ($status)/m;
        $transaction->update({ input_data => $input });

        my $record = $transaction->transaction_records->search->first;
        $record->update(
            {
                input  => $input,
                output => $result,
            }
        );
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
