import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from './../../shared/appService';
import keyBy from 'lodash/keyBy';
import every from 'lodash/every';
import find from 'lodash/find';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';
import get from 'lodash/get';
import sortBy from 'lodash/sortBy';
import shortid from 'shortid';
import getAttributes from '../../shared/getAttributes';
import nGramFilter from '../../shared/nGramFilter';
import getCaseValueFromAttribute from '../../shared/getCaseValueFromAttribute';
import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.meetingListView', [
			resourceModule,
			composedReducerModule,
			appServiceModule,
			angularUiRouterModule
		])
		.directive('meetingListView', [ '$state', '$stateParams', '$window', 'resource', 'composedReducer', 'appService', ( $state, $stateParams, $window, resource, composedReducer, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposalResource: '&',
					meetingResource: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						filteredProposalReducer,
						meetingReducer,
						sortedMeetingReducer,
						itemsCollapsedState = {},
						appConfigReducer,
						totalProposalsReducer;

					appConfigReducer = composedReducer( { scope: $scope }, ctrl.appConfig )
						.reduce( config => config);

					// Builds the filter suggestion index
					ctrl.proposalResource().onUpdate(( data ) => {
						nGramFilter.addToIndex( data || [] );
					});

					let mappings = {
						date: 'vergaderingdatum',
						time: 'vergaderingtijd',
						label: 'vergaderingtitel',
						location: 'vergaderinglocatie',
						chairman: 'vergaderingvoorzitter'
					};

					sortedMeetingReducer = composedReducer({ scope: $scope }, ctrl.meetingResource(), appConfigReducer, ( ) => $state.params.meetingType)
						.reduce(( meetings, config, meetingType ) => {

							let attributes = getAttributes(config).vergadering,
								sortedAttributes,
								sortAttributeDate,
								sortAttributeTime;
							
							sortAttributeDate = find(attributes, ( n ) => {
								return n.external_name === 'vergaderingdatum';
							});

							sortAttributeTime = find(attributes, ( n ) => {
								return n.external_name === 'vergaderingtijd';
							});

							if (!sortAttributeDate) {
								return meetings;
							}

							sortedAttributes = sortBy(meetings, ( meeting ) => {
								let date = meeting.instance.attributes[sortAttributeDate.internal_name.searchable_object_id],
									time,
									match,
									hours;

									if (sortAttributeTime) {
										time = meeting.instance.attributes[sortAttributeTime.internal_name.searchable_object_id] || '00:00';
									} else {
										time = '00:00';
									}

									match = time.match(/(\d{1,2})(:|\.)?(\d{2})/);
									hours = match ? Number(match[1]) + (Number(match[3]) / 60) : 0;

								return new Date(date).getTime() + (hours * 60 * 60 * 1000);
							});

							if (meetingType === 'archief') {
								sortedAttributes = sortedAttributes.reverse();
							}

							return sortedAttributes;

						});

					totalProposalsReducer = composedReducer({ scope: $scope }, ctrl.proposalResource, ( ) => $stateParams )
						.reduce( ( proposals, stateParams ) => {
							if ( proposals.totalRows() <= stateParams.paging ) {
								return false;
							}
							return true;
						});

					ctrl.isMoreAvailable = totalProposalsReducer.data;

					filteredProposalReducer = composedReducer({ scope: $scope }, ctrl.proposalResource(), ( ) => appService.state().filters )
						.reduce( ( proposals, filters ) => {

							let attributesToIndex = nGramFilter.getAttributesToIndex();

							return filters.length ?
								proposals.filter(
									proposal => {
										return filters.every( filter => {
											return attributesToIndex.map( attr => getCaseValueFromAttribute(attr, proposal))
												.some(val => val.indexOf(filter) !== -1);
										});
									}
								)
								: proposals;
						});

					meetingReducer = composedReducer({ scope: $scope }, sortedMeetingReducer, filteredProposalReducer, appConfigReducer)
						.reduce(( meetings, filteredProposals, config ) => {

							let proposalsById =
								keyBy(
									filteredProposals,
									'reference'
								),
								groups =
									meetings.map( ( meeting ) => {

										let meetingObj = assign(
											{
												$id: shortid(),
												id: meeting.reference,
												casenumber: meeting.instance.number,
												children:
													meeting.instance.relations.instance.rows
														.filter(proposal => proposalsById[proposal.reference])
														.map( ( proposal, index ) => {
															return proposalsById[proposal.reference].merge({ instance: { attributes: { $index: index + 1 } } }, { deep: true });
														})
											},
											meeting,
											mapValues(mappings, ( value ) => {

												let attributeName =
													get(
														find(getAttributes( config ).all, { external_name: value }),
														'internal_name.searchable_object_id'
													);

												return meeting.instance.attributes[attributeName];
													
											})
										);

										return meetingObj;

									});

							return groups;

						});

					ctrl.getMeetings = meetingReducer.data;

					ctrl.handleToggle = ( id ) => {

						let isStateExpanded = appService.state().expanded,
							allCollapsed;

						itemsCollapsedState[id] = ctrl.isExpanded(id);

						allCollapsed = every(meetingReducer.data(), ( meeting ) => !ctrl.isExpanded(meeting.id));

						if (isStateExpanded && allCollapsed) {
							appService.dispatch('toggle_expand');
						}
					};

					ctrl.isExpanded = ( id ) => {
						return itemsCollapsedState.hasOwnProperty(id) ? !itemsCollapsedState[id] : !!appService.state().expanded;
					};

					ctrl.isGrouped = ( ) => !!($state.current.name === 'meetingList');

					// If items are expanded, fold them in on this view
					if (appService.state().expanded) {
						appService.dispatch('toggle_expand');
					}

					ctrl.isLoading = ( ) => meetingReducer.state() === 'pending';

					ctrl.loadMore = ( ) => {
						$state.go($state.current.name, { paging: $stateParams.paging + 50 });
					};

					$scope.$on(
						'$destroy',
						appService.on('toggle_expand', ( ) => {

							itemsCollapsedState = {};

						}, 'expanded')
					);

				}],
				controllerAs: 'meetingListView'
			};

		}
		])
		.name;
