import navigate from './../../../../functions/common/navigate';
import verifyRestricted from './../../../../functions/common/auth/verifyRestricted';

describe('when logging in as behandelaar', () => {

    beforeAll(() => {

        navigate.as('behandelaar');

    });

    describe('and opening the catalog', () => {

        it('should show the no access page', () => {

            expect(verifyRestricted('/beheer/bibliotheek')).toBe(true);

        });

    });

    describe('and opening a case', () => {

        beforeAll(() => {

            navigate.to(24);

        });

        it('should not have the rule mode button present', () => {

            expect($('[data-name="debug_rules"]').isPresent()).toBe(false);

        });

    });

});

describe('when logging in as zaaktypebeheerder', () => {

    beforeAll(() => {

        navigate.as('zaaktypebeheerder');

    });

    describe('and opening the catalog', () => {

        it('should not show the no access page', () => {

            expect(verifyRestricted('/beheer/bibliotheek')).toBe(false);

        });

    });

    describe('and opening a case', () => {

        beforeAll(() => {

            navigate.to(24);

        });

        it('should have the rule mode button present', () => {

            expect($('[data-name="debug_rules"]').isPresent()).toBe(true);

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
