import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../../api/resource/composedReducer';
import './styles.scss';

export default
	angular.module('zsNotificationListItem', [
		composedReducerModule
	])
		.directive('zsNotificationListItem', [
			'$animate', 'composedReducer', ( $animate, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
                    notification: '&',
					onArchive: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						notificationReducer = composedReducer( { scope }, ctrl.notification )
							.reduce( notification => notification);
						
					ctrl.getLinkLabel = ( ) => notificationReducer.data().link.label;

					ctrl.getNotificationIcon = ( ) => notificationReducer.data().icon;

					ctrl.isLink = ( ) => !!(notificationReducer.data().link);

					ctrl.getCaseLink = ( ) => notificationReducer.data().link.url;

					ctrl.getNotificationTitle = ( ) => notificationReducer.data().title;

					ctrl.getMessageSender = ( ) => notificationReducer.data().message.sender;

					ctrl.getMessageContent = ( ) => notificationReducer.data().message.content;

					ctrl.getTimestamp = ( ) => notificationReducer.data().timestamp;

					ctrl.archiveNotification = ( event ) => {

						$animate.addClass(element, 'removing')
							.finally(( ) => {
								ctrl.onArchive({ $id: notificationReducer.data().id });
							});

						event.stopPropagation();
						event.preventDefault();
					};


				}],
				controllerAs: 'vm'
			};

		}])
		.name;
