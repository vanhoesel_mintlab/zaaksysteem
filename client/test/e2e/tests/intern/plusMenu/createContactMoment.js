import navigate from './../../../functions/common/navigate';
import waitForElement from './../../../functions/common/waitForElement';
import {
    createContactMoment
} from './../../../functions/intern/plusMenu';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';

describe('when creating a contact moment', () => {

    let newContactMoment = {
        type: 'natuurlijk_persoon',
        bsn: '123456789',
        case: '43',
        message: 'this is my contact moment'
    };

    beforeAll(() => {

        navigate.as();

        createContactMoment(newContactMoment);

    });

    it('should display a success message', () => {

        expect($('.snack-message-content').getText()).toContain(newContactMoment.case);

    });

    describe('and when opening the timeline of the case', () => {

        beforeAll(() => {

            navigate.as('admin', `/intern/zaak/${(newContactMoment.case)}`);

            waitForElement('.case-view');

            browser.ignoreSynchronization = true;

            openTab('timeline');

            waitForElement('[data-event-type="subject/contactmoment/create"]');

        });

        it('the contact moment should be present', () => {

            expect($('[data-event-type="subject/contactmoment/create"]').isPresent()).toBe(true);

        });

        it('the contact moment should contain the message', () => {

            let contactMomentMessage = $('[data-event-type="subject/contactmoment/create"] .timeline-item-content div:nth-child(2) pre');

            expect(contactMomentMessage.getText()).toEqual(newContactMoment.message);

        });

        afterAll(() => {

            browser.ignoreSynchronization = false;

            navigate.to();

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
