package Zaaksysteem::AppointmentProvider::Roles::TestConnection;
use Moose::Role;

requires qw(
    _test_host_port
    _test_certificate
);

use BTTW::Tools;
use BTTW::Tools::Tests qw(:all);

=head1 NAME

Zaaksysteem::AppointmentProvider::Roles::TestConnection - "test_connection" role for appointment providers

=head1 DESCRIPTION

This role implements a simple "test_connection" method for L<Zaaksysteem::AppointmentProvider> plugins.

=head1 REQUIRED METHODS

=head2 _test_host_port

Return a two-element array, with the host and port to test.

=head2 _test_certificate

Return a hash with at least the key C<ca>, containing the path to the CA
certificate bundle to use. If the key is missing or empty, the system CA path
will be used.

=cut

sig _test_host_port => '=> Str, Str';

sig _test_certificate => '=> HashRef';

=head1 METHODS

=head2 test_connection

Do a basic connection test to the configure host/port and checking the
configured CA certificate.

=cut

sub test_connection {
    my ($self) = @_;
    
    my ($host, $port) = $self->_test_host_port();
    my %certificates = $self->_test_certificate();

    {
        my $error = test_plain_connect($host, $port);
        if ($error) {
            throw(
                'appointmentprovider/connection_failed',
                "Fout bij verbinden: '$error'\nIs de firewall juist ingesteld?"
            );
        }
    }

    {
        my $error = test_secure_connect(
            $host,
            $port,
            $certificates{ca},
        );

        if ($error) {
            throw(
                'appointmentprovider/secure_connection_failed',
                sprintf(
                    "Fout bij het maken van een beveiligde verbinding: '%s'".
                    "\nControleer of het CA-certificaat klopt en of de server beveiligde ".
                    "verbindingen accepteert.",
                    $error
                ),
            );
        }
    }

    return;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
