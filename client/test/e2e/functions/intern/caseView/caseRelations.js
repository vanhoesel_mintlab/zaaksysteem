export const subjectRelations = element.all(by.css('.related_subjects .table-row'));

export const getContactRole = contactName => new Promise(resolve => {
    subjectRelations
        .filter(relation =>
            relation
                .$('[column-id="name"]')
                .getText()
                .then(name =>
                    name === contactName
                )
        ).then(filteredElements =>
            filteredElements[0]
                .$('[column-id="role"]')
                .getText()
                .then(role =>
                    resolve(role)
                )
        );
    });

export const getContactAuthorisation = contactName => new Promise(resolve => {
    subjectRelations
        .filter(relation =>
                relation.$('[column-id="name"]').getText().then(name =>
                    name === contactName
            )
        ).then(filteredElements =>
            filteredElements[0]
                .$('[column-id="pip_authorized"] .mdi-check')
                .isPresent()
                .then(presence =>
                    resolve(presence)
                )
        );
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
