import executeRule from '.';
import { difference, keys } from 'lodash';
import evaluateFormula from './evaluateFormula';

describe('executeRule', () => {
	test('should return an object with the keys hidden, disabled, values', () => {
		const rule = {
			conditions: {
				conditions: []
			},
			then: [],
			else: []
		};
		const result = executeRule(rule, {
			values: {}
		});

		expect(
			difference(
				['hidden', 'disabled', 'values'],
				keys(result)
			).length
		).toBe(0);
	});

	describe('when rule matches', () => {
		test('should execute the "then" actions', () => {
			const rule = {
				conditions: {
					conditions: []
				},
				then: [
					{
						data: {
							attribute_name: 'foo',
							value: 'foo'
						},
						type: 'set_value'
					}
				],
				else: [
					{
						data: {
							attribute_name: 'bar',
							value: 'bar'
						},
						type: 'set_value'
					}
				]
			};
			const result = executeRule(rule, {
				values: {}
			});

			expect(result.values.foo).toBe('foo');
			expect(result.values.bar).not.toBe('bar');
		});
	});

	describe('when rule doesn\'t match', () => {
		test('should execute the "else" actions', () => {
			const rule = {
				conditions: {
					conditions: [
						{
							attribute_name: 'foo',
							validation_type: 'immediate',
							values: ['foo']
						}
					],
					type: 'and'
				},
				then: [
					{
						data: {
							attribute_name: 'foo',
							value: 'foo'
						},
						type: 'set_value'
					}
				],
				else: [
					{
						data: {
							attribute_name: 'bar',
							value: 'bar'
						},
						type: 'set_value'
					}
				]
			};
			const result = executeRule(rule, {
				values: {}
			});

			expect(result.values.foo).not.toBe('foo');
			expect(result.values.bar).toBe('bar');
		});
	});

	describe('when executing actions', () => {
		const createRuleWithAction = action => ({
			conditions: {
				conditions: [],
				type: 'and'
			},
			then: [action]
		});

		describe('of type set_value', () => {
			describe('and can_change is truthy', () => {
				const rule = createRuleWithAction({
					type: 'set_value',
					data: {
						can_change: true,
						attribute_name: 'foo',
						value: 'foo'
					}
				});

				test('should set the value only when it is empty', () => {
					let result = executeRule(rule, {
						values: {
							foo: 'bar'
						}
					});

					expect(result.values.foo).toBe('bar');
					expect(result.disabled.foo).toBeFalsy();

					result = executeRule(rule, {
						values: {
							foo: ''
						}
					});

					expect(result.values.foo).toBe('');
					expect(result.disabled.foo).toBeFalsy();
				});
			});

			describe('and can_change is falsy', () => {
				test('should set the value and disabled to true', () => {
					const rule = createRuleWithAction({
						type: 'set_value',
						data: {
							can_change: false,
							attribute_name: 'foo',
							value: 'foo'
						}
					});
					const result = executeRule(rule, {
						values: {
							foo: 'bar'
						}
					});

					expect(result.values.foo).toBe('foo');
					expect(result.disabled.foo).toBeTruthy();
				});
			});
		});

		describe('of type hide_attribute', () => {
			test('should hide the attribute', () => {
				const rule = createRuleWithAction({
					type: 'hide_attribute',
					data: {
						attribute_name: 'foo'
					}
				});
				const result = executeRule(rule, {
					values: {}
				});

				expect(result.hidden.foo).toBe(true);
			});
		});

		describe('of type show_attribute', () => {
			test('should unhide the attribute', () => {
				const rule = createRuleWithAction({
					type: 'show_attribute',
					data: {
						attribute_name: 'foo'
					}
				});
				const result = executeRule(rule, {
					values: {}
				});

				expect(result.hidden.foo).toBeFalsy();
			});
		});

		describe('of type hide_group', () => {
			test('should hide all the related attributes', () => {
				const rule = createRuleWithAction({
					type: 'hide_group',
					data: {
						attribute_name: 1,
						related_attributes: ['foo', 'bar']
					}
				});
				const result = executeRule(rule, {
					values: {}
				});

				expect(result.hidden.foo).toBe(true);
				expect(result.hidden.bar).toBe(true);
			});
		});

		describe('of type show_group', () => {
			test('should show all the related attributes', () => {
				const rule = createRuleWithAction({
					type: 'show_group',
					data: {
						attribute_name: 1,
						related_attributes: ['foo', 'bar']
					}
				});
				const result = executeRule(rule, {
					values: {}
				});

				expect(result.hidden.foo).toBeFalsy();
				expect(result.hidden.bar).toBeFalsy();
			});
		});

		describe('of type set_value_formula', () => {
			test('should set the value of the attribute to the result of the formula and disable the attribute', () => {
				const formula = '3 * 5';
				const rule = createRuleWithAction({
					type: 'set_value_formula',
					data: {
						attribute_name: 'foo',
						formula
					}
				});
				const result = executeRule(rule, {
					values: {}
				});

				expect(result.values.foo).toBe(evaluateFormula(formula, {}));
				expect(result.disabled.foo).toBe(true);
			});
		});
	});
});
