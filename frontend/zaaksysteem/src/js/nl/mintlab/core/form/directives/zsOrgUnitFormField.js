/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsOrgUnitFormField', [ 'organisationalUnitService', function ( organisationalUnitService )  { 
			
			return {
				require: 'ngModel',
				link: function ( scope, element, attrs, ngModel ) {
					
					function getModelArray ( ) {
						return ngModel.$modelValue ? ngModel.$modelValue.concat() : [];
					}
					
					ngModel.$isEmpty = function ( value ) {
						return value && value.length > 0;	
					};
					
					scope.addOrgUnit = function ( ) {
						organisationalUnitService.getUnits().then(function ( ) {
							var array = getModelArray(),
								defaultOrgUnitId = organisationalUnitService.getDefaultOrgUnitId(),
								defaultRoleId = organisationalUnitService.getDefaultRoleIdForOrgUnitId(defaultOrgUnitId);
							
							array.push({
								orgUnit: {
									org_unit_id: defaultOrgUnitId,
									role_id: defaultRoleId
								}
							});
							
							ngModel.$setViewValue(array);	
						});
					};
					
					scope.removeOrgUnit = function ( item ) {
						var array = getModelArray();
							
						_.pull(array, item);
						
						ngModel.$setViewValue(array);
					};
					
					scope.isLimitReached = function ( limit ) {
						var reached = false;
						if(limit !== undefined) {
							reached = (ngModel.$modelValue ? ngModel.$modelValue.length : 0) >= limit;
						}
						return reached;
					};
					
					scope.$watch('scope[field.name]', function ( ) {
						ngModel.$setViewValue(getModelArray());
					}, true);
				}
			};
			
		}]);
	
})();