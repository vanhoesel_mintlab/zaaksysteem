package Zaaksysteem::Scheduler::Job::OLOSync;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Scheduler::Job::OLOSync - Job runner for OmgevingsLoket Online
sync/poll events

=head1 DESCRIPTION

This scheduled job triggers the broadcast of an event on the message bus, to
be picked up by the C<zaaksysteem-olo-service> daemon, which should poll for
new messages.

For implementation details see
L<Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket/broadcast_poll_event>.

=cut

use DateTime;

=head1 METHODS

=head2 run

Delegate for the C<broadcast_poll_event> trigger.

Also updates the C<last_poll_event> timestamp field in the associated
interface.

=cut

sub run {
    my $self = shift;
    my $c = shift;

    my $interface = $c->model('DB::Interface')->find($self->interface_id);

    $interface->process_trigger('broadcast_poll_event', {
        queue_model => $c->model('ZSQueue'),
        instance_hostname => $c->uri_for('/')->host,
        olo_service_host => $c->config->{ olo_service_host } || $ENV{ OLO_SERVICE_HOST }
    });

    my $config = $interface->get_interface_config;

    $config->{ last_sync } = DateTime->now->iso8601 . 'Z';

    $interface->update_interface_config($config);

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
