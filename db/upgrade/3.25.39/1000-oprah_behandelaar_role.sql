BEGIN;

-- Adds systemrole 'Behandelaar' for every subject which doesn't have one yet

UPDATE subject
SET role_ids = role_ids || (
	SELECT id
	FROM roles
	WHERE name = 'Behandelaar' AND system_role = true
)
WHERE id IN (
	SELECT id
	FROM subject
	WHERE NOT role_ids @> array_append(ARRAY[]::integer[], (
		SELECT id
		FROM roles
		WHERE name = 'Behandelaar' AND system_role = true
	))
);

COMMIT;
