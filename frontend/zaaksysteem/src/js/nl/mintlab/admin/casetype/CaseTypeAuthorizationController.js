/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.casetype')
		.controller('nl.mintlab.admin.casetype.CaseTypeAuthorizationController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			$scope.rightGroups = [
				{
					id: 'non_confidential',
					label: 'Openbaar/Intern'
				},
				{
					id: 'confidential',
					label: 'Vertrouwelijk'
				}
			];
			
			$scope.rightTypes = [
				{
					id: 'zaak_beheer',
					label: 'Mag zaken beheren'
				},
				{
					id: 'zaak_edit',
					label: 'Mag zaken behandelen'
				},
				{
					id: 'zaak_read',
					label: 'Mag zaken raadplegen'
				},
                {
                    id: 'zaak_search',
                    label: 'Mag zaken zoeken'
                }
			];
			
			function getUnits ( ) {
				$scope.loading = true;
				
				smartHttp.connect({
					method: 'GET',
					url: '/api/authorization/org_unit',
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						$scope.units = response.result;
					})
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error'
						});
					})
					['finally'](function ( ) {
						$scope.loading = false;
					});
			}
			
			function getRights ( ) {
				return smartHttp.connect({
					method: 'GET',
					url: '/api/casetype/authorization/get/' + $scope.casetypeId,
					params: {
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						$scope.rights = parseRights(response.result);
						if(!$scope.rights) {
							createRights();
						}
					})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error'
						});
					});
			}
			
			function createRights ( ) {
				$scope.rights = {
					'confidential': [],
					'non_confidential': []
				};
			}
			
			function parseRights ( rights ) {
				var parsed = {};
				
				_.each(rights, function ( right ) {
					var key = right.confidential === 1 ? 'confidential' : 'non_confidential',
						typeRights = parsed[key],
						role;
						
					if(!typeRights) {
						parsed[key] = typeRights = [];
					}
					
					role = _.find(typeRights, function ( r ) {
						return r.org_unit_id === right.ou_id && r.role_id === right.role_id;
					});
					
					if(!role) {
						role = {
							org_unit_id: right.ou_id,
							role_id: right.role_id
						};
						typeRights.push(role);
					}
					
					role[right.recht] = true;
					
				});
				
				return parsed;
			}
			
			function serializeRights ( ) {
				var rights = [];
				
				_.each($scope.rights, function ( rightsGroup, type ) {
					var confidential = type === 'confidential' ? 1 : 0;
					_.each(rightsGroup, function ( role ) {
						_.each(role, function ( value, key ) {
							switch(key) {
								case 'org_unit_id':
								case 'role_id':
								break;
								
								default:
								if(value && key.indexOf('$$') !== 0) {
									rights.push({
										ou_id: role.org_unit_id,
										role_id: role.role_id,
										recht: key,
										confidential: confidential
									});
								}
								break;
							}
						});
					});
				});
				
				return JSON.stringify(rights);
			}
			
			function saveChanges ( ) {
				smartHttp.connect({
					method: 'POST',
					url: '/api/casetype/authorization/post/' + $scope.casetypeId,
					data: {
						authorizations: serializeRights()
					}
				})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error'
						});
					});
			}
			
			$scope.addRight = function ( typeId ) {
				var rightsByType = $scope.rights[typeId],
					unit = $scope.getUnits()[0],
					unitId = unit.org_unit_id,
					roleId = $scope.getRolesForUnit(unitId)[0].role_id;
					
				if(!rightsByType) {
					$scope.rights[typeId] = rightsByType = [];
				}

				rightsByType.push({
					org_unit_id: unitId,
					role_id: roleId
				});
				
			};
			
			$scope.removeRight = function ( typeId, right ) {
				var rights = $scope.rights[typeId] || [],
					index = _.indexOf(rights, right);
				
				if(index !== -1) {
					rights.splice(index, 1);
				}
			};
			
			$scope.handleUnitChange = function ( unit ) {
				var roleId = unit.role_id,
					roles = $scope.getRolesForUnit(unit.org_unit_id),
					roleIsAvailable = !!_.find(roles, function ( r ) {
						return roleId === r.role_id;
					});
				
				if(!roleIsAvailable) {
					unit.role_id = roles[0].role_id;
				}
			};
			
			$scope.getUnits = function ( ) {
				return _.filter($scope.units, function ( unit ) {
					return unit.depth > 0;
				});
			};
			
			$scope.getRolesForUnit = function ( unitId ) {
				var unitObj = _.find($scope.units, function ( u ) {
					return u.org_unit_id === unitId;
				});
				return unitObj ? unitObj.roles : [];
			};
			
			$scope.getRoleName = function ( role ) {
				return (!role.system ? '* ' : '') + role.name;
			};
			
			$scope.getUnitName = function ( unit ) {
				var name = new Array(unit.depth).join('-') + (unit.depth > 0 ? ' ' : '') + unit.name;
				return name;
			};
			
			$scope.$watch('casetypeId', function ( nwVal/*, oldVal, scope*/ ) {
				if(nwVal) {
					getRights();
				}
			});
			
			$scope.$watch('rights', function ( nwVal, oldVal/*, scope*/ ) {
				if(oldVal && oldVal !== nwVal) {
					saveChanges();
				}
			}, true);
			
			getUnits();
			
		}]);
	
})();
