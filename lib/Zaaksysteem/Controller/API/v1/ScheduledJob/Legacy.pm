package Zaaksysteem::Controller::API::v1::ScheduledJob::Legacy;
use Moose;

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has '+namespace' => ( default => 'legacy_scheduled_jobs' );

=head1 NAME

Zaaksysteem::Controller::API::v1::ScheduledJob::Legacy - API v1 controller for legacy scheduled jobs

=head1 DESCRIPTION

This controller returns the legacy scheduled job type

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('scheduled_job/legacy') : CaptureArgs(0) : Scope('legacy_jobs') {
    my ($self, $c) = @_;
}

=head2 list

=head3 URL Path

C</api/v1/scheduled_job/legacy>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_all_scheduled_jobs($c);
    $self->list_set($c);
}

=head2 object_base

=cut

sub object_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;
    my $rs = $c->model('DB::ScheduledJobs')->search_rs(
        { 'me.deleted' => undef, 'me.uuid' => $uuid },
        { prefetch => 'case_id' },
    );
    $c->stash->{$self->namespace} = $rs->first;
}

=head2 get

=head3 URL Path

C</api/v1/scheduled_jobs/legacy/[UUID]>

=cut

sub get : Chained('object_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}

=head2 get_all_scheduled_jobs

Get all the legal entitities from Zaaksysteem

=cut

sub get_all_scheduled_jobs {
    my ($self, $c)      = @_;

    my $rs = $c->model('DB::ScheduledJobs')->search_rs(
        { 'me.deleted' => undef },
        {
            order_by => 'me.id',
            prefetch => 'case_id'
        },
    );
    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content => [ $rs->all ],
    );

    $c->stash->{set} = $set;
    $c->stash->{$self->namespace} = $set->init_paging($c->request);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
