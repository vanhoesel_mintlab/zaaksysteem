package Zaaksysteem::Controller::API::v1::Controlpanel;

use Moose;

use Zaaksysteem::Types qw[UUID FQDN ArrayRefIPs CustomerType Betrokkene IPv4 NonEmptyStr Boolean Timestamp SoftwareVersions];
use BTTW::Tools;

use Zaaksysteem::Object::Types::Controlpanel;
use Zaaksysteem::Object::Types::Naw;

use Zaaksysteem::BR::Subject;

use Moose::Util::TypeConstraints qw[enum union];

use JSON qw[decode_json];
use DateTime;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern allow_pip/] }
);


has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['controlpanel'] },
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Controlpanel - APIv1 controller for Controlpanel objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/controlpanel>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Controlpanel>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Controlpanel>

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/controlpanel> URI namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('controlpanel') : CaptureArgs(0) : Scope('controlpanel') {
    my ($self, $c)      = @_;

    my $interface = $c->model('DB::Interface')->search_active({module => 'controlpanel'})->first;

    if (!$interface) {
        throw(
            'api/v1/controlpanel/interface/inactive',
            'Controlpanel interface unavailable'
        );
    };
    $c->stash->{controlpanel_interface} = $interface;

    my $zql             = 'SELECT {} FROM controlpanel';

    ### PIP toegang
    if ($c->session->{pip}) {
        $zql            = 'SELECT {} FROM controlpanel WHERE owner="' . $c->session->{pip}->{ztc_aanvrager} . '"';
    } else {
        if ($c->req->params->{zql}) {
            $zql            = $c->req->params->{zql};
        }
    }

    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::ResultSet->new(
            iterator => $c->model('Object')->zql_search($zql)->rs
        );
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ set }              = $set;
    $c->stash->{ controlpanels }    = $set->build_iterator;
}

=head2 instance_base

Reserves the C</api/v1/controlpanel/[UUID]> namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ controlpanel } = try {
        return $c->stash->{ controlpanels }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/controlpanel/not_found', sprintf(
            "The controlpanel object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ controlpanel } && $c->stash->{ controlpanel }->object_class eq 'controlpanel') {
        throw('api/v1/controlpanel/not_found', sprintf(
            "The controlpanel object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/controlpanel>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result }   = $c->stash->{ set }->init_paging($c->request);
}

=head2 list_for_daemon

=head3 URL Path

C</api/v1/controlpanel/list_for_daemon>

=cut

sub list_for_daemon : Chained('/api/v1/base') : PathPart('controlpanel/list_for_daemon') : Args() : RO {
    my ($self, $c) = @_;

    ### Only for internal usage by the controlpanel daemon
    throw('api/v1/controlpanel/no_internal_user', sprintf(
        "Not allowed",
    ), { http_code => 403 }) unless $c->user_exists;

    $c->stash->{v1_serializer_options}->{controlpanel_full} = 1;

    my $iterator = $c->model('Object')->search('controlpanel');

    my $set = try {
        return Zaaksysteem::API::v1::ResultSet->new(
            iterator => $iterator->rs
        );
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel',
            'API configuration error, unable to continue.'
        );
    };

    $set->rows_per_page(1000);
    $c->stash->{ result } = $set->init_paging($c->request);
}

=head2 update_for_daemon

Updates data given from the daemon. It is a convenience method which can only update certain data
in an instance.

=over 4

=item password

Used to clear the password

=item database_provisioned

Date when the database was provisioned

=item filestore_provisioned

Date when the filestore was provisioned

=item provisioned_on

Date when instance was provisioned

=item database

The database which was created

=item filestore

The filestore bucket which was created

=item freeform_reference

Clear or change the freeform reference

=item deleted

Indicates the instance is deleted. Our daemon will set this to true when the
C<delete_on> fields lies in the past.

=back

=head3 URL Path

C</api/v1/controlpanel/update_for_daemon>

=begin javascript

    {
        "instances": [
            {
                "reference": "500df97a-9ab8-4543-93f4-1615a64759f0",
                "database_provisioned": "2017-08-02T10:05:05Z",
                "deleted": true
            },
            {
                "reference": "600df97a-9ab8-4543-93f4-1615a64759a4",
                "filestore_provisioned": "2017-08-02T10:05:05Z"
            },
            [...]
        ]
    }

=end javascript

=cut

sub update_for_daemon : Chained('base') : PathPart('update_for_daemon') : Args(0) : RO {
    my ($self, $c) = @_;

    ### Only for internal usage by the controlpanel daemon
    throw('api/v1/controlpanel/update_for_daemon', sprintf(
        "Not allowed",
    ), { http_code => 403 }) unless $c->user_exists;

    my @instances = @{ $c->req->params->{instances} };

    my @updated_instances;
    for my $instance_changes (@instances) {
        my $params  = try {
            assert_profile(
                $instance_changes,
                profile => {
                    required => {
                        reference           => UUID,
                    },
                    optional    => {
                        provisioned_on          => Timestamp,
                        database_provisioned    => Timestamp,
                        filestore_provisioned   => Timestamp,
                        deleted                 => 'Bool',
                        password                => 'Any',
                        filestore               => 'Str',
                        database                => 'Str',
                        database_host           => 'Str',
                        freeform_reference      => 'Str',
                        stat_diskspace          => 'Num',
                        software_version        => SoftwareVersions,
                    },
                    field_filters  => {
                        provisioned_on => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
                        database_provisioned => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
                        filestore_provisioned => sub { DateTime::Format::ISO8601->parse_datetime(shift) },
                    },
                    missing_optional_valid => 1
                }
            )->valid
        } catch {
            $c->log->error(
                "Invalid parameters for update_for_daemon: $_"
            );

            return;
        };

        next unless $params;

        my $object = try {
            $c->model("Object")->retrieve(uuid => $params->{reference});
        } catch {
            $c->log->warn($_);

            throw('api/v1/controlpanel/instance/not_found', sprintf(
                "The controlpanel instance object with UUID '%s' could not be found.",
                $params->{reference}
            ), { http_code => 404 });
        };

        next unless $object;

        $c->log->debug(
            sprintf("Updating controlpanel/instance with uuid: '%s', params: ", $params->{reference})
            . join(", ", map({ "$_: '$params->{$_}'" } keys %$params))
        );

        $object = try {
            delete $params->{reference};

            for my $param (grep { exists $params->{$_} } keys %$params) {
                my $clearer = '_clear_' . $param;
                if (!defined $params->{ $param } && $object->can($clearer)) {
                    $object->$clearer();
                    next;
                }
                $object->$param($params->{ $param });
            }


            $c->model("Object")->save(object => $object);
        } catch {
            $c->log->error(
                sprintf("Error updating instance '%s' (%s): %s",$object->id, $object->fqdn, $_)
            );

            return;
        };

        if ($object) {
            push(@updated_instances, $object->id);
        }
    }

    $c->stash->{result} = {
        type        => 'freeform',
        instance    => {
            'success'       => \@updated_instances
        }
    };
}


=head2 get

=head3 URL Path

C</api/v1/controlpanel/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ controlpanel };
}


=head2 create

=head3 URL Path

C</api/v1/controlpanel/create>

=cut

define_profile create => (
    required => {
        owner           => Betrokkene,
    },
    optional => {
        customer_type       => CustomerType,
        template            => NonEmptyStr,
        ipaddresses         => IPv4,
        allowed_instances   => 'Int',
        shortname           => 'Str',
        read_only           => Boolean,
        customer_reference  => 'Str',
        allowed_diskspace   => 'Int',
    },
    defaults => {
        customer_type => 'overheid',
        template      => 'mintlab',
        customer_reference => '',
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    ### Prevent creation of controlpanel object by unknown users
    throw(
        'api/v1/controlpanel/forbidden',
        'Creation of controlpanel objects only allowed for logged in users'
    ) unless $c->user_exists;

    ### We allow UUID for owner, let's transform it
    my $owner = $c->req->params->{owner};
    if (UUID->check($owner)) {
        my $bridge  = Zaaksysteem::BR::Subject->new(schema => $c->model('DB')->schema);

        my $subject = $bridge->find($owner);

        if ($subject && $subject->subject_type eq 'company') {
            $owner      = 'betrokkene-bedrijf-' . $subject->subject->_table_id;
        }
    }

    my $params  = assert_profile({ %{ $c->req->params }, owner => $owner })->valid;

    ### Validate if owner already has an object
    if ($c->model('Object')->count('controlpanel', {owner => $params->{owner}})) {
        throw(
            'api/v1/controlpanel/controlpanel_exists',
            'controlpanel already exists for this owner_identifier'
        );
    }

    my $betrokkene = $c->model('Betrokkene')->get({}, $params->{owner});
    unless ($betrokkene) {
        throw(
            'api/v1/controlpanel/invalid_betrokkene',
            'controlpanel cannot be created on unknown owner'
        );
    }

    my $cp;
    eval {
        $c->model('DB')->txn_do(
            sub {
                $cp      = $self->_create_controlpanel_object(
                    $c->model('Object'),
                    {
                        betrokkene          => $c->model('Betrokkene')->get({}, $params->{owner}),
                        %$params
                    },
                    $c
                );
            }
        );
    };

    if ($@) {
        throw(
            'api/v1/controlpanel/fault',
            'There was a problem creating this controlpanel object: ' . $@
        );
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $cp->id });
}

=head2 update

=head3 URL Path

C</api/v1/controlpanel/[UUID]/update>

=cut

define_profile update => (
    optional => {
        template            => NonEmptyStr,
        ipaddresses         => IPv4,
        allowed_instances   => 'Int',
        shortname           => 'Str',
        read_only           => Boolean,
        customer_reference  => 'Str',
        allowed_diskspace   => 'Int',
    },
    missing_optional_valid => 1,
    msgs => sub {},
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    ### Prevent creation of controlpanel object by unknown users
    throw(
        'api/v1/controlpanel/forbidden',
        'Creation of controlpanel objects only allowed for logged in users'
    ) unless $c->user_exists;

    my $params  = assert_profile($c->req->params)->valid;

    if (keys %$params) {
        eval {
            $c->model('DB')->txn_do(
                sub {
                    $self->_update_controlpanel_object($c->model('Object'), $c->stash->{controlpanel}, $params);
                }
            );
        };

        if ($@) {
            throw(
                'api/v1/controlpanel/fault',
                'There was a problem updating this controlpanel object: ' . $@
            );
        }
    }

    $c->stash->{result} = $c->model('DB::ObjectData')->find({ uuid => $c->stash->{controlpanel}->uuid });
}

=head2 host_available

=head3 URL Path

C</api/v1/controlpanel/host_available>

=cut

define_profile host_available => (
    required => {
        fqdn           => 'Str',
        customer_type  => CustomerType,
    }
);

sub host_available : Chained('/api/v1/base') : PathPart('controlpanel/host_available') : Args(0) : RW {
    my ($self, $c) = @_;

    ### Prevent creation of controlpanel object by unknown users
    throw(
        'api/v1/controlpanel/forbidden',
        'Creation of controlpanel objects only allowed for logged in users'
    ) unless $c->user_exists;

    my $errors = {};
    my $available = (
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => $c->req->params->{fqdn},
            objectmodel     => $c->model('Object'),
            customer_type   => $c->req->params->{customer_type}
        )
        ? 1
        : 0
    );

    $c->stash->{result} = {
        type        => 'freeform',
        instance    => {
            'host'          => $c->req->params->{fqdn},
            'available'     => $available,
            'error'         => $errors->{_custom_messages}->{fqdn}
        }
    };
}

define_profile _create_controlpanel_object => (
    required    => {
        owner               => Betrokkene,
        customer_type       => CustomerType,
        betrokkene          => 'Zaaksysteem::Betrokkene::Object',
        allowed_instances   => 'Int',
    },
    optional => { template => NonEmptyStr, ipaddresses     => IPv4, shortname => 'Str', read_only  => Boolean, customer_reference => 'Str', allowed_diskspace   => 'Int',},
    defaults => { template => 'mintlab', allowed_instances => 25 },
);

sub _create_controlpanel_object {
    my $self                    = shift;
    my $objectmodel             = shift;

    my $params                  = assert_profile(shift || {})->valid;
    my $c                       = shift;

    ### Create NAW object when it does not exist
    my $naw;
    {
        if (!$objectmodel->count('naw', {owner => $params->{owner}})) {
            $naw     = Zaaksysteem::Object::Types::Naw->new_from_betrokkene(
                betrokkene      => $params->{betrokkene},
            );

            $naw        = $objectmodel->save_object(object => $naw);
        }
    }

    ### Generate a shortname, when none is given
    my $shortname = $params->{shortname};
    if (!$shortname && $naw) {
        $shortname = $naw->naam;

        ### Remove some lots of occurring words
        $shortname =~ s/$_//gi for qw/gemeente stichting agentschap/;
        $shortname =~ s/[^a-zA-Z0-9]//;
        $shortname =~ s/\s//g;
        $shortname = lc(substr($shortname, 0, 10));
    }

    ### Create controlpanel object
    my $cp      = Zaaksysteem::Object::Types::Controlpanel->new(
        owner               => $params->{owner},
        template            => $params->{template},
        customer_type       => $params->{customer_type},
        domain              => ($params->{customer_type} eq 'commercial' ? 'zaaksysteem.net' : 'zaaksysteem.nl'),
        allowed_instances   => $params->{allowed_instances},
        $params->{customer_reference} ? (customer_reference => $params->{customer_reference}) : (),
        $params->{read_only} ? (read_only => $params->{read_only}) : (),
        $shortname ? (shortname           => $shortname) : (),
        $params->{ipaddresses} ? (ipaddresses => $params->{ipaddresses}) : (),
    );

    ### Every controlpanel gets a default ACL for behandelaar
    my ($behandelaar) = grep ({ $_->system_role && $_->name eq 'Behandelaar' } @{ $c->model('DB::Roles')->get_all_cached($c->stash) });
    $cp->permit($behandelaar, qw/read write/);

    $cp         = $objectmodel->save_object(object => $cp);

    return $cp;
}

define_profile _update_controlpanel_object => (
    optional => {
        template            => NonEmptyStr,
        ipaddresses         => IPv4,
        allowed_instances   => 'Int',
        shortname           => 'Str',
        read_only           => Boolean,
        customer_reference  => 'Str',
        allowed_diskspace   => 'Int',
    },
    missing_optional_valid => 1
);

sub _update_controlpanel_object {
    my $self            = shift;
    my $objectmodel     = shift;
    my $controlpanel    = shift;

    my $params = assert_profile(shift || {})->valid;
    my $cp = $objectmodel->retrieve(uuid => $controlpanel->id);

    for my $param (grep { exists $params->{$_} } qw/read_only customer_reference allowed_instances ipaddresses template allowed_diskspace/) {
        my $clearer = '_clear_' . $param;
        if (!defined $params->{ $param } && $cp->can($clearer)) {
            $cp->$clearer();
            next;
        }
        $cp->$param($params->{ $param });
    }

    if ($params->{shortname} && $params->{shortname} !~ /betrokkene-bedrijf/) {
        $cp->shortname($params->{shortname});
    }

    $objectmodel->save(object => $cp);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
