package Zaaksysteem::Controller::API::Queue;

use Moose;
use namespace::autoclean;

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Queue - Action queue controller

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

Reserves the C</api/queue> URI path namespace.

=head3 Parameters

=over 4

=item context_object_id

C<UUID> reference of the context for which the queue is accessed.

=back

=cut

define_profile base => (
    optional => { context_object_id => UUID }
);

sub base : Chained('/api/base') : PathPart('queue') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    my $rs = $c->model('DB::Queue');

    if (exists $params->{ context_object_id }) {
        $rs = $rs->search({ object_id => $params->{ context_object_id } });
    }

    $c->stash->{ queue } = $rs;
}

=head2 item_base

Reserves the C</api/queue/[UUID]> path namespace.

=cut

sub item_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $item = $c->stash->{ queue }->find($uuid);

    unless ($item) {
        throw('api/queue/item_not_found', sprintf(
            'Could not find queue item by UUID "%s"',
            $uuid
        ));
    }

    $c->stash->{ item } = $item;
}

=head2 list

View a list of queued items.

=head3 URL

C</api/queue>

=head3 Response

Responds to a request with a list of L<Zaaksysteem::Schema::Queue> rows.

=cut

sub list : Chained('base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c, $item) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ queue }->all ];
}

=head2 item

View a specific queued item.

=head3 URL

C</api/queue/[UUID]>

=head3 Response

Response with a single L<Zaaksysteem::Schema::Queue> row instance, if one
could be found.

=cut

sub item : Chained('item_base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c, $item) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ item } ];
}

=head2 run_item

Runs a specific queued item.

=head3 URL

C</api/queue/[UUID]/run>

=head3 Response

Response with a single L<Zaaksysteem::Schema::Queue> row instance.

=cut

sub run_item : Chained('item_base') : PathPart('run') : Args(0) : ZAPI : Access(*) : DisableACL {
    my ($self, $c, $uuid) = @_;

    # emulated a logged in user here, so the underlying code for case item
    # handlers can use the should-be-replaced $zaak_rs->current_user stuff
    $c->model('DB')->schema->set_current_user($c->stash->{ item }->subject);

    $c->stash->{ zapi } = [
        # Yes, just pass along the id, so that run_item can select for update
        $c->model('Queue')->run_item($c->stash->{ item }->id)
    ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
