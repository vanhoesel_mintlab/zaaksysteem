export default function findUtilityProperty(utility, searchType, searchValue, searchResultType) {

    let object = utility.filter( obj => {
        return obj[searchType] === searchValue;
    })[0];

    return object[searchResultType];

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
