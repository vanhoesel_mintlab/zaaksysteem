package Zaaksysteem::API::v1::Serializer::Reader::CountryCode;
use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::CountryCode - Read CountryCode objects.

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::CountryCode->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_country_code';

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Object::Types::CountryCode' }

=head2 read_country_code

Reader for controlpanel object

=cut

sig read_country_code => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_country_code {
    my ($self, $serializer, $object) = @_;

    return {
        type => 'country_code',
        reference => undef,
        instance => {
            $self->_get_object_attributes($serializer, $object, qw(dutch_code label alpha_one alpha_two code)),
        }
    };
}

sub _get_object_attributes {
    my ($self, $serializer, $object, @attribute_list) = @_;
    return map { $_ =>  $object->$_ } @attribute_list;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
