package Zaaksysteem::Backend::Filestore::ResultSet;
use Moose;

use Data::UUID;
use Digest::MD5::File qw(file_md5_hex);
use File::ArchivableFormats;
use File::Basename;
use File::MimeInfo::Magic;
use File::stat;
use File::Scan::ClamAV;
use Params::Profile;
use Zaaksysteem::Constants;
use BTTW::Tools::File qw(get_file_extension);
use BTTW::Tools;

extends 'DBIx::Class::ResultSet';

with 'Zaaksysteem::Roles::FilestoreModel';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=item force_mimetype [optional]

Force the given mimetype. Please use with care, the L<File::ArchivableFormats> module is smart enough to
find the mimetype for you.

=back

=head3 Returns

The newly created Filestore object.

=cut

define_profile 'filestore_create' => (
    required => [qw/
        original_name
        file_path
    /],
    optional => [qw/
        ignore_extension
        id
        force_mimetype
    /]
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    my $args = assert_profile($opts)->valid;

    my $file_path = $args->{file_path};

    if (!-f $file_path || !-r $file_path) {
        throw('filestore/no_readable_file', "File '$file_path' is does not exist or isn't readable");
    }

    # File database properties
    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $stat = stat($file_path);
    Zaaksysteem::StatsD->statsd->end('filestore.io.stat.time', $t0);

    if (!$stat->size) {
        throw("filestore/create/empty", "Will not add an empty file");
    }

    $self->clamscan($file_path);

    # Open the file and add it to the filestore
    open my $fh, '<', $file_path or die "Unable to open file: $file_path";

    my $af = File::ArchivableFormats->new();

    my $info;
    if ($opts->{force_mimetype}) {
        $info = $af->identify_from_mimetype($opts->{force_mimetype});
    } else {
        $info = $af->identify_from_path("$file_path");
    }

    my $archivable = $info->{DANS}{archivable};

    $t0 = Zaaksysteem::StatsD->statsd->start;

    my $uuid = Data::UUID->new->create_str();

    my $default_engine = $self->filestore_model->get_default_engine();
    $default_engine->write($uuid => $fh);

    Zaaksysteem::StatsD->statsd->end('filestore.io.add.time', $t0);

    my $db_params = {
        md5              => file_md5_hex($file_path),
        size             => $stat->size,
        mimetype         => $info->{mime_type},
        original_name    => $opts->{original_name},
        uuid             => $uuid,
        storage_location => [$default_engine->name],
        is_archivable    => $archivable,
    };

    $db_params->{id} = $opts->{id} if $opts->{id};

    $t0 = Zaaksysteem::StatsD->statsd->start;
    my $create = $self->create($db_params);
    Zaaksysteem::StatsD->statsd->end('filestore.db.create.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create', 1);

    my $schema = $self->result_source->schema;
    my $qitem = $schema->resultset('Queue')->create_item(
        'filestore_replicate' => {
            label => 'Filestore-replicatie',
            data => {
                filestore_id => $create->id,
            },
        },
    );
    push(@{$schema->default_resultset_attributes->{queue_items}}, $qitem);

    return $create->discard_changes;
}

=head2 get_mimetype

    $self->get_mimetype($file_path);

    # Returns e.g.:
    # application/msword

Will use module L<File::MimeInfo::Magic> for calculating the mimetype of the given file. Unfortunatly,
this module is a little outdated and has troubles with some mimetypes. We convert these types to the
expected mimetype.

B<Corrected mimetypes>

=over

=item application/vnd.ms-word

Corrects the wrong C<application/vnd.ms-word> to C<application/msword>. References:
L<http://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc>
L<https://blogs.msdn.microsoft.com/vsofficedeveloper/2008/05/08/office-2007-file-format-mime-types-for-http-content-streaming-2/>

=back

=cut

sub get_mimetype {
    my $self        = shift;
    my $mimetype    = mimetype(shift);

    return unless $mimetype;

    my %mime_map    = (
        'application/vnd.ms-word' => 'application/msword'
    );

    return $mime_map{$mimetype} if $mime_map{$mimetype};
    return $mimetype;
}


=head2 clamscan($path)

Scan a new file, throw exception if it contains a virus.

=cut

sub clamscan {
    my ($self, $path) = @_;

    throw('filestore/clamscan/file_not_found', "Bestand niet gevonden: $path")
        unless -e $path;

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $scanner = File::Scan::ClamAV->new(
        host => $ENV{CLAMAV_SERVER} // 'localhost',
        port => $ENV{CLAMAV_PORT} // 3310,
    );

    throw(
        "filestore/clamscan/scanner_not_available",
        "Virusscanner niet operationeel",
        { error => $scanner ? $scanner->errstr : undef }
    ) unless defined($scanner) and $scanner->ping();

    open my $file, "<", $path or
        throw('filestore/clamscan/file_not_found', 'Bestand niet gevonden');
    my $data = do { local $/; <$file>; };
    close $file;

    my ($status, $virus) = $scanner->streamscan($data);

    Zaaksysteem::StatsD->statsd->end('filestore.clamav.scan.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.clamav.scan', 1);

    if ($status eq 'FOUND') {
        Zaaksysteem::StatsD->statsd->increment('filestore.clamav.virus_found', 1);
         throw(
            "filestore/clamscan/virus_found",
            "Virus '$virus' aangetroffen in bestand, niet toegevoegd."
         );
    }
}


sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 find_by_uuid

TODO: Fix the POD

=cut
