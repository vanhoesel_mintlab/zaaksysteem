package Zaaksysteem::Auth::Bedrijfid;

use Moose;
use Authen::Passphrase;

use BTTW::Tools;

has [qw/authdbic log config session params c/] => (
    is      => 'rw',
);

has [qw/succes cancel error login success_endpoint/] => (
    is      => 'rw'
);

has 'prod' => (
    'is'        => 'rw',
);

sub BUILD {
    my $self = shift;

    ### Check for session
    if (exists($self->session->{_bedrijfid})) {
        $self->_load_session;
    }
}

=head2 authenticate(%opts)

See if there's a matching record in the bedrijf_authenticatie
table, manipulate the catalyst session, return given url to redirect
to. A number of things are stored in the session for later retrieval
through this module.

=cut

define_profile authenticate => (
    required => [qw/login password success_endpoint/]
);
sub authenticate {
    my $self = shift;
    my %opts = %{ assert_profile({@_})->valid };

    ### Remove digid session
    delete($self->session->{_bedrijfid});

    return unless (
        (
            $opts{login} &&
            $opts{login} =~ /^\d+$/
        ) &&
        $opts{password}
    );

    # Search
    my $users = $self->authdbic->search(
        {
            login       => $opts{login},
        }
    );

    if ($users->count) {
        my $user = $users->first;

        ### Check passphrase
        my $authfail    = 1;
        if ($user->password =~ /^\{SSHA\}.+$/) {
            my $ppr = Authen::Passphrase->from_rfc2307($user->password);
            $authfail = 0 if ($opts{password} && $ppr->match($opts{password}));
        } else {
            $authfail = 0 if ($opts{password} && $opts{password} eq $user->password);
        }

        if ($authfail) {
            my $msg =
                'Bedrijfid: authenticatie request mislukt:'
                . ' Geen gebruiker gevonden met deze login en password';
            $self->log->error("$msg: wachtwoord incorrect");
            $self->error( $msg );
            return;
        }

        my $msg = 'Bedrijfid: authenticatie request geslaagd';
        $self->log->debug($msg);
    } else {
        my $msg =
            'Bedrijfid: authenticatie request mislukt:'
            . ' Geen gebruiker gevonden met deze login en password';
        $self->log->error($msg);
        $self->error( $msg );

        return;
    }

    ### Succes
    $self->session->{_bedrijfid} = {};
    $self->session->{authenticated_by} = 'bedrijfid';

    ### Expire session
    #$self->c->session_expire_key('_bedrijfid' => '900');
    $self->session->{_bedrijfid}->{login} = $opts{login};
    $self->session->{_bedrijfid}->{success_endpoint} = $opts{success_endpoint};

    $self->_load_session;

    return $opts{success_endpoint};
}

sub logout {
    my ($self) = @_;

    ### Destroy session
    delete($self->session->{_bedrijfid});

    $self->$_(undef) for qw/
        succes
        success_endpoint
        login
    /;
}

sub _load_session {
    my ($self) = @_;

    return unless exists($self->session->{_bedrijfid}->{login});

    $self->succes(1);
    $self->success_endpoint(
        $self->session->{_bedrijfid}->{success_endpoint}
    );
    $self->login($self->session->{_bedrijfid}->{login});
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

