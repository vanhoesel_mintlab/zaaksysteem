(function ( ) {
	
	angular.module('Zaaksysteem.user')
		.controller('nl.mintlab.user.UserExtensionController', [ '$scope', 'translationService', function ( $scope, translationService ) {
			
			$scope.$on('form.submit.success', function ( ) {
				$scope.$emit('systemMessage', {
					type: 'info', 
					content: translationService.get('Uw extensie is succesvol gewijzigd.')
				});
			});
			
		}]);
	
})();