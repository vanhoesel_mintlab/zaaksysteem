# Zaaksysteem /client

## Application code: `/client/src`

Directories in `src`:

- `shared`: shared modules 
- everything else is an app
  - `intern` is the main zaaksysteem

## `/client/src/intern`

### `./views`

Dynamically loaded top-level `intern` routes.

Cf. `require.context` call in `/client/src/intern/routing.js`
