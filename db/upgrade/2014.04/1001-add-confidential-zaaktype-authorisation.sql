BEGIN;
alter table zaaktype_authorisation add confidential boolean default 'f' not null;
create type confidentiality as enum ('public', 'internal', 'confidential');
alter table zaak add confidentiality confidentiality not null default 'public';
COMMIT;