import {
    openView,
    openCase,
    getTransitionButtonText,
    closeCaseView,
    caseViewButton
} from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app and opening a new case', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/open');
        openCase('133');
    });

    it('it should navigate to the detailView', () => {
        expect(browser.getCurrentUrl()).toContain('!melding/');
    });

    it('it should contain a detailView', () => {
        expect($('case-detail-view').isPresent()).toBe(true);
    });

    it('it should have a detailView that contains a handle button', () => {
        expect(getTransitionButtonText()).toEqual('in behandeling nemen');
    });

    it('it should display a map', () => {
        expect($('case-detail-view').isPresent()).toBe(true);
    });

});

describe('when opening the mor app and opening an open case', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/open');
        openCase('136');
    });

    it('it should navigate to the detailView', () => {
        expect(browser.getCurrentUrl()).toContain('!melding/');
    });

    it('it should contain a detailView', () => {
        expect($('case-detail-view').isPresent()).toBe(true);
    });

    it('it should have a detailView that contains a handle button', () => {
        expect(getTransitionButtonText()).toEqual('afhandelen');
    });

    it('it should display a map', () => {
        expect($('case-detail-view').isPresent()).toBe(true);
    });

    afterAll(() => {
        closeCaseView();
    });

});

describe('when opening the mor app and opening a closed case', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/open');
        openView('afgehandeld');
        openCase('150');
    });

    it('it should navigate to the detailView', () => {
        expect(browser.getCurrentUrl()).toContain('!melding/');
    });

    it('it should contain a detailView', () => {
        expect($('case-detail-view').isPresent()).toBe(true);
    });

    it('it should have a detailView that contains a handle button', () => {
        expect(caseViewButton.isPresent()).toBe(false);
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
