import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

const and1 = $('[data-name="voorwaarde_and_1"]');
const and2 = $('[data-name="voorwaarde_and_2"]');
const or1 = $('[data-name="voorwaarde_or_1"]');
const or2 = $('[data-name="voorwaarde_or_2"]');
const andResult = $('[data-name="voorwaarde_and_resultaat"]');
const orResult = $('[data-name="voorwaarde_or_resultaat"]');

describe('when opening a registration form with AND and OR testscenarios', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Voorwaarden AND of OR',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    describe('and setting the AND conditions to no and no', () => {
    
        beforeAll(() => {
    
            and1.$('[value="Nee"]').click();
            and2.$('[value="Nee"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(andResult)).toEqual('False');
    
        });
    
    });

    describe('and setting the AND conditions to yes and no', () => {
    
        beforeAll(() => {
    
            and1.$('[value="Ja"]').click();
            and2.$('[value="Nee"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(andResult)).toEqual('False');
    
        });
    
    });

    describe('and setting the AND conditions to no and yes', () => {
    
        beforeAll(() => {
    
            and1.$('[value="Nee"]').click();
            and2.$('[value="Ja"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(andResult)).toEqual('False');
    
        });
    
    });

    describe('and setting the AND conditions to yes and yes', () => {
    
        beforeAll(() => {
    
            and1.$('[value="Ja"]').click();
            and2.$('[value="Ja"]').click();
    
        });
    
        it('the result of the rule should be true', () => {
    
            expect(caseAttribute.getClosedValue(andResult)).toEqual('True');
    
        });
    
    });

    describe('and setting the OR conditions to no and no', () => {
    
        beforeAll(() => {
    
            or1.$('[value="Nee"]').click();
            or2.$('[value="Nee"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(orResult)).toEqual('False');
    
        });
    
    });

    describe('and setting the OR conditions to yes and no', () => {
    
        beforeAll(() => {
    
            or1.$('[value="Ja"]').click();
            or2.$('[value="Nee"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(orResult)).toEqual('True');
    
        });
    
    });

    describe('and setting the OR conditions to no and yes', () => {
    
        beforeAll(() => {
    
            or1.$('[value="Nee"]').click();
            or2.$('[value="Ja"]').click();
    
        });
    
        it('the result of the rule should be false', () => {
    
            expect(caseAttribute.getClosedValue(orResult)).toEqual('True');
    
        });
    
    });

    describe('and setting the OR conditions to yes and yes', () => {
    
        beforeAll(() => {
    
            or1.$('[value="Ja"]').click();
            or2.$('[value="Ja"]').click();
    
        });
    
        it('the result of the rule should be true', () => {
    
            expect(caseAttribute.getClosedValue(orResult)).toEqual('True');
    
        });
    
    });

    afterAll(() => {

        navigate.to();

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
