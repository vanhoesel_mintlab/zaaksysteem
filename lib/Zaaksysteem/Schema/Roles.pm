use utf8;
package Zaaksysteem::Schema::Roles;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Roles

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<roles>

=cut

__PACKAGE__->table("roles");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'roles_id_seq'

=head2 parent_group_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 system_role

  data_type: 'boolean'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "roles_id_seq",
  },
  "parent_group_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "system_role",
  { data_type => "boolean", is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 parent_group_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Groups>

=cut

__PACKAGE__->belongs_to(
  "parent_group_id",
  "Zaaksysteem::Schema::Groups",
  { id => "parent_group_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fyskGcqSQSsFRxzbmRZNFw

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Roles::ResultSet');

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Roles::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->add_columns('date_modified',
    { %{ __PACKAGE__->column_info('date_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
