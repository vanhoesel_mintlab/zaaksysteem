package Zaaksysteem::XML::Generator;
use Moose::Role;

with 'MooseX::Log::Log4perl';

requires 'path_prefix', 'name';

use autodie;
use Cwd qw(realpath);
use File::Basename;
use File::Spec::Functions qw(catdir catfile);
use MIME::Base64;
use Template;
use Template::Constants qw(:debug);
use Template::Provider::Encoding;

=head1 NAME

Zaaksyteem::XML::Generator - Role for template generator collection classes.

=head1 SYNOPSIS

    package Zaaksysteem::XML::Generator::SomeKindOfAPI;
    with 'Zaaksyteem::XML::Generator';

    sub path_prefix { 'some_kind_of_api' }

    # Put templates in share/xml-templates/some_kind_of_api/*.xml
    #
    # Methods will automatically be created with the same name as the XML files
    # by calling this:
    __PACKAGE__->build_generator_methods();

    __PACKAGE__->meta->make_immutable();

=head1 ATTRIBUTES

=head2 template_engine

An instance of L<Template>, to be used to process the templates.

The default value is a new instance of L<Template> with C<EVAL_PERL> and
C<INTERPOLATE> turned off, C<POST_CHOMP> turned on and
L<Template::Provider::Encoding> as a template loader (so templates are always
character strings).


=cut

has template_engine => (
    is      => 'ro',
    isa     => 'Template',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return Template->new({
            EVAL_PERL      => 0,
            INTERPOLATE    => 0,
            LOAD_TEMPLATES => [
                Template::Provider::Encoding->new({
                    INCLUDE_PATH => $self->template_path,
                })
            ],
            POST_CHOMP     => 1,
            STRICT         => 1,
            PRE_DEFINE     => $self->predefines,

            DEBUG => DEBUG_UNDEF,
        });
    },
);

=head2 predefines

Predefines are a set of predefined functions you can use within Template::Toolkit for your convenience.

By default the following functions are predefined:

=over

=item get_xml_compile_value

It retrieves the value using an XML::Compile compatible value: the plain value if it's a
scalar, the value of the "_" key if it's a hash reference.

=item encode_base64

base64 encode a scalar value.

=back

In case you want to add predefines, override, around, the builder in _build_predefines

=cut

has predefines => (
    is      => 'ro',
    isa     => 'HashRef',
    builder => '_build_predefines',
    lazy    => 1,
);

sub _build_predefines {
    return {
        get_xml_compile_value => sub {
            my $val = shift;
            return $val->{_} if (ref($val) eq 'HASH');
            return $val;
        },
        encode_base64 => sub {
            my $val = shift;
            return encode_base64($val, '');
        },
        exists     => sub {
            my $val     = shift;
            my $hashval = shift;

            if (!$hashval) {
                return (defined($val) ? $val : 0);
            }

            if (ref $val eq 'HASH') {
                return (exists($val->{$hashval}) ? $val->{$hashval} : 0);
            }

            return 0;
        }
    };
}


=head1 CLASS METHODS

=head2 template_root

Directory where the XML template subdirectories are stored.

=cut

sub template_root {
    my $class = shift;

    return catdir(
        dirname(__FILE__), # Generator.pm
        '..',              # XML/
        '..',              # Zaaksysteem/
        '..',              # lib/
        'share',
        'xml-templates',
    );
}

=head2 template_path

Canonizalized path to the XML template directory for this instance.

=cut

sub template_path {
    my $class = shift;

    return realpath(catdir($class->template_root, $class->path_prefix));
}

=head2 path_prefix

This method is required to be implemented by consumers of this role. It should
return the name of the subdirectory if the template root
(C<$PROJECT_ROOT/share/xml-templates>) where the XML templates for the
implemented API are stored.

=head2 build_generator_methods

Build methods to process templates found in the subdirectory (named by C<path_prefix> ).

This method should be called by classes implementing this role, on load time.

=cut

sub build_generator_methods {
    my $class = shift;

    # MooseX::Log::Log4perl interacts weirdly with class methods
    my $logger = $class->log(__PACKAGE__);

    opendir(my $dir, $class->template_path);

    while (my $entry = readdir($dir)) {
        my $entry_fullname = catfile($class->template_path, $entry);

        next if $entry !~ /\.xml$/;
        next if ! -f $entry_fullname; # skip entries that aren't files (directories, etc.)

        my ($shortname) = ($entry =~ /(.*)\.xml$/);

        $logger->trace("Adding method $shortname, to process template $entry");
        $class->meta->add_method(
            $shortname => sub {
                my $self = shift;
                my $type = shift;

                if ($type ne 'writer' && $type ne 'reader') {
                    $self->log->error(
                        "First argument to template processing method wasn't 'reader' or 'writer'"
                    );
                }

                $self->log->trace("Processing template '$shortname'");
                return $self->_process_template($entry, @_);
            }
        );
    }

    return;
}

=head1 INSTANCE METHODS

=head2 _process_template

Called by the generated methods in L</build_generator_methods> to actually
process the template.

Returns the processed template as a character string.

=cut

sub _process_template {
    my $self = shift;
    my ($template_filename, $stash) = @_;

    my $output = '';
    $self->template_engine->process($template_filename, $stash, \$output)
        || die $self->template_engine->error;

    return $output;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
