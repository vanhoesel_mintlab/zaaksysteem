#! /bin/bash

# REMINDER
# Use double "/" at the start of absolute paths inside containers, so Windows
# users can use this script too (MINGW mangles command line arguments)

set -e

GIT_ROOT=$(dirname $0)/../;

do_usage() {
    cat <<EOT
Usage: testbase.sh subcommand [arguments]"

Available subcommands:
    pull   - Retrieve latest version of 'testbase' git repository
    load   - (Re)load the template database and files from git repository
    save   - Save template database and files to the git repository
    commit - Commit changes in the 'testbase' git submodule
    push   - Push changes in the 'testbase' git submodule
    create - Copy template database and files for a test run
    drop   - Remove test run database and files

The usual workflows are:
    1. pull, so the latest version of testbase is available locally
    2. load, to load it into the Docker environment

After making some changes on testbase.dev.zaaksysteem.nl:
    1. save, to extract the new database, file data from the Docker environment
    2. commit, to commit the new files, database dump
    3. push, to push the new changes to the remote git repository

When running tests:
    1. create, creates a clone of 'testbase' database and files, on
      'testbase-instance.dev.zaaksysteem.nl'
    2. drop, drops the testbase-instance database and removes all related files.

EOT
}

assert_docker() {
    if ! docker-compose ps | grep -q 'Up'; then
        echo "Did not find any running containers. Did you run 'docker-compose up'?"
        exit 1
    fi
}

# Git-based commands
do_pull() {
    cd "$GIT_ROOT"
    git submodule update --init --remote db/testbase
}

do_commit() {
    cd "$GIT_ROOT"/db/testbase

    git add testbase.sql filestore
    git commit
}

do_push() {
    cd "$GIT_ROOT"/db/testbase

    git push origin master
}

# Stuff running in containers
do_load() {
    assert_docker
    echo "Loading database into Docker environment"
    docker-compose exec database psql -U zaaksysteem -f //opt/zaaksysteem/db/testbase/preload.sql template1
    docker-compose exec database psql -U zaaksysteem -f //opt/zaaksysteem/db/testbase/testbase.sql testbase
    docker-compose exec -u root backend dev-bin/testbase.sh internal load
    docker-compose restart zs-queue
}

do_save() {
    cd "$GIT_ROOT"
    assert_docker
    echo "Saving database from Docker environment to local repository"
    docker-compose exec database pg_dump -U zaaksysteem testbase > db/testbase/testbase.sql

    # Pass in the "outside" user-id, so files ownership can be set
    # appropriately from "inside"
    docker-compose exec -u root backend dev-bin/testbase.sh internal save $(id -u)
}

do_drop_instance() {
    assert_docker
    echo "Dropping testbase_instance"
    docker-compose stop zs-queue
    docker-compose exec database dropdb -U zaaksysteem testbase_instance || true
    docker-compose exec -u root backend dev-bin/testbase.sh internal drop_instance
    docker-compose start zs-queue
}

do_create_instance() {
    assert_docker

    echo "Creating testbase_instance from testbase"
    docker-compose exec database psql -U zaaksysteem -f //opt/zaaksysteem/db/testbase/copy_instance.sql template1
    docker-compose exec -u root backend dev-bin/testbase.sh internal copy_to_instance
    docker-compose restart zs-queue
}


do_internal() {
    # Internal ("in-container") actions
    case "$1" in
        load)
            rm -rf //opt/filestore/testbase
            mkdir -p //opt/filestore/testbase/storage
            cp -rv //opt/testbase/filestore/* -t //opt/filestore/testbase/storage
            chown -R zaaksysteem:zaaksysteem //opt/filestore/testbase
            ;;
        save)
            rm -rf //opt/testbase/filestore
            mkdir -p //opt/testbase/filestore
            cp -vr //opt/filestore/testbase/storage/* -t //opt/testbase/filestore
            chown -R "$2" //opt/testbase/filestore
            ;;
        copy_to_instance)
            rm -rf //opt/filestore/testbase_instance
            cp -al //opt/filestore/testbase //opt/filestore/testbase_instance
            ;;
        drop_instance)
            rm -rf //opt/filestore/testbase_instance
            ;;
        *)
            echo "Subcommand not understood: '$1'"
            exit 1
    esac
}

if [ -z "$1" ]; then
    do_usage
    exit 1
fi

# Allow commands to be run as 'testbase.sh X' or 'testbase.sh --X'
COMMAND="${1##--}"

case "${COMMAND}" in
    pull)
        do_pull
        ;;
    commit)
        do_commit
        ;;
    push)
        do_push
        ;;
    load)
        do_load
        ;;
    save)
        do_save
        ;;
    create)
        do_create_instance
        ;;
    drop)
        do_drop_instance
        ;;
    internal)
        shift
        do_internal "$@"
        ;;
    help)
        do_usage
        ;;
    *)
        echo "Unsupported command: ${COMMAND}"
        echo ""
        do_usage
        exit 1
        ;;
esac

exit 0
