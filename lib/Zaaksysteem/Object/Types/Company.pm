package Zaaksysteem::Object::Types::Company;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::BR::Subject::Types::Company
    Zaaksysteem::Object::Types::Roles::Address
);

use BTTW::Tools;
use Zaaksysteem::Types qw(ObjectSubjectType JSONBoolean Timestamp CompanyCocNumber CompanyCocLocationNumber);
use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use Zaaksysteem::Object::Types::LegalEntityType;
use Zaaksysteem::Object::Types::CountryCode;

use constant SUBJECT_TYPE => 'company';
use constant SCHEMA_TABLE => 'Bedrijf';

=head1 NAME

Zaaksysteem::Object::Types::Company - This object describes a subject of the type: Company

=head1 DESCRIPTION

This object contains the primary data of a company, like the "name" (handelsnaam) and their
coc number. It has references to their correspondence and residential address.

=head1 COMPANY ATTRIBUTES

=head2 coc_number

The number for this company from the Chamber of Commerce.

=cut

has coc_number => (
    is       => 'rw',
    isa      => CompanyCocNumber,
    traits   => [qw(OA)],
    label    => 'The CoC number (kvk-nummer)',
);

=head2 coc_location_number

The company-location number for this company from the Chamber of Commerce.

=cut

has coc_location_number => (
    is       => 'rw',
    isa      => CompanyCocLocationNumber,
    traits   => [qw(OA)],
    label    => 'The location of residence number (vestigingsnummer)',
);

### TODO:
# =head2 rsin

# The rsin number for this company from the Chamber of Commerce.

# =cut

# has rsin => (
#     is       => 'rw',
#     isa      => 'Number',
#     traits   => [qw(OA)],
#     label    => 'RSIN',
#     required => 0,
# );

=head2 company

The name of the company

=cut

has company => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Company name',
);


=head2 company_type

The type of the.

=cut

has company_type => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::LegalEntityType',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Company type',
);

=head2 mobile_phone_number

This subject's mobile phone number.

=cut

has mobile_phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Mobile phone number',
    required => 0,
);

=head2 phone_number

This subject's preferred phone number.

=cut

has phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Phone number',
    required => 0,
);

=head2 email_address

This subject's email address

=cut

has email_address => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Email address',
    required => 0,
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
