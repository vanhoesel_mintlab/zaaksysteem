import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import vormTemplateServiceModule from './../../../vorm/vormTemplateService';
import zsObjectSuggestModule from './../../../object/zsObjectSuggest';
import zsRolePickerModule from './../../../ui/zsRolePicker';
import vormObjectSuggestDisplayModule from './../../../object/vormObjectSuggest/vormObjectSuggestDisplay';
import roleServiceModule from './../../../user/roleService';
import options from './options';
import assign from 'lodash/fp/assign';
import get from 'lodash/get';
import find from 'lodash/find';
import includes from 'lodash/includes';
import template from './template.html';
import './styles.scss';

export default
	angular.module('vormAllocation', [
		zsRolePickerModule,
		zsObjectSuggestModule,
		vormTemplateServiceModule,
		composedReducerModule,
		vormObjectSuggestDisplayModule,
		roleServiceModule
	])
		.component('vormAllocation', {
			template,
			bindings: {
				depth: '<',
				types: '<'
			},
			require: {
				ngModel: 'ngModel'
			},
			controller: [
				'$scope', '$element', '$animate', 'composedReducer', 'roleService',
				function ( scope, $element, $animate, composedReducer, roleService ) {

				let ctrl = this;

				ctrl.$onInit = ( ) => {

					let optionReducer,
						selfOnlyOptionReducer;

					let handleEnter = ( ) => {
						// set focus after enter animation has started
						// and focusable element is in DOM
						$animate.off('leave', $element);
						scope.$$postDigest(( ) => {
							let focusable = $element[0].querySelector('input, select');

							if (focusable) {
								focusable.focus();
							}
							
						});
					};

					let getType = ( ) => get(ctrl.ngModel.$modelValue, 'type');

					optionReducer = composedReducer({ scope }, getType, ( ) => ctrl.types)
						.reduce(( opt, types ) => {

							let availableTypes = types || options.map(option => option.name);

							return options
								.filter(option => includes(availableTypes, option.name))
								.map(option => {

									let selected = option.name === opt;

									return assign(
										option,
										{
											icon: selected ?
												'radiobox-marked'
												: 'radiobox-blank',
											selected,
											classes: {
												selected,
												'self-assign': !!(availableTypes.length > 1 && option.name === 'me')
											}
										}
									);
								});

						});

					selfOnlyOptionReducer = composedReducer({ scope }, optionReducer )
						.reduce( opts => {
							if (opts.length === 1 && find(opts, opt => opt.name === 'me')) {
								return true;
							}
							return false;
						});

					ctrl.getIsSelfOnlyOption = selfOnlyOptionReducer.data;

					if (ctrl.getIsSelfOnlyOption() ) {
						ctrl.ngModel.$setViewValue(
							{
								type: 'me',
								data: {
									me: true
								}
							}
						);
					}

					ctrl.handleAssignSelf = ( ) => {

						let val = !!(get(ctrl.ngModel, '$modelValue.data.me'));

						ctrl.ngModel.$setViewValue(
							{
								type: 'me',
								data: {
									me: !val
								}
							}
						);
					};

					ctrl.handleChangeDepartment = ( ) => {

						let obj = get(ctrl.ngModel, '$modelValue'),
							isRouteSet = ctrl.getIsDepartmentChecked();

						ctrl.ngModel.$setViewValue(
							obj.merge(
								{
									data: {
										changeDept: !isRouteSet
									}
								},
								{
									deep: true
								}
							)
						);
					};

					ctrl.handleInformAssignee = ( ) => {

						let obj = get(ctrl.ngModel, '$modelValue'),
							isInformAssigneeSet = ctrl.getIsInformAssigneeChecked();

						ctrl.ngModel.$setViewValue(
							obj.merge(
								{
									data: {
										informAssignee: !isInformAssigneeSet
									}
								},
								{
									deep: true
								}
							)
						);
					};

					ctrl.getIsDepartmentChecked = ( ) => {
						const changeDept = get(ctrl.ngModel, '$modelValue.data.changeDept');

						if (changeDept === undefined) {
							return true;
						}
						
						return Boolean(changeDept);
					};

					ctrl.getIsInformAssigneeChecked = ( ) => {
						const informAssignee = get(ctrl.ngModel, '$modelValue.data.informAssignee');

						if (informAssignee === undefined) {
							return true;
						}

						return Boolean(informAssignee);
					};

					ctrl.getIsAssigneeEmpty = ( ) => !(get(ctrl.ngModel, '$modelValue.type') === 'coworker' && get(ctrl.ngModel, '$modelValue.data') !== null);

					ctrl.handleOrgUnitChange = ( unit, role ) => {

						ctrl.ngModel.$setViewValue(
							{
								type: 'org-unit',
								data: {
									unit,
									role
								}
							}
						);

					};

					ctrl.handleSuggest = ( contact ) => {

						ctrl.ngModel.$setViewValue(
							{
								type: 'coworker',
								data: {
									type: 'medewerker',
									label: contact.label,
									id: contact.data.id,
									uuid: contact.data.uuid,
									route: contact.data.route,
									role: contact.data.role
								}
							}
						);

					};

					ctrl.clearCoworker = ( ) => {
						ctrl.ngModel.$setViewValue({
							type: 'coworker',
							data: null
						});
					};

					ctrl.handleTypeClick = ( type ) => {
						if (type !== getType()) {
							ctrl.ngModel.$setViewValue({ type, data: type === 'org-unit' ? roleService.defaults() : null });

							if (type === 'coworker' || type === 'org-unit') {
								$animate.on('leave', $element, handleEnter);
							}
						}
					};

					ctrl.getTypeOptions = optionReducer.data;

				};

			}]
		})
		.component('vormAllocationDisplay', {
			bindings: {
				value: '<'
			},
			template:
				'<span class="delegate-display">{{$ctrl.getLabel()}}</span>',
			controller: [
				'$scope', 'composedReducer', 'roleService',
				function ( scope, composedReducer, roleService ) {

				let ctrl = this;

				ctrl.getLabel = composedReducer({ scope }, roleService.createResource(scope), ( ) => ctrl.value)
					.reduce(( units, value ) => {

						let type = get(value, 'type'),
							label = '';

						switch (type) {
							case 'me':
							label = 'Zelf in behandeling nemen';
							break;

							case 'org-unit': {
								let unit = find(units, { org_unit_id: get(value.data, 'unit') }),
									role = unit ?
										find(unit.roles, { role_id: get(value.data, 'role') })
										: null;

								if (unit) {
									label = `${unit.name}`;
								}

								if (role) {
									label += `/${role.name}`;
								}
							}
							break;

							case 'coworker':
							if (value.data) {
								label = value.data.label;
							}
							break;
							
						}

						return label;

					}).data;

			}]
		})
		.run( [ 'vormTemplateService', ( vormTemplateService ) => {

			vormTemplateService.registerType('allocation', {
				control: angular.element(
					`<vorm-allocation
						data-depth="vm.invokeData('depth')"
						data-types="vm.invokeData('types')"
						ng-model
					></vorm-allocation>`
				),
				display: angular.element(
					`<vorm-allocation-display
						data-value="delegate.value"
					>
					</vorm-allocation-display>`
				)
			});

		}])
		.name;
