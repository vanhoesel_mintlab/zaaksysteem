import casetypes from './../../utilities/casetypes';
import citizens from './../../utilities/citizens';
import organisations from './../../utilities/organisations';
import employees from './../../utilities/employees';
import findUtilityProperty from './findUtilityProperty';

export default data => {
    function getUtility (type) {

        switch (type) {
            case 'citizen':
            return citizens;

            case 'organisation':
            return organisations;

            case 'employee':
            return employees;

            default:
            break;
        }
    }

    const channelOfContact = data.channelOfContact;
    const casetypeUuid = findUtilityProperty(casetypes, 'name', data.casetype, 'uuid');
    const requestorUuid = findUtilityProperty(getUtility(data.requestorType), 'id', data.requestorId, 'uuid');
    const recipientUuid = data.recipientId ? findUtilityProperty(getUtility(data.recipientType), 'id', data.recipientId, 'uuid') : '';
    const recipientString = `&ontvanger=${recipientUuid}`;

    browser.get(`https://testbase-instance.dev.zaaksysteem.nl/intern/aanvragen/${casetypeUuid}/?aanvrager=${requestorUuid}${recipientString}&contactkanaal=${channelOfContact}`);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
