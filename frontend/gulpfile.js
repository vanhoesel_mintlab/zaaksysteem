const gulp = require('gulp');
const watch = require('gulp-watch');
const del = require('del');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const bowerFiles = require('main-bower-files');
const runSequence = require('run-sequence');
const mergeStream = require('merge-stream');
const changed = require('gulp-changed');
const livereload = require('gulp-livereload');

/* javascript */
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const protractor = require('gulp-protractor');

/* sass */
const sass = require('gulp-sass');

/* html */
const swig = require('gulp-swig');
const replace = require('gulp-replace');
const cleanCss = require('gulp-clean-css');

const SRC = './*/src';
const TEST = './*/test';
const DEST = './../root';
const APP_NAME = 'zaaksysteem';
const TASKS = ['concat', 'minify', 'comp', 'sass', 'sass-build', 'swig'];

const config = {
    js: {
        src: [
            `${SRC}/js/**/_*.js`,
            `${SRC}/js/**/*.js`,
            ...[
                'OpenLayers-2.12/OpenLayers.js',
                'jquery.cookie.js',
                'jquery.layout-latest.min.js',
                'jquery.layout.resizePaneAccordions-latest.js',
                'jquery.treeTable.js',
                'zaaksysteem.js',
                'jquery.hoverIntent.js',
                'easing.js',
                'multiselect/plugins/localisation/jquery.localisation-min.js',
                'ui.multiselect.js',
                'highcharts.js',
                'exporting.js',
                'regel_editor.js',
                'jquery.defaultvalue.js',
                'zaaksysteem/WebformTextfieldUpdateRegistry.js',
                'webform.js',
                'validate.js',
                'jquery.fileUploader.js',
                'jquery.qtip.min.js',
                'modernizr.js',
                'mintloader.js',
                'zaaktype_import.js',
                'zaaksysteem/ezra_objectsearch.js',
                'ezra_listactions.js',
                'maps.js',
                'zaaksysteem/wgxpath.install.js',
                'zaaksysteem/ezra_dialog.js',
                'zaaksysteem/ezra_simpletable.js',
                'zaaksysteem/ezra_kennisbank.js',
                'zaaksysteem/ezra_search_box.js',
                'zaaksysteem/ezra_maps.js',
                'zaaksysteem/ezra_import.js',
                'zaaksysteem/ezra_object_import.js',
                'zaaksysteem/import_inavigator.js',
                'zaaksysteem/ezra_search_chart.js',
                'ICanHaz.min.js',
                'zaaksysteem/ezra_woz_photo.js',
                'zaaksysteem/ezra_ztb_notificaties.js',
                'zaaksysteem/ezra_attribute_ie_upload.js',
                'iban.js',
                'quill/quill.min.js'
            ]
                .map(filename => `../root/tpl/zaak_v1/nl_NL/js/${filename}`)
        ],
        dest: `${DEST}/js`,
        concat: {
            name: `${APP_NAME}.js`
        },
        components: {
            name: 'components.js'
        },
        uglify: {
            name: `${APP_NAME}.min.js`,
            sourcemaps: {
                name: `${APP_NAME}.min.js.map`,
                dest: '.'
            }
        },
        protractor: {
            files: `${TEST}/e2e/**/*.js`
        }
    },
    css: {
        src: [
            `${SRC}/css/**/*.scss`,
            `!${SRC}/css/**/*_.scss`
        ],
        dest: `${DEST}/css`,
        concat: {
            name: `${APP_NAME}.css`
        }
    },
    html: {
        src: [`${SRC}/html/**/*.swig`],
        dest: `${DEST}/html`
    }
};

gulp.task('clean', function ( callback ) {
    del([
        config.js.dest,
        config.css.dest,
        config.html.dest
    ], {
        force: true,
        read: false
    })
        .then(function () {
            callback();
        }, function () {
            callback();
        });
});

gulp.task('concat', () =>
    gulp
        .src(config.js.src)
        .pipe(concat(config.js.concat.name))
        .pipe(gulp.dest(config.js.dest)));

gulp.task('comp', () =>
    mergeStream(
        gulp
            .src(bowerFiles({ env: 'production' }))
            .pipe(concat(config.js.components.name))
            .pipe(replace(/\/\/# sourceMappingURL=(.*?)map/g, ''))
            .pipe(gulp.dest(config.js.dest)),
        gulp
            .src('**/*', { cwd: './components/webodf' })
            .pipe(gulp.dest('../root/webodf')),
        gulp
            .src('**/*', { cwd: './components/pdf.js-with-viewer' })
            .pipe(gulp.dest('../root/pdf.js-with-viewer'))
    ));

gulp.task('minify', () =>
    gulp
        .src(config.js.src)
        .pipe(sourcemaps.init())
        .pipe(concat(config.js.uglify.name))
        .pipe(uglify())
        .pipe(sourcemaps.write(config.js.uglify.sourcemaps.dest))
        .pipe(gulp.dest(config.js.dest)));

gulp.task('protractor', () =>
    gulp
        .src(config.js.protractor.files)
        .pipe(protractor.protractor({
            configFile: './zaaksysteem/test/protractor.config.js'
        })));

gulp.task('sass', () =>
    gulp
        .src(config.css.src)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(rename(function ( p ) {
            p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]css[\\/]?/, '');
        }))
        .pipe(changed(config.css.dest, { hasChanged: changed.compareSha1Digest }))
        .pipe(gulp.dest(config.css.dest)));

gulp.task('sass-build', () =>
    gulp
        .src(config.css.src)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(rename(function ( p ) {
            p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]css[\\/]?/, '');
            p.basename += '.min';
        }))
        .pipe(cleanCss({
            keepSpecialComments: false
        }))
        .pipe(gulp.dest(config.css.dest)));

gulp.task('swig', () =>
    gulp
        .src(config.html.src)
        .pipe(replace(/%%(.*?)%%/g, '$1'))
        .pipe(replace(/\\%/g, '%'))
        .pipe(swig({
            defaults: {
                cache: false,
                varControls: ['[[', ']]'],
                tagControls: ['[%', '%]'],
                cmtControls: ['{#', '#}']
            }
        }))
        .pipe(rename(function ( p ) {
            p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]html/, 'nl');
        }))
        .pipe(gulp.dest(config.html.dest)));

gulp.task('compile', TASKS);

gulp.task('build', function () {
    runSequence('clean', 'compile');
});

gulp.task('e2e', function () {
    runSequence(TASKS, 'protractor');
});

gulp.task('default', function () {
    [
        'concat',
        'sass',
        'swig'
    ].forEach(function ( task ) {
        if (TASKS.indexOf(task) !== -1) {
            const key = {
                concat: 'js',
                sass: 'css',
                swig: 'html'
            }[task];

            gulp.watch(config[key].src, [task]);
        }
    });

    livereload.listen();

    return gulp
        .src(`${config.css.dest}/**/*.css`)
        .pipe(watch(`${config.css.dest}/**/*.css`))
        .pipe(livereload());
});
