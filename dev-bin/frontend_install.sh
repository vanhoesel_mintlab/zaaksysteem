#!/usr/bin/env bash

sudo npm install -g npm@3.10.10
sudo npm config set ca ""
sudo npm set progress=false

ROOT=$(readlink -m $(dirname $0))

for i in client frontend server/api2csv server/proxy
do
    dir=$(readlink -m "${ROOT}/../$i")
    if [ -d $dir ]
    then
        cd $dir

        echo "npm install of $i started"

        # Some exceptions to the rule, this one could potentially break our
        # build
        if [ "$i" == 'frontend' ]
        then
            npm install --no-optional "$@" fs
        fi

        # Force no-optional upon the user, this prevents a bug in npm < 3.5.2
        # https://github.com/npm/npm/issues/9643
        # Hopefully this also makes our build a bit quicker.
        npm install --no-optional "$@"

        rc=$?

        if [ $rc -eq 0 ]
        then
            echo "npm install of $i succeeded"
        else
            echo "npm install of $i failed" 1>&2
            exit $rc
        fi
    fi
done

#
# COPYRIGHT and LICENSE
# =============
#
# Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
# Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
#
