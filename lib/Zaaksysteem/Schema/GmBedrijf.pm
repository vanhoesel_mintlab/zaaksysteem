use utf8;
package Zaaksysteem::Schema::GmBedrijf;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::GmBedrijf

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<gm_bedrijf>

=cut

__PACKAGE__->table("gm_bedrijf");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'gm_bedrijf_id_seq'

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 hoofdvestiging_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 hoofdvestiging_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 vorig_dossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 vorig_subdossiernummer

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 handelsnaam

  data_type: 'text'
  is_nullable: 1

=head2 rechtsvorm

  data_type: 'smallint'
  is_nullable: 1

=head2 kamernummer

  data_type: 'smallint'
  is_nullable: 1

=head2 faillisement

  data_type: 'smallint'
  is_nullable: 1

=head2 surseance

  data_type: 'smallint'
  is_nullable: 1

=head2 telefoonnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 vestiging_adres

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_straatnaam

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_huisnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 vestiging_huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 vestiging_postcodewoonplaats

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 vestiging_woonplaats

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_straatnaam

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_huisnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 correspondentie_huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 correspondentie_postcodewoonplaats

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 correspondentie_woonplaats

  data_type: 'text'
  is_nullable: 1

=head2 hoofdactiviteitencode

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode1

  data_type: 'integer'
  is_nullable: 1

=head2 nevenactiviteitencode2

  data_type: 'integer'
  is_nullable: 1

=head2 werkzamepersonen

  data_type: 'integer'
  is_nullable: 1

=head2 contact_naam

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 contact_aanspreektitel

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 contact_voorletters

  data_type: 'varchar'
  is_nullable: 1
  size: 19

=head2 contact_voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 contact_geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 95

=head2 contact_geslachtsaanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 1

=head2 authenticated

  data_type: 'smallint'
  is_nullable: 1

=head2 authenticatedby

  data_type: 'text'
  is_nullable: 1

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 vestigingsnummer

  data_type: 'bigint'
  is_nullable: 1

=head2 vestiging_huisletter

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_huisletter

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 vestiging_landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=head2 correspondentie_adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 correspondentie_landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "gm_bedrijf_id_seq",
  },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "hoofdvestiging_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "hoofdvestiging_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "vorig_dossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "vorig_subdossiernummer",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "handelsnaam",
  { data_type => "text", is_nullable => 1 },
  "rechtsvorm",
  { data_type => "smallint", is_nullable => 1 },
  "kamernummer",
  { data_type => "smallint", is_nullable => 1 },
  "faillisement",
  { data_type => "smallint", is_nullable => 1 },
  "surseance",
  { data_type => "smallint", is_nullable => 1 },
  "telefoonnummer",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "vestiging_adres",
  { data_type => "text", is_nullable => 1 },
  "vestiging_straatnaam",
  { data_type => "text", is_nullable => 1 },
  "vestiging_huisnummer",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "vestiging_huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "vestiging_postcodewoonplaats",
  { data_type => "text", is_nullable => 1 },
  "vestiging_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "vestiging_woonplaats",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_straatnaam",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_huisnummer",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "correspondentie_huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "correspondentie_postcodewoonplaats",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "correspondentie_woonplaats",
  { data_type => "text", is_nullable => 1 },
  "hoofdactiviteitencode",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode1",
  { data_type => "integer", is_nullable => 1 },
  "nevenactiviteitencode2",
  { data_type => "integer", is_nullable => 1 },
  "werkzamepersonen",
  { data_type => "integer", is_nullable => 1 },
  "contact_naam",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "contact_aanspreektitel",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "contact_voorletters",
  { data_type => "varchar", is_nullable => 1, size => 19 },
  "contact_voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "contact_geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 95 },
  "contact_geslachtsaanduiding",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "authenticated",
  { data_type => "smallint", is_nullable => 1 },
  "authenticatedby",
  { data_type => "text", is_nullable => 1 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "vestigingsnummer",
  { data_type => "bigint", is_nullable => 1 },
  "vestiging_huisletter",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_huisletter",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "vestiging_adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "vestiging_landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
  "correspondentie_adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "correspondentie_landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zM+in6PAvC37b6EnSCgeWA



# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
