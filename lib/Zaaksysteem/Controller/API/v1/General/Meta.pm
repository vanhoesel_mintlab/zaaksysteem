package Zaaksysteem::Controller::API::v1::General::Meta;
use Moose;

use Zaaksysteem::Object::Types::Meta;
use Zaaksysteem::API::v1::ArraySet;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has '+namespace' => (
    default => 'meta'
);

=head1 NAME

Zaaksysteem::Controller::API::v1::General::Config - API v1 controller for configuration entries

=head1 DESCRIPTION

This controller returns a set of configuration items for the current Zaaksysteem instance.

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/general/base') : PathPart('meta') : CaptureArgs(0) : Scope('general') {
    my ($self, $c)      = @_;
    $self->get_config_items($c);
}

=head2 list

Return a list of all configuration items

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $self->list_set($c);
}

=head2 get_config_items

Aggregates several data about the Zaaksysteem instance. Returns an
L<Zaaksysteem::API::v1::ArraySet> of L<Zaaksysteem::Object::Types::Meta>
instances.

=cut

sub get_config_items {
    my ($self, $c) = @_;

    my $version = Zaaksysteem::Object::Types::Meta->new(
        label => 'Version',
        value => $c->config->{ZS_VERSION},
    );
    my $nen2081_version = Zaaksysteem::Object::Types::Meta->new(
        label => 'NEN Version',
        value => $c->config->{NEN2081_VERSION},
    );
    my $app_server = Zaaksysteem::Object::Types::Meta->new(
        label => 'Appserver',
        value => $c->req->hostname,
    );
    my $customer_info = Zaaksysteem::Object::Types::Meta->new(
        label => 'Customer information',
        value => $c->get_customer_info,
    );
    my $database = Zaaksysteem::Object::Types::Meta->new(
        label => 'Database',
        value => $c->model('DB')->schema->storage->connect_info->[0]{dsn},
    );

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => [ $version, $nen2081_version, $app_server, $customer_info, $database ],
    );
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $name) = @_;
}

=head2 get

Retrieve a single configuration item.

=head3 URL Path

C</api/v1/general/config/CONFIG_ITEM_NAME>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    throw(
        'api/v1/general/meta/get/not_allowed',
        'Single items cannot be fetched',
    );
}

=head2 create

This action is not implemented

=head3 URL Path

C</api/v1/general/config/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/meta/create/not_allowed',
        'Creation of meta entries is not allowed',
    );

}

=head2 delete

This action is not implemented

=head3 URL Path

C</api/v1/general/config/NAME/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/meta/delete/not_allowed',
        'Deletion of meta entries is not allowed',
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
