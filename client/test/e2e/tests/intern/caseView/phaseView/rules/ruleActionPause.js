import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';

const choice = $('[data-name="pauze_keuze"]');
const placeholder1 = $('[data-name="placeholder_1"]');
const pauseMessage = $('zs-case-pause-application');

describe('when opening case 68 with pause testscenarios', () => {

    beforeAll(() => {

        navigate.as('admin', 68);

        openPhase('1');

    });

    describe('and activating the pause', () => {
    
        beforeAll(() => {
    
            choice.$('[value="Pauze normaal"]').click();
    
        });

        it('the pause message should not be present', () => {
    
            expect(pauseMessage.isPresent()).toBe(false);
    
        });
    
        it('the attribute following the pausing attribute should be present', () => {
    
            expect(placeholder1.isPresent()).toBe(true);
    
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
