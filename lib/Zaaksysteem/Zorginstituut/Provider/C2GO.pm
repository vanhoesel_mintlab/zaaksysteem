package Zaaksysteem::Zorginstituut::Provider::C2GO;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Zorginstituut::Provider';
with 'Zaaksysteem::Zorginstituut::Roles::Provider';

use JSON::XS qw(decode_json);

=head1 NAME

Zaaksysteem::Zorginstituut::Providers::C2GO - C2GO interface profile

=head1 DESCRIPTION

Module to take care of sending messages to C2GO
Extends L<Zaaksysteem::Zorginstituut::Provider> and consumes L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Provider::C2GO;

    my $provider = Zaaksysteem::Zorginstituut::Provider::C2GO->new(
        di01_endpoint => 'http:://opperschaap.net'
    );

    # Internal calls for the provider calls:
    my $req = $provider->build_request(
        endpoint      => $provider->di01_endpoint
        'soap-action' => 'washme',
        xml           => 'foo',
    );

    my $res = $provider->send_request($req);


=head1 ATTRIBUTES

=head2 token

The OAuth token

=cut

has token => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    builder => '_build_token',
);

=head2 oauth_endpoint

The endpoint for OAuth authentication requests

=cut

has oauth_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);


=head2 oauth_endpoint

The endpoint for OAuth authentication requests

=cut
has oauth_username => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);


=head2 oauth_endpoint

The endpoint for OAuth authentication requests

=cut
has oauth_password => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);


=head2 oauth_tenant

The endpoint for OAuth authentication requests

=cut

has oauth_tenant => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head1 METHODS

=head2 shortname

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 label

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 configuration_items

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=cut

sub shortname { "c2go" }
sub label     { "C2GO" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_c2go_di01_endpoint',
        label       => 'Di01 endpoint',
        description => 'De URI voor Di01 (heen) berichttypes',
        type        => 'text',
        required    => 1,
        data        => { pattern => '^https:\/\/.+' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_c2go_oauth_endpoint',
        type     => 'text',
        label    => 'OAuth endpoint URI',
        data     => { pattern => '^https:\/\/.+' },
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_c2go_oauth_username',
        type     => 'text',
        label    => 'Gebruikersnaam',
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_c2go_oauth_password',
        type     => 'password',
        label    => 'Wachtwoord',
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_c2go_oauth_tenant',
        type     => 'text',
        label    => 'Tenant ID',
        required => 1,
    ),
);

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

sub _build_token {
    my $self = shift;

    my $ua = $self->ua;

    my $res = $ua->post(
        $self->oauth_endpoint,
        Content_Type => 'application/x-www-form-urlencoded',
        Content      => [
                grant_type => 'password',
                username => $self->oauth_username,
                password => $self->oauth_password,

            ]
    );

    my $json = $self->parse_json_response($res);
    return $json->{access_token};
}

sub parse_json_response {
    my ($self, $response) = @_;

    my $json = decode_json($self->assert_http_response($response));
    if ($self->log->is_trace) {
        $self->log->trace("Got the following JSON: " . dump_terse($json));
    }
    return $json;
}

around build_request => sub {
    my $orig = shift;
    my $self = shift;

    my $req = $self->$orig(@_);
    $req->header('Authorization', "Bearer " . $self->token);
    return $req;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

