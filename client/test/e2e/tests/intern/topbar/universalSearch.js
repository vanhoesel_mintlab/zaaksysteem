import {
    activate,
    getResults,
    exitUniversalSearch,
    search,
    selectFilter,
    activateContact,
    activateByKey,
    openResult,
    getMoreResults
} from './../../../functions/intern/universalSearch';
import {
    exitActiveSubject
} from './../../../functions/intern/activeContact';
import navigate from './../../../functions/common/navigate';

describe('when opening the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        activate();

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Mijn openstaande zaken',
                icon: 'file-find',
                link: '/search/mine'
            },
            {
                name: 'Mijn afdeling',
                icon: 'file-find',
                link: '/search/my-department'
            },
            {
                name: 'Alle zaken',
                icon: 'file-find',
                link: '/search/all'
            },
            {
                name: 'Zaakintake',
                icon: 'file-find',
                link: '/search/intake'
            },
            {
                name: 'Universal search',
                icon: 'file-find',
                link: '/search/d0ca4cd9-9532-43ce-8f6c-ad9e383a88fd'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when pressing the hotkeys for opening the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        activateByKey('basic');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Mijn openstaande zaken',
                icon: 'file-find',
                link: '/search/mine'
            },
            {
                name: 'Mijn afdeling',
                icon: 'file-find',
                link: '/search/my-department'
            },
            {
                name: 'Alle zaken',
                icon: 'file-find',
                link: '/search/all'
            },
            {
                name: 'Zaakintake',
                icon: 'file-find',
                link: '/search/intake'
            },
            {
                name: 'Universal search',
                icon: 'file-find',
                link: '/search/d0ca4cd9-9532-43ce-8f6c-ad9e383a88fd'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when pressing the hotkeys for opening the actions in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        activateByKey('actions');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Zaak aanmaken',
                icon: 'folder-outline',
                link: undefined
            },
            {
                name: 'Contact aanmaken',
                icon: 'account-plus',
                link: undefined
            },
            {
                name: 'Contactmoment toevoegen',
                icon: 'comment-plus-outline',
                link: undefined
            },
            {
                name: 'Widget aanmaken',
                icon: 'plus-box',
                link: undefined
            },
            {
                name: 'Dashboard',
                icon: 'home',
                link: '/intern'
            },
            {
                name: 'Documentintake',
                icon: 'file',
                link: '/zaak/intake?scope=documents'
            },
            {
                name: 'Uitgebreid zoeken',
                icon: 'magnify',
                link: '/search'
            },
            {
                name: 'Contacten zoeken',
                icon: 'account-search',
                link: '/betrokkene/search'
            },
            {
                name: 'Over Zaaksysteem.nl',
                icon: 'information',
                link: '/intern/!over'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a case in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search 152');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: '152: Universal search Nieuw',
                icon: 'folder-outline',
                link: '/intern/zaak/152'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    describe('and when clicking a case result', () => {
    
        beforeAll(() => {
    
            openResult('152: Universal search Nieuw');
    
        });
    
        it('it should open the case', () => {
    
            expect($('.case-view').isPresent()).toBe(true);
    
        });
    
    });

    afterAll(() => {

        navigate.to();

    });

});

describe('when searching for multiple cases in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search case');

    });

    it('it should display three correct results with the correct icon and link', () => {

        let expected = [
            {
                name: '155: Universal search Nieuw',
                icon: 'folder-outline',
                link: '/intern/zaak/155'
            },
            {
                name: '154: Universal search Nieuw',
                icon: 'folder-outline',
                link: '/intern/zaak/154'
            },
            {
                name: '153: Universal search Nieuw',
                icon: 'folder-outline',
                link: '/intern/zaak/153'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    describe('and when clicking to view more results', () => {
    
        beforeAll(() => {
    
            getMoreResults();
    
        });
    
        it('it should display more correct results with the correct icon and link', () => {

            let expected = [
                {
                    name: '155: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/155'
                },
                {
                    name: '154: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/154'
                },
                {
                    name: '153: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/153'
                },
                {
                    name: '152: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/152'
                }
            ];

            expect(getResults()).toEqual(expected);

        });

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a deleted case in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search case 156');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a citizen in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('211900001');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'U. Search',
                icon: 'account',
                link: '/betrokkene/10/?gm=1&type=natuurlijk_persoon'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    describe('and when activating the contact', () => {
    
        beforeAll(() => {
    
            activateContact('U. Search');

        });
    
        it('it should have activated the contact', () => {
    
            expect($('zs-active-subject').isPresent()).toBe(true);
    
        });
    
        afterAll(() => {
    
            exitActiveSubject();
    
        });
    
    });

    afterAll(() => {

        navigate.to();

    });

});

describe('when searching for a deleted citizen in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('211900002');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for an organisation in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('21190001');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Universal search',
                icon: 'domain',
                link: '/betrokkene/4/?gm=1&type=bedrijf'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a deleted organisation in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('21190002');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a document in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search doc1');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Universal search doc1',
                icon: 'file',
                link: '/intern/zaak/157/documenten/'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a deleted document in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search doc2');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a document from a deleted case in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search doc3');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for an object in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal object1');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Universal search - object1',
                icon: 'hexagon-outline',
                link: '/object/30baefd1-67fc-49d4-8c6d-ab704395156b'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a deleted object in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal object2');

    });

    it('it should display no results', () => {

        let expected = [];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a product in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('paspoort rijbewijs');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Product - Paspoort, ID-kaart en Rijbewijs',
                icon: 'package-variant-closed',
                link: '/object/659827e4-a149-4f27-9e69-08fec9802c56'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching for a question in the universal search', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('tweede paspoort');

    });

    it('it should display the correct results with the correct icon and link', () => {

        let expected = [
            {
                name: 'Vraag - Binnen hoeveel dagen kan ik mijn tweede paspoort ophalen?',
                icon: 'help',
                link: '/object/30551b76-b728-41b1-b036-3233162939b2'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    afterAll(() => {

        exitUniversalSearch();

    });

});

describe('when searching without filter', () => {

    beforeAll(() => {

        navigate.as('universalsearch');

        search('universal search');

    });

    it('it should display a result of every kind', () => {

        let expected = [
            {
                name: 'Universal search',
                icon: 'domain',
                link: '/betrokkene/4/?gm=1&type=bedrijf'
            },
            {
                name: '160: Universal search Afgehandeld',
                icon: 'folder-outline',
                link: '/intern/zaak/160'
            },
            {
                name: '158: Universal search In behandeling',
                icon: 'folder-outline',
                link: '/intern/zaak/158'
            },
            {
                name: '157: Universal search In behandeling',
                icon: 'folder-outline',
                link: '/intern/zaak/157'
            },
            {
                name: 'Universal search doc1',
                icon: 'file',
                link: '/intern/zaak/157/documenten/'
            },
            {
                name: 'Universal search',
                icon: 'file-find',
                link: '/search/d0ca4cd9-9532-43ce-8f6c-ad9e383a88fd'
            },
            {
                name: 'U. Search',
                icon: 'account',
                link: '/betrokkene/10/?gm=1&type=natuurlijk_persoon'
            }
        ];

        expect(getResults()).toEqual(expected);

    });

    let filters = [
        {
            filter: 'Contacten',
            expected: [
                {
                    name: 'Universal search',
                    icon: 'domain',
                    link: '/betrokkene/4/?gm=1&type=bedrijf'
                },
                {
                    name: 'U. Search',
                    icon: 'account',
                    link: '/betrokkene/10/?gm=1&type=natuurlijk_persoon'
                }
            ]
        },
        {
            filter: 'Zaak',
            expected: [
                {
                    name: '160: Universal search Afgehandeld',
                    icon: 'folder-outline',
                    link: '/intern/zaak/160'
                },
                {
                    name: '158: Universal search In behandeling',
                    icon: 'folder-outline',
                    link: '/intern/zaak/158'
                },
                {
                    name: '157: Universal search In behandeling',
                    icon: 'folder-outline',
                    link: '/intern/zaak/157'
                },
                {
                    name: '155: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/155'
                },
                {
                    name: '154: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/154'
                },
                {
                    name: '153: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/153'
                },
                {
                    name: '152: Universal search Nieuw',
                    icon: 'folder-outline',
                    link: '/intern/zaak/152'
                }
            ]
        },
        {
            filter: 'Zoekopdrachten',
            expected: [
                {
                    name: 'Universal search',
                    icon: 'file-find',
                    link: '/search/d0ca4cd9-9532-43ce-8f6c-ad9e383a88fd'
                }
            ]
        },
        {
            filter: 'Documenten',
            expected: [
                {
                    name: 'Universal search doc1',
                    icon: 'file',
                    link: '/intern/zaak/157/documenten/'
                }
            ]
        }
    ];

    for (const index in filters) {

        let filterType = filters[index].filter;

        describe(`with the ${filterType} filter on`, () => {

            beforeAll(() => {

                selectFilter(filterType);

            });
        
            it(`it should only display the ${filterType} results`, () => {

                expect(getResults()).toEqual(filters[index].expected);

            });

        });

    }

    afterAll(() => {

        exitUniversalSearch();

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
