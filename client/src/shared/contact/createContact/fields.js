import get from 'lodash/get';

export default ( input ) => {

	let values = get(input, 'vals'),
		countries = get(input, 'countries'),
		entities = get(input, 'entities');

	return {
		fields: [
			{
				name: 'betrokkene_type',
				label: 'Type betrokkene',
				template: 'radio',
				data: {
					options: [
						{
							value: 'natuurlijk_persoon',
							label: 'Burger'
						},
						{
							value: 'bedrijf',
							label: 'Organisatie'
						}
					]
				},
				required: true
			}
		],
		citFields: [
			{
				name: 'np-burgerservicenummer',
				label: 'BSN',
				template: 'text',
				required: false,
				data: {

				}
			},
			{
				name: 'np-voornamen',
				label: 'Voornamen',
				template: 'text',
				required: false
			},
			{
				name: 'np-voorvoegsel',
				label: 'Voorvoegsel',
				template: 'text',
				required: false
			},
			{
				name: 'np-geslachtsnaam',
				label: 'Achternaam',
				template: 'text',
				required: true
			},
			{
				name: 'np-adellijke_titel',
				label: 'Adellijke titel',
				template: 'text',
				required: false
			},
			{
				name: 'np-geslachtsaanduiding',
				label: 'Geslacht',
				template: 'radio',
				data: {
					options: [
						{
							value: 'M',
							label: 'Man'
						},
						{
							value: 'V',
							label: 'Vrouw'
						}
					]
				},
				required: true
			},
			{
				name: 'np-landcode',
				label: 'Land',
				template: 'select',
				data: {
					options: countries
				},
				required: true
			},
			{
				name: 'npc-telefoonnummer',
				label: 'Telefoonnummer (vast)',
				template: 'text',
				required: false
			},
			{
				name: 'npc-mobiel',
				label: 'Telefoonnummer (mobiel)',
				template: 'text',
				required: false
			},
			{
				name: 'npc-email',
				label: 'Emailadres',
				template: 'text',
				required: false
			}
		],
		persDomesticAddrFields: [
			{
				name: 'briefadres',
				label: 'Briefadres',
				template: 'checkbox',
				data: {
					checkboxLabel: 'Ja'
				},
				required: false
			},
			{
				name: 'np-postcode',
				label: 'Postcode',
				template: 'text',
				required: ( ) => !get(values, 'briefadres', false)
			},
			{
				name: 'np-huisnummer',
				label: 'Huisnummer',
				template: 'text',
				required: ( ) => !get(values, 'briefadres', false)
			},
			{
				name: 'np-huisnummertoevoeging',
				label: 'Huisnummertoevoeging',
				template: 'text',
				required: false
			},
			{
				name: 'np-straatnaam',
				label: 'Straat',
				template: 'text',
				required: ( ) => !get(values, 'briefadres', false),
				data: {
					fieldDisabled: get(values, 'np-Disabled', true) && get(values, 'fieldDisabled')
				}
			},
			{
				name: 'np-woonplaats',
				label: 'Woonplaats',
				template: 'text',
				required: ( ) => !get(values, 'briefadres', false),
				data: {
					fieldDisabled: get(values, 'np-Disabled', true) && get(values, 'fieldDisabled')
				}
			},
			{
				name: 'np-in_gemeente',
				label: 'Binnengemeentelijk',
				template: 'checkbox',
				data: {
					checkboxLabel: 'Ja'
				},
				required: false
			}
		],
		persForeignAddrFields: [
			{
				name: 'np-adres_buitenland1',
				label: 'Adresregel 1',
				template: 'text',
				required: true
			},
			{
				name: 'np-adres_buitenland2',
				label: 'Adresregel 2',
				template: 'text',
				required: false
			},
			{
				name: 'np-adres_buitenland3',
				label: 'Adresregel 3',
				template: 'text',
				required: false
			}
		],
		corrFields: [
			{
				name: 'np-correspondentie_postcode',
				label: 'Correspondentie postcode',
				template: 'text',
				required: true

			},
			{
				name: 'np-correspondentie_huisnummer',
				label: 'Correspondentie huisnummer',
				template: 'text',
				required: true
			},
			{
				name: 'np-correspondentie_huisnummertoevoeging',
				label: 'Correspondentie huisnummer toevoeging',
				template: 'text',
				required: false
			},
			{
				name: 'np-correspondentie_straatnaam',
				label: 'Correspondentie straat',
				template: 'text',
				required: true,
				data: {
					fieldDisabled: get(values, 'np-correspondentie_Disabled', true) && get(values, 'fieldDisabled')
				}
			},
			{
				name: 'np-correspondentie_woonplaats',
				label: 'Correspondentie woonplaats',
				template: 'text',
				required: true,
				data: {
					fieldDisabled: get(values, 'np-correspondentie_Disabled', true) && get(values, 'fieldDisabled')
				}
			}
		],
		orgFields: [
			{
				name: 'vestiging_landcode',
				label: 'Vestiging land',
				template: 'select',
				data: {
					options: countries
				},
				required: true
			},
			{
				name: 'rechtsvorm',
				label: 'Rechtsvorm',
				template: 'select',
				data: {
					options: entities
				},
				required: true,
				when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
			},
			{
				name: 'dossiernummer',
				label: 'KVK-nummer',
				template: 'text',
				required: true,
				when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
			},
			{
				name: 'vestigingsnummer',
				label: 'Vestigingsnummer',
				template: 'text',
				required: false,
				when: [ '$values', ( vals ) => vals.vestiging_landcode === '6030' ]
			},
			{
				name: 'handelsnaam',
				label: 'Handelsnaam',
				template: 'text',
				required: true
			},
			{
				name: 'npc-telefoonnummer',
				label: 'Telefoonnummer (vast)',
				template: 'text',
				required: false
			},
			{
				name: 'npc-mobiel',
				label: 'Telefoonnummer (mobiel)',
				template: 'text',
				required: false
			},
			{
				name: 'npc-email',
				label: 'Emailadres',
				template: 'text',
				required: false
			}
		],
		orgDomesticAddrFields: [
			{
				name: 'vestiging_postcode',
				label: 'Vestiging postcode',
				template: 'text',
				required: true
			},
			{
				name: 'vestiging_huisnummer',
				label: 'Vestiging huisnummer',
				template: 'text',
				required: true
			},
			{
				name: 'vestiging_huisletter',
				label: 'Vestiging huisletter',
				template: 'text',
				required: false
			},
			{
				name: 'vestiging_huisnummertoevoeging',
				label: 'Vestiging toevoeging',
				template: 'text',
				required: false
			},
			{
				name: 'vestiging_straatnaam',
				label: 'Vestiging straat',
				template: 'text',
				required: true,
				data: {
					fieldDisabled: get(values, 'vestiging_Disabled', true) && get(values, 'fieldDisabled')
				}
			},
			{
				name: 'vestiging_woonplaats',
				label: 'Vestiging woonplaats',
				template: 'text',
				required: true,
				data: {
					fieldDisabled: get(values, 'vestiging_Disabled', true) && get(values, 'fieldDisabled')
				}
			}
		],
		orgForeignAddrFields: [
			{
				name: 'vestiging_adres_buitenland1',
				label: 'Vestiging Adresregel 1',
				template: 'text',
				required: true
			},
			{
				name: 'vestiging_adres_buitenland2',
				label: 'Vestiging Adresregel 2',
				template: 'text',
				required: false

			},
			{
				name: 'vestiging_adres_buitenland3',
				label: 'Vestiging Adresregel 3',
				template: 'text',
				required: false
			}
		]
	};
};
