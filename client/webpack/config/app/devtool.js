const cherryPack = require('../../library/cherryPack');
const { IS_DEBUG_BUILD } = require('../../library/constants');

module.exports = cherryPack({
  production: IS_DEBUG_BUILD ? 'source-map' : false,
  development: 'cheap-module-eval-source-map',
});
