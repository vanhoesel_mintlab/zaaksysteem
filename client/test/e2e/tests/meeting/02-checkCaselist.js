import navigate from './../../functions/common/navigate';
import {
    header,
    openTab,
    proposals,
    getMeetingTitleBarText,
    getColumnTitles,
    getProposalInfo,
    toggleMeeting,
    toggleSorting
} from './../../functions/meeting/meeting';

describe('when opening the meeting app', () => {

    beforeAll(() => {
        navigate.as('burgemeester', '/vergadering/bbv/');
    });

    it('it should contain a header', () => {
        expect(header.isPresent()).toBe(true);
    });

    it('it should contain a meetingList where the items are initially closed', () => {
        expect(proposals.count()).toEqual(0);
    });

    it('the meeting item should contain the date', () => {
        expect(getMeetingTitleBarText(1)).toContain('02 jan. 2017');
    });

    it('the meeting item should contain the time', () => {
        expect(getMeetingTitleBarText(1)).toContain('10:20');
    });

    it('the meeting item should contain the subject', () => {
        expect(getMeetingTitleBarText(1)).toContain('Openstaande vergadering');
    });

    it('the meeting item should contain the number of proposals contained', () => {
        expect(getMeetingTitleBarText(1)).toContain('(2)');
    });

    it('the meeting item should contain the location', () => {
        expect(getMeetingTitleBarText(1)).toContain('Vergaderzaal 1');
    });

    it('the meeting item should contain the chairman', () => {
        expect(getMeetingTitleBarText(1)).toContain('Wethouder 2');
    });

    describe('and when opening the first meeting', () => {

        beforeAll(() => {
            toggleMeeting(1);
        });

        it('it should contain a meetingList with two proposals', () => {
            expect(proposals.count()).toEqual(2);
        });

        it('the proposals should be displayed with the correct columns', () => {
            const columnTitles = ['Vertrouwelijkheid', 'Voorstel Agenderingswijze', 'Datum Voorstel 1', 'Resultaat', 'Voorstel Bijlage 1'];

            expect(getColumnTitles()).toEqual(columnTitles);
        });

        it('the proposals should contain the right information', () => {
            const proposalInfo = ['1', '40', 'Openbaar', 'Bespreken', '15 jan. 2017', '-', '243c2df8-e93c-4b3e-ad11-6ce7e9d16ef3', '2'];

            expect(getProposalInfo('40')).toEqual(proposalInfo);
        });

    });

    describe('and when displaying the proposals unsorted', () => {

        beforeAll(() => {
            toggleSorting();
        });

        it('it should contain a meetingList with two proposals', () => {
            expect(proposals.count()).toEqual(3);
        });

        afterAll(() => {
            toggleSorting();
        });

    });

    describe('and when opening the closed view and the first meeting', () => {

        beforeAll(() => {
            openTab(2);
            toggleMeeting(1);
        });

        it('it should contain a meetingList with two proposals', () => {
            expect(proposals.count()).toEqual(2);
        });

    });

    describe('and when displaying the proposals unsorted', () => {

        beforeAll(() => {
            toggleSorting();
            openTab(2);
        });

        it('it should contain a meetingList with two proposals', () => {
            expect(proposals.count()).toEqual(2);
        });

        afterAll(() => {
            toggleSorting();
        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
