package Zaaksysteem::Backend::Sysin::Modules::MultiChannel;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use Zaaksysteem::External::DataB;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::MultiChannel - Integration module for
DataB's MultiChannel platform

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 CONSTANTS

=head2 INTERFACE_ID

C<qmatic>

=cut

use constant INTERFACE_ID               => 'multichannel';

=head2 INTERFACE_CONFIG_FIELDS

C<ArrayRef[Zaaksysteem::ZAPI::Form::Field]>

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint',
        type => 'text',
        label => 'URL MultiChannel Documentserver',
        required => 1,
        description => 'Configureer de endpoint URL voor de documentserver'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_cid',
        type => 'text',
        label => 'DataB CID',
        required => 1,
        description => 'Klantcode voor het gebruik van de DataB API'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_secret',
        type => 'text',
        label => 'DataB sleutel',
        required => 1,
        description => 'De geheime sleutel om veilig met DataB te communiceren'
    )
];

=head2 MODULE_SETTINGS

C<HashRef>

=cut

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'DataB MultiChannel DMS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => [],
    is_multiple                     => 0,
    is_manual                       => 0,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition  => {
        get_session_url => { method => 'get_session_url', update => 1 }
    },

    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'connection_test',
                method => 'test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 TRIGGERS

=head2 get_session_url

The main trigger for this module, requests a new session URL from the
document server and returns it.

=head3 Usage

    my $interface = ...;    # The 'multichannel' interface component
    my $subject = ...;      # Betrokkene object
    my $useragent = ...;    # DataB does some sanity checks using the useragent string of the client

    my $url = $interface->process_trigger('get_session_url', {
        subject => $subject,
        useragent => $useragent_string,
    });

=head3 Exceptions

=over 4

=item sysin/multichannel/request_failed

Failure to connect to the MultiChannel Documentserver

=item sysin/multichannel/request_unsuccessful

MultiChannel Documentserver responded with an unsuccessful status

=back

=cut

define_profile get_session_url => (
    required => {
        subject    => 'Zaaksysteem::Betrokkene::Object',
        useragent  => 'Str'
    }
);

sub get_session_url {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    return $interface->model->get_session_url(%$params);
}

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $config    = $interface->get_interface_config;

    return Zaaksysteem::External::DataB->new(
        cid       => $config->{cid},
        secret    => $config->{secret},
        endpoint  => $config->{endpoint},
    );
}


=head2 test_connection

Hook for the Sysin function testing interface. This method will attempt to
connect to the given endpoint. Checks are do to ensure the endpoint is SSL
encryted.

=cut

sub test_connection {
    my ($self, $interface) = @_;

    return $self->test_host_port_ssl($interface->jpath('$.endpoint'));
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
