package TestFor::General::OutlookMailParser;
use base (ZSTest);

use TestSetup;
use Zaaksysteem::OutlookMailParser;
use File::Spec::Functions;

sub zs_outlookmailparsing : Tests {

    my $parser = Zaaksysteem::OutlookMailParser->new();

    {
        my $args = {
            path          => catfile(qw(t data mails marco.mime)),
            original_name => 'foobar.eml',
        };

        my $msg = $parser->process_outlook_message($args);
        ok($msg, ".eml converted to MIME");
        isa_ok($msg, "Mail::Track::Message");
    }

    {
        my $args = {
            path          => catfile(qw(t inc  OutlookTestMails HTML_mail.msg)),
            original_name => 'foobar.msg',
        };

        my $msg = $parser->process_outlook_message($args);
        ok($msg, ".msg converted to MIME");
        isa_ok($msg, "Mail::Track::Message");
    }

    {
        my $args = {
            path          => catfile(qw(t data mails marco.mime)),
            original_name => 'foobar.foo',
        };

        my $msg = $parser->process_outlook_message($args);
        is($msg, undef, "Non-mail files are not parsed based on their extension");
    }

}

1;

__END__

=head1 NAME

TestFor::General::OutlookMailParser - A testmodule for the ZS::OutlookMailParser module

=head1 DESCRIPTION

Test the outlook mail parsing module

=head1 SYNOPSIS

    ./zs_prove t/lib/TestFor/General/OutlookMailParser.pm

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
