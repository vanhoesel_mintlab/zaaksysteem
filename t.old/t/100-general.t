#! perl
use TestSetup;

# This prevents classes inheriting ZSTest to run twice...better idea welcome.
BEGIN {
    $ENV{'Test::Class::Load'} = 1;
};

initialize_test_globals_ok;

use Test::Class::Load 't/lib/TestFor/General';

Test::Class->runtests();
