import angular from 'angular';
import get from 'lodash/get';
import find from 'lodash/find';
import shortId from 'shortid';
import { searchHashExpression } from '../../../../shared/util/searchHash';

const mimeTypes = {
	doc: 'application/msword',
	docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	dotx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
	gif: 'image/gif',
	jpg: 'image/jpeg',
	html: 'text/html',
	pdf: 'application/pdf',
	png: 'image/png',
	ppt: 'application/vnd.ms-powerpoint',
	pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	rtf: 'application/rtf',
	tif: 'image/tiff',
	tiff: 'image/tiff',
	txt: 'text/plain',
	xls: 'application/vnd.ms-excel',
	xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
};

export default class ExternalDocumentViewController {

	static get $inject() {
		return [
			'$rootScope', '$scope', '$document', '$window', '$state', '$http',
			'composedReducer', 'externalSearchService', 'snackbarService'
		];
	}

	constructor(
		$rootScope, scope, $document, $window, $state, $http,
		composedReducer, externalSearchService, snackbarService
	) {
		const ctrl = this;

		ctrl.searchQuery = '';

		const fieldReducer = composedReducer({ scope }, ctrl.document())
			.reduce(doc =>
				doc
					.map(field => ({
						id: shortId(),
						name: field.name,
						label: field.label,
						value: get(field, 'value') || '-'
					}))
			);

		const relatedDocumentReducer = composedReducer({ scope }, ctrl.relatedDocuments)
			.reduce(documents =>
				documents
					.map(doc => ({
						id: shortId(),
						label: get(doc, '_source.Filename'),
						link: $state.href('externaldocument', {
							next2knowSearchIndex: get(doc, '_index'),
							next2knowdocumentId: get(doc, '_id')
						})
					}))
			);

		ctrl.getFields = fieldReducer.data;

		ctrl.getImage = () => ctrl.thumbnail();

		ctrl.getFilename = () => get(
			find(
				fieldReducer.data(),
				field => ((field.name === 'Filename') && (field.value !== '-'))
			),
			'value'
		);

		ctrl.isDownloadable = () => Boolean(ctrl.getFilename());

		ctrl.downloadDocument = () => {
			const filename = ctrl.getFilename();
			const ext = filename.split('.').pop();

			event.preventDefault();

			snackbarService.wait(
				'Het bestand wordt gedownload', {
					promise:
						$http(externalSearchService.getDownloadRequestOptions(filename))
							.then(response => {
								const URL = ('URL' in $window) ?
									$window.URL
									: $window.webkitURL;
								const file = new Blob([response.data], { type: mimeTypes[ext] });
								const blobUrl = URL.createObjectURL(file);
								const anchor = angular.element('<a></a>');

								anchor.attr('href', blobUrl);
								anchor.attr('download', filename);
								$document.find('body').append(anchor);
								anchor[0].click();
								anchor.remove();
								URL.revokeObjectURL(blobUrl);
							}),
					then: () => '',
					catch: () => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
				});
		};

		ctrl.getRelatedDocuments = relatedDocumentReducer.data;

		ctrl.openSpotenlighter = () => {
			$rootScope.$emit('spotenlighter:open', this.searchQuery);
		};
	}

	setSearchQuery() {
		const { hash } = window.location;
		const match = searchHashExpression.exec(hash);

		if (match) {
			this.searchQuery = decodeURIComponent(match[1]);

			return true;
		}

		return false;
	}

}
