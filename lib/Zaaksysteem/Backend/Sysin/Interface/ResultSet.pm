package Zaaksysteem::Backend::Sysin::Interface::ResultSet;
use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw(NonEmptyStr);

use Zaaksysteem::Backend::Sysin::Modules;

extends 'Zaaksysteem::Backend::ResultSet';
with 'Zaaksysteem::Backend::Sysin::Interface::Roles::InterfaceConfig';

has '_active_params' => (
    is          => 'ro',
    lazy        => 1,
    default     => sub {
        return {
            active           => 1,
            date_deleted     => undef,
        }
    }
);

=head2 interface_create

Add an interface definition to the database.

=head3 Arguments

=over

=item name [required]

A name chosen by the zaaksysteem administrator to describe this interface.

=item module [required]

The perl library that this interface is set up for. This has to be -exactly- the same.

=item interface_config [optional]

The JSON hash containing all relevant configuration details.

=item case_type_id [optional]

An optional case_type_id pointing to the zaaktype->id.

=item max_retries [optional, default 10]

Defines the number of automated retries on the transactions belonging to this
interface before the process gives up. Defaults are set in the module.

=item multiple [optional, default 0]

Defines whether or not this interface will always receive a single mutation or multiple mutations.

=item active [optional, default 0]

Defines whether or not the Interface should be considered as 'in production'.

=back

=head3 Returns

A newly created Sysin::Interface object.

=cut

define_profile interface_create => (

    missing_optional_valid => 1,
    required               => [
        qw/
            name
            module
            schema
            /
    ],
    optional => [
        qw/
            case_type_id
            max_retries
            multiple
            interface_config
            active
            /
    ],
    constraint_methods => {
        max_retries => qr/^\d+/,
        multiple    => qr/(0|1)/,
        active      => qr/(0|1)/,
        module      => sub {
            my ($dfv, $val) = @_;

            my $schema = $dfv->get_input_data->{schema};

            return 1
                if (grep { $_->name eq $val }
                Zaaksysteem::Backend::Sysin::Modules->list_of_modules($schema));

            return;
        },
        case_type_id => sub {
            my ($dfv, $val) = @_;
            my ($entry);

            return unless ref($val) || $val =~ /^\d+/;

            my $schema = $dfv->get_input_data->{schema};

            my $zt_id = (ref($val) ? $val->id : $val);

            return 1
                if (($entry = $schema->resultset('Zaaktype')->find($zt_id))
                && $entry->active
                && !$entry->deleted);

            return;
            }
    },
    typed    => { name => NonEmptyStr, },
    defaults => {
        active           => 0,
        interface_config => '{}',
    }
);

sub interface_create {
    my $self    = shift;
    my $opts    = assert_profile(
        {
            %{ shift() },
            schema  => $self->result_source->schema
        }
    )->valid;

    $self->_prepare_options($opts);
    delete $opts->{schema};

    my $retval = $self->create($opts);

    $retval->trigger_log('create');

    return $retval;
}

=head2 find_by_module_name

Returns the first active interface with the specified "module".

If more than one active interface exists with the given "module", an exception will be
thrown.

=head3 Positional arguments

=over

=item * module

The module name to search for. This is the value of the "name" field in the
related interface module.

=back

=cut

sub find_by_module_name {
    my $self    = shift;
    my $module  = shift;

    my @interfaces = $self->search_active({ module => $module })->all;

    if (scalar @interfaces > 1) {
        throw('sysin/interfaces/find_by_module_name/multiple_found', sprintf(
            'Cannot get interface for module "%s": found multiple objects',
            $module
        ));
    }

    return $interfaces[0];
}

=head2 search_module

Convenience method for retrieving modules by their moduletype.

Provides abiltity to filter with L<JSON::Path> queries, and do simple tests
for values.

Example:

    # This will search for 'samlidp' modules, with a "true" value in the
    # "login_type_company" field.
    $rs->search_module('samlidp', '$.login_type_company');

=head3 Params

=over

=item module

B<Required>. String containing the module type to be searched for

=item jpath

B<Required>. A L<JSON::Path> expression to be executed in the context of the
interface's configuration JSON content. Any interface for which this
JSON::Path->value returns true-ish will be returned.

=back

=cut

sig search_module => 'Str, Str';

sub search_module {
    my $self = shift;
    my ($module, $path) = @_;

    my $module_rs = $self->search_active({ module => $module });

    return grep { $_->jpath($path) } $module_rs->all;
}

=head1 INTERNAL METHODS

=head2 _prepare_options

Allows Moose Roles to work on the options given to a create or update call.

=cut

sub _prepare_options { shift; return shift; }


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
