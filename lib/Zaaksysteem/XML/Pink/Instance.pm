package Zaaksysteem::XML::Pink::Instance;
use Moose;
use File::Spec::Functions qw(catfile);
use BTTW::Tools;

with qw(
    Zaaksysteem::StUF::GeneralInstance
    Zaaksysteem::XML::Compile::Instance
);

=head1 NAME

Zaaksysteem::XML::Pink::Instnace - PinkRoccade specific SOAP calls

=head1 SYNOPSIS


=head1 DESCRIPTION

Pink's own StUF-related calls for handling "VOA"s

=head1 ATTRIBUTES

=head2 name

Name for the accessor this instnace creates on the L<XML::Compile::Backend>

=head2 wsdl

WSDL files to use for SOAP calls.

=head2 schemas

XSD files to read for XML definitions related to the Pink SOAP calls.

=head2 elements

List of elements to create a reader/writer for.

=cut

has name => (is => 'ro', default => 'pink');

has wsdl => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return [
            catfile($self->home, qw(share wsdl CML afnemersindiciatie_voa.wsdl)),
        ];
    }
);

has schemas => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return [
            catfile($self->home, qw(share wsdl CML afnemersindiciatie_voa.xsd)),
            catfile($self->home, qw(share wsdl CML cgs_fouten.xsd)),
        ],
    },
);

has elements => (
    is => 'ro',
    lazy => 1,
    default => sub {
        return [
            {
                element  => '{http://makelaarsuite.nl/CML}plaatsAfnemersIndicatieVerzoek',
                compile  => 'RW',
                method   => 'pink_voa_kennisgeving',
                soapcall => {
                    call    => 'plaatsAfnemersIndicatieVerzoek',
                    service => 'PlaatsenAfnemersIndicatie',
                    port    => 'PlaatsenAfnemersIndicatiePort',
                },
            },
            {
                element  => '{http://makelaarsuite.nl/CML}plaatsAfnemersIndicatieAntwoord',
                compile  => 'RW',
                method   => 'pink_voa_bevestiging',
            },
        ],
    },
);


has _reader_config => (is => 'ro', default => sub {});


=head1 METHODS

=head2 set_pink_voa

Arguments: \%PARAMS, \%OPTIONS

Return value: $STRING_XML

    my $xml             = $self->xmlcompile->stuf0204->set_pink_voa(
        {
            a_nummer                => '1234567890',
            burgerservicenummer     => '987654321',
            voornamen               => 'Tinus',
            voorletters             => 'TV',
            voorvoegsel             => 'vander',
            geslachtsnaam           => 'Testpersoon',
            geboortedatum           => '19830609',
            datum_overlijden        => '19830610',
            indicatie_geheim        => undef,
            burgerlijkestaat        => undef,
            aanduiding_naamgebruik  => 'P',
        },
        {
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

Generates the XML for a "voa message to pink".

=cut

define_profile 'set_pink_voa' => (
    required => [],
    require_some => {
        'bsn_or_anummer' => [1, qw/burgerservicenummer a_nummer/],
    },
    ### Below constraints are from official StUF XSD
    constraint_methods  => {
        burgerservicenummer => qr/^\d{8,9}$/,           ### Not from XSD: our own restriction
        a_nummer            => qr/^[1-9][0-9]{9}$/,
    },
);

sub set_pink_voa {
    my $self                = shift;
    my $params              = assert_profile(shift || {})->valid;
    my $options             = shift || {};

    my $perldata            = {};

    if ($params->{a_nummer}) {
        $perldata->{Anummer}    = $params->{a_nummer};
    } elsif ($params->{burgerservicenummer}) {
        $perldata->{BSN}        = $params->{burgerservicenummer};
    }

    return $self->handle_message(
        'pink_voa_kennisgeving',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );
}

sub spoof_answer {
    my $self                = shift;
    my $request             = shift;

    my $xml                 = $request->content;

    if ($xml =~ /CML:plaatsAfnemersIndicatieVerzoek/) {
        return $self->_spoof_pink_voa($xml);
    }

}

sub _spoof_pink_voa {
    my $self = shift;
    my $xml  = shift;

    my ($element, $toomany) = $self->retrieve_xml_element(
        {
            xml    => $xml,
            method => 'pink_voa_kennisgeving'
        }
    );

    my $perldata = try {
        $self->pink_voa_kennisgeving('READER', $element)
    } catch {
        $self->log->warn("Spoof: _spoof_pink_voa: \$perldata failed: $_");
        return;
    };

    my $lv01xml = try {
        $self->_spoof_pink_voa_response($perldata);
    } catch {
        $self->log->warn("Spoof: _spoof_pink_voa: \$lv01xml failed: $_");
        return;
    };

    return HTTP::Response->new(
        200,
        'answer manually created',
        ['Content-Type' => 'text/xml'], $lv01xml
    );
}

sub _spoof_pink_voa_response {
    my $self     = shift;
    my $perldata = shift;

    my $response = try {
        $self->pink_voa_bevestiging(
            'WRITER',
            {
                melding    => 'Works',
                referentie => String::Random->new()->randpattern('nnnnnnn')
            }
        );
    } catch {
        $self->log->warn("Spoof: _spoof_pink_voa_response: \$response failed: $_");
    };

    return $self->_wrap_in_soap($response);
}

sub _wrap_in_soap {
    my $self = shift;
    my $xml  = shift;

    my $xmlelement = XML::LibXML->load_xml(string => $xml);
    my $doc = XML::LibXML::Document->new('1.0', 'utf-8');

    my $envelope = $doc->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    $doc->setDocumentElement($envelope);

    $envelope->setNamespace('http://schemas.xmlsoap.org/soap/envelope/', 'soap', 1);
    $envelope->setNamespace('http://www.w3.org/2001/XMLSchema',          'xsd',  0);
    $envelope->setNamespace('http://www.w3.org/2001/XMLSchema-instance', 'xsi',  0);

    my $body = $doc->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    $envelope->appendChild($body);

    $body->addChild($xmlelement->documentElement());

    return $doc->toString(1);
}

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
