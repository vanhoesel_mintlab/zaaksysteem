export const header = $('meeting-app meeting-nav');
export const tabs = $('.meeting-nav__tabs');
export const noteBarPlaceholder = $('proposal-note-bar');
export const voteBar = $('proposal-vote-bar');
export const voteButtons = element.all(by.css('.vote-button'));
export const voteSave = $('.proposal-vote-view__header-buttons button:nth-child(2)');
export const voteTextarea = $('textarea');
export const noteBar = $('.proposal-detail-view__notecontent');
export const noteTextarea = $('textarea');
export const noteSave = $('.proposal-note-view__header-button .mdi-check');
export const notes = element.all(by.css('.proposal-detail-view__noteplaceholder'));
export const proposalDetailViews = element.all(by.css('proposal-detail-view'));
export const proposalContainer = $('.proposal-item-table');
export const proposals = element.all(by.css('meeting-app ui-view .proposal-item-table tbody tr'));
export const meetings = element.all(by.css('meeting-app ui-view .meeting-item-titlebar'));
export const sortingButton = $('.meeting-nav__button-bar button:nth-child(2)');
export const meetingTitleBar = element.all(by.css('meeting-item-titlebar'));

export const openTab = tabToOpen => {
    $(`.meeting-nav__tabs a:nth-child(${tabToOpen})`).click();
};

export const toggleMeeting = meetingToOpen => {
    const meetingElementToOpen = meetingToOpen - 1;

    meetings
        .get(meetingElementToOpen)
        .click();
};

export const toggleSorting = () => {
    sortingButton.click();
};

export const getMeetingTitleBarText = meetingToGet => new Promise(resolve => {
    meetings
        .get(meetingToGet - 1)
        .getText()
        .then(text => resolve(text));
});

export const getColumnTitles = () => new Promise(resolve => {
    let titles = [];

    proposalContainer
        .all(by.css('th:not(.small):not(.medium)'))
        .each(columnTitle => {
            columnTitle
                .getText()
                .then(title => {
                    titles.push(title);
            });
    });

    resolve(titles);
});

export const getProposalInfo = caseNumber => new Promise(resolve => {
    let proposalInfo = [];

    proposals
        .filter(proposal =>
            proposal
                .getAttribute('proposal-case-number')
                .then(proposalCaseNumber =>
                    proposalCaseNumber === caseNumber
                )
        )
        .each(proposal =>
            proposal
                .all(by.css('td'))
                .each(proposalColumn =>
                    proposalColumn
                        .getText()
                        .then(info =>
                            proposalInfo.push(info)
                        )
                )
        );

    resolve(proposalInfo);
});

export const getProposalViewValue = valueName => new Promise(resolve => {
    const proposalItems = '.proposal-detail-view__list .proposal-detail-view__item';
    const fieldValue = '.proposal-detail-view__value';

    const result = element
        .all(by.css(proposalItems))
        .filter((elm) =>
            elm
                .$('.proposal-detail-view__label')
                .getText()
                .then((text) => text === valueName
            )
        ).first();

    const fieldValueText = result
        .$(fieldValue)
        .getText();

    resolve(fieldValueText);
});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
