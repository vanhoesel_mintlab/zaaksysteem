const cherryPack = require('../../library/cherryPack');
const { DEV_SERVER_HOSTNAME, DEV_SERVER_PORT } = require('../../library/constants');

module.exports = cherryPack({
  development() {
    return {
      https: true,
      host: DEV_SERVER_HOSTNAME,
      port: DEV_SERVER_PORT,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    };
  },
});
