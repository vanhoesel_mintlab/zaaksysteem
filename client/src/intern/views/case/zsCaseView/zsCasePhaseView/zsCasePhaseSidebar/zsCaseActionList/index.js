import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import zsTooltipModule from './../../../../../../../shared/ui/zsTooltip';
import zsModalModule from './../../../../../../../shared/ui/zsModal';
import zsCaseActionFormModule from './zsCaseActionForm';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';

import controller from './CaseActionListController';
import template from './template.html';

export default angular
	.module('zsCaseActionList', [
		angularUiRouterModule,
		composedReducerModule,
		zsTooltipModule,
		zsModalModule,
		zsCaseActionFormModule,
		snackbarServiceModule
	])
	.component('zsCaseActionList', {
		bindings: {
			actions: '&',
			onActionAutomaticToggle: '&',
			onActionUntaint: '&',
			onActionTrigger: '&',
			requestor: '&',
			recipient: '&',
			assignee: '&',
			phases: '&',
			templates: '&',
			caseDocuments: '&',
			disabled: '&',
			phaseState: '&'
		},
		controller,
		template
	})
	.name;
