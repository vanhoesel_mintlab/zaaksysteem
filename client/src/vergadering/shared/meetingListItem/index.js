import angular from 'angular';
import template from './template.html';
import proposalItemListModule from './proposalItemList';
import resourceModule from '../../../shared/api/resource';
import rwdServiceModule from '../../../shared/util/rwdService';
import includes from 'lodash/includes';
import first from 'lodash/head';
import isArray from 'lodash/isArray';

import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.meetingListItem', [
			resourceModule,
			proposalItemListModule,
			rwdServiceModule
		])
		.directive('meetingListItem', [ '$state', 'resource', 'rwdService', 'dateFilter', ( $state, resource, rwdService, dateFilter ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					onToggleExpand: '&',
					isExpanded: '&',
					isGrouped: '&',
					meeting: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getDate = ( ) => {
						return dateFilter( ctrl.meeting().date, 'dd MMM yyyy');
					};

					ctrl.getTime = ( ) => {
						return ctrl.meeting().time;
					};

					ctrl.getTitle = ( ) => {
						return isArray(ctrl.meeting().label) ? first(ctrl.meeting().label) : ctrl.meeting().label || ctrl.meeting().instance.casetype.instance.name;
					};

					ctrl.getLocation = ( ) => {
						if (isArray(ctrl.meeting().location )) {
							return first(ctrl.meeting().location);
						}
						return ctrl.meeting().location;
					};

					ctrl.getChairman = ( ) => {
						return ctrl.meeting().chairman;
					};

					ctrl.getProposals = ( ) => {
						return !ctrl.isExpanded() ? [] : ctrl.meeting().children;
					};

					ctrl.getProposalsLength = ( ) => {
						return ctrl.meeting().children.length;
					};

					ctrl.toggleExpand = ( ) => {
						ctrl.onToggleExpand();
					};

					ctrl.isMeetingList = ( ) => {
						return !!($state.current.name.indexOf('meetingList') !== -1);
					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

				}],
				controllerAs: 'meetingListItem'

			};
		}
		])
		.name;
