import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const attributeTypes = ['enkelvoudige_keuze', 'meervoudige_keuze', 'keuzelijst'];
const testData = ['Woord', 'Woord met extra tekst', 'Nul', 'Een', 'Diakriët'];
const addressData = [
    {
        type: 'lower than',
        input: '6343AB',
        output: 'Lower than 6343AC'
    },
    {
        type: 'exact',
        input: '6343AC',
        output: 'Exact 6343AC'
    },
    {
        type: 'higher than',
        input: '6343AD',
        output: 'Higher than 6343AC'
    }
];

describe('when opening case 65 with attribute type testscenarios', () => {

    beforeAll(() => {

        navigate.as('admin', 65);

        openPhase('1');

    });

    for ( const attributeType in attributeTypes ) {

        const attributeToInput = $(`[data-name="voorwaarde_${attributeTypes[attributeType]}"]`);
        const attribureWithOutput = $(`[data-name="voorwaarde_${attributeTypes[attributeType]}_resultaat"]`);

        for ( const index in testData ) {

            describe(`and inputting the ${attributeTypes[attributeType]} attribute with ${testData[index]}`, () => {
            
                beforeAll(() => {
            
                    caseAttribute.inputAttribute(attributeToInput, attributeTypes[attributeType] === 'meervoudige_keuze' ? [Number(index) + 1] : Number(index) + 1);
            
                });
            
                it(`the recipient attribute value should equal ${testData[index]}`, () => {
            
                    expect(caseAttribute.getClosedValue(attribureWithOutput)).toEqual(testData[index]);
            
                });
            
            });

        }

    }

    for ( const index in addressData ) {

        describe(`and setting the address to ${addressData[index].type} 6343AC`, () => {

            beforeAll(() => {

                caseAttribute.inputAttribute($(`[data-name="voorwaarde_kenmerk_postcode_${Number(index) + 1}"]`), addressData[index].input);

            });

            xit(`the result of the rule should be ${addressData[index].output}`, () => {
    
                expect(caseAttribute.getClosedValue($(`[data-name="voorwaarde_kenmerk_postcode_resultaat_${Number(index) + 1}"]`))).toEqual(addressData[index].output);
        
            });

        });

    }

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
