package Zaaksysteem::Controller::API::v1::General::Config;
use Moose;

use Zaaksysteem::API::v1::ArraySet;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has '+namespace' => (
    default => 'config'
);

my @CONFIG_ITEM_WHITELIST = (qw(
    allocation_notification_template_id
    disable_dashboard_customization
    newrelic_application_id
    newrelic_license_key
    case_distributor_group
    case_distributor_role
));

=head1 NAME

Zaaksysteem::Controller::API::v1::General::Config - API v1 controller for configuration entries

=head1 DESCRIPTION

This controller returns a set of configuration items for the current Zaaksysteem instance.

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/general/config> URI namespace.

=cut

sub base : Chained('/api/v1/general/base') : PathPart('config') : CaptureArgs(0) : Scope('general') {
    my ($self, $c)      = @_;
    $c->stash->{set} = $self->get_config_items($c->model('DB::Config'));
}

=head2 instance_base

Reserves the C</api/v1/general/config/$name> URI namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $name) = @_;

    if (grep { $_ eq $name } @CONFIG_ITEM_WHITELIST) {
        $c->stash->{$self->namespace} = $c->model('DB::Config')->get_value($name);
    }
    else {
        $c->log->debug("Non-whitelisted configuration item requested");
    }

    if (!$c->stash->{$self->namespace}) {
        throw(
            'api/v1/general/config/not_found',
            "Unable to find configuration item '$name'",
        );
    }
}

=head2 list

Return a list of all configuration items

=head3 URL Path

C</api/v1/general/config>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head2 get

Retrieve a single configuration item.

=head3 URL Path

C</api/v1/general/config/CONFIG_ITEM_NAME>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $self->get_object($c);
}

=head2 create

This action is not implemented

=head3 URL Path

C</api/v1/general/config/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/config/create/not_allowed',
        'Creation of configuration entries is not allowed',
    );

}

=head2 delete

This action is not implemented

=head3 URL Path

C</api/v1/general/config/NAME/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/config/delete/not_allowed',
        'Deletion of configuration entries is not allowed',
    );
}

=head1 METHODS

=head2 get_config_items

Returns a L<Zaaksysteem::API::v1::ArraySet> instance of config items from the
C<config> table.

=cut

sub get_config_items {
    my ($self, $config_rs) = @_;

    my $items_rs = $config_rs->search(
        {
            parameter => \@CONFIG_ITEM_WHITELIST
        },
        {order_by => 'parameter'}
    );

    return Zaaksysteem::API::v1::ArraySet->new(
        content => [ $items_rs->all ],
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
