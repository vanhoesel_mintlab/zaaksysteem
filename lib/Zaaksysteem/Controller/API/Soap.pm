package Zaaksysteem::Controller::API::Soap;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Types qw(NonEmptyStr);

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

with qw(MooseX::Log::Log4perl);

=head2 soap

C</api/soap>

Generic SOAP Endpoint

urn:Centric/ITS/GBA/v1.0/BOAanvraag/Aanvraag

=cut

sub soap : Chained('/') : PathPart('api/soap') : Args(0) : SOAP('DocumentLiteral') {
    my ($self, $c) = @_;

    my $soapaction = $c->req->header('soapaction') // '';
    $soapaction    =~ s/\s*^"//;
    $soapaction    =~ s/"\s*$//;

    if (!NonEmptyStr->check($soapaction)) {
        throw("api/soap/soap-action", "No or empty SOAPAction defined in header")
    }

    my $dn          = $c->engine->env->{SSL_CLIENT_S_DN} || $c->req->header('x-client-ssl-s-dn');
    my $client_cert = $c->engine->env->{SSL_CLIENT_CERT} || $c->req->header('x-client-ssl-cert');

    my $interface  = try {
        $self->select_interface_from_input($c,
            xml             => $c->stash->{soap}->envelope(),
            soapaction      => $soapaction,
            schema          => $c->model('DB')->schema,
            client_cert     => $client_cert // '',
            client_s_dn     => $dn // '',
        );
    } catch {
        $c->log->info('Error: ' . $_);
        if (blessed($_) && $_->isa('BTTW::Exception::Base')) {
            $c->stash->{soap}->fault(
                {
                    code   => '500',
                    reason => $_->type,
                    detail => $_->message
                }
            );
        } else {
            $c->stash->{soap}->fault(
                {
                    code   => '500',
                    reason => 'error/unknown',
                    detail => "$_"
                }
            );
        }

        $c->detach;
    };

    ### For sysin, make sure entry contains the working interface:
    $c->stash->{entry}      = $interface;
    $c->forward('/sysin/interface/soap/process_soap');
}

=head2 select_interface_from_input

    my $interface = $self->select_interface_from_input(
        xml             => '<xml><bunch /><of /><it />',
        soapaction      => 'urn:VENDOR/ITS/GBA/v1.0/BOAanvraag/Aanvraag'
        schema          => $c->model('DB')->schema,
        client_cert     => 'client certificate',
        client_s_dn     => 'client_subject_dn',
    );

Returns a C<interface> row matching the given soapaction, given xml, and client certificate

All fields are required

=cut

define_profile select_interface_from_input => (
    required => {
        xml        => 'Str',
        soapaction => 'Str',
        schema     => 'Zaaksysteem::Schema',
    },
    optional => {
        client_s_dn => 'Str',
        client_cert => 'Str',
    },
    defaults => {
        client_s_dn => sub { return '' },
        client_cert => sub { return '' },
    },
);

my $SOAP_INTERFACES;

sub _get_soap_interface_names {
    my ($self, $schema) = @_;

    if ($SOAP_INTERFACES) {
        return $SOAP_INTERFACES;
    }
    my @list = Zaaksysteem::Backend::Sysin::Modules->list_modules_by_module_type($schema, 'soapapi');
    $SOAP_INTERFACES = [ map ({ $_->name } @list) ];
    return $SOAP_INTERFACES;

}

sub select_interface_from_input {
    my $self    = shift;
    my $c       = shift;
    my $params  = assert_profile({ @_ })->valid;
    my $schema  = $params->{schema};

    my @possible_interfaces = $schema->resultset('Interface')->search_active(
        { module => $self->_get_soap_interface_names($schema) },
    );

    for my $interface (@possible_interfaces) {
        my $valid;
        try {
            $valid = $interface->is_valid_soap_api(
                xml         => $params->{xml},
                soapaction  => $params->{soapaction},
            );
        } catch {
            # prevent ill configured interfaces from breaking the whole /api/soap
            # infrastructure
            $self->log->info("Error: $_");
        };

        next unless $valid;

        if ($interface->module_object->does('Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPAPI')) {
            $valid = $interface->check_client_certificate(
                client_s_dn => $params->{client_s_dn},
                client_cert => $params->{client_cert},
            );
            next unless $valid;
            $c->stash->{client_certificate_valid} = $valid;
        }
        return $interface;
    }

    throw(
        'api/soap/unknown_soap_action',
        sprintf(
            "No interface found for soapaction: '%s'",
            $params->{soapaction}
        )
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
