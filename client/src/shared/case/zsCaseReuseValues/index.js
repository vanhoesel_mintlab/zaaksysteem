import angular from 'angular';
import template from './template.html';
import transformAttributeValues from './../zsCaseRegistration/transformAttributeValues';
import first from 'lodash/first';
import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import pick from 'lodash/pick';
import filter from 'lodash/filter';
import keyBy from 'lodash/keyBy';
import assign from 'lodash/assign';
import './styles.scss';

export default
	angular.module('zsCaseReuseValues', [
	])
		.component('zsCaseReuseValues', {
			bindings: {
				casetypeReference: '<',
                casetypeFields: '<',
                casetypeName: '<',
				userId: '<',
                onRetrieveCase: '&'
			},
			controller: [ '$http', function ( $http ) {

				let ctrl = this,
                    loading = false,
                    loaded = false,
                    caseValues;

                // This helper function will format checkbox values retrieved from API
                // to a form that the new reg form understands.
                // This transformation might need to be done on a higher level
                // such as attrToVorm > formatAttributeValue. But for now, it will do.
                let transformCheckboxValues = ( values, attributes ) => {

                    let transformedValues = mapValues(attributes, ( attribute ) => {
                        return values[attribute.name] ? values[attribute.name].reduce( ( acc, val ) => {
                            let key = Object.keys(val)[0];

                            acc[key] = val[key];
                            return acc;
                        }, {})
                        : [];
                    });

                    return assign(
                        {},
                        values,
                        transformedValues
                    );
                };

                /*
                This function takes values retrieved from api/v1/case, and transforms them into
                the format that the new reg form understands.

                Logic is as follows:
                1. Take the casetype attributes from the currently loaded casetype.
                2. Filter not allowed attributes (specifically file and calendar types) out of the
                   values object so those are not processed and passed along to the backend when
                   submitting. This also ensure values that extraneous values that were present in
                   retrieved case but not in currently active casetype definition are filtered out.
                3. If value for attribute is not empty in values object, take the value out of array
                   container that the API wraps all values in
                4. Pass this prepared object to the transformAttributeValues function, which parses
                   value types in a form that the reg form can interpret
                5. If there are checkbox values in the retrieved values object, do an extra
                   transform pass on this value to ensure that values for this type arent duplicated.
                */

                let prepareValues = ( values, casetypeAttributes ) => {

                    let allowedAttributes = filter(casetypeAttributes, ( field ) => field.type !== 'file' && field.type !== 'calendar' && field.type !== 'calendar_supersaas' ),
                        checkboxAttributes = keyBy(filter(casetypeAttributes, ( field ) => field.type === 'checkbox'), 'name'),
                        filteredValues = pick(values, allowedAttributes.map( attribute => attribute.name ));

                    let caseVals =
                        transformAttributeValues(
                            casetypeAttributes,
                            pickBy(
                                mapValues(filteredValues, ( value ) => {
                                    return first(value) ? first(value) : '';
                                }),
                                identity
                            ),
                            'formatters'
                        );
                    
                    return checkboxAttributes ? transformCheckboxValues(caseVals, checkboxAttributes) : caseVals;

                };

                let loadCase = ( ) => {

                    loading = true;

                    $http({
                        url: '/api/v1/case',
                        method: 'GET',
                        params: {
                            zql: `SELECT {} FROM case WHERE (case.casetype = "${ctrl.casetypeReference}") AND (case.requestor.id = "${ctrl.userId}") NUMERIC ORDER BY case.number DESC LIMIT 1`
                        }
                    })
                    .then( data => {

                        let caseObj = first(get(data, 'data.result.instance.rows'));

                        if (caseObj) {
                            caseValues = prepareValues(get(caseObj, 'instance.attributes'), ctrl.casetypeFields);
                        }

                    })
                    .finally( ( ) => {

                        loading = false;

                    });

                };

                ctrl.getValues = ( ) => caseValues;
                
                ctrl.isLoading = ( ) => loading;

                ctrl.isLoaded = ( ) => loaded;

                ctrl.usePreviousCaseValues = ( ) => {

                    ctrl.onRetrieveCase({ $caseValues: caseValues });

                    loaded = true;

                };

                loadCase();

			}],
			template
		})
		.name;
