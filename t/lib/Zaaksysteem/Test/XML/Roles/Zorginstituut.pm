use utf8;
package Zaaksysteem::Test::XML::Roles::Zorginstituut;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::XML::Roles:;Zorginstituut - Zorginstituut XML helper tests

=head1 DESCRIPTION

Test Zorginstituut XML generation functions

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::XML::Roles::Zorginstituut

=cut


=head2 set_int_enum

Converts a product code to a valid value (1 => 01).

=cut

sub test_set_int_enum {
    throws_ok(
        sub {
            my $val = Zaaksysteem::XML::Roles::Zorginstituut::set_int_enum(undef, qw(01 02 03));
        },
        qr#xml/zorginstituut/set_enum/invalid#,
        "Undef is an invalid enum"
    );
}

sub test_encode_to_base64 {
    my $val;
    lives_ok(sub {
        $val = Zaaksysteem::XML::Roles::Zorginstituut::encode_to_base64('☃');
    });
    chomp($val);
    is($val, "4piD", "Unicode snowman found");
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
