package Zaaksysteem::Test::Auth::Credential::Token;

use Zaaksysteem::Test;

use Zaaksysteem::Auth::Credential::Token;

sub test_credential_glue {
    my $cred_module = Zaaksysteem::Auth::Credential::Token->new;

    isa_ok $cred_module, 'Zaaksysteem::Auth::Credential::Token';

    my $validate_token;
    my $interface_name;
    my $realm_info;
    my $user;

    my $session_model = mock_one(validate => sub {
        $validate_token = shift;
        return mock_one(id => 'subject');
    });

    my $interface_model = mock_one(find_by_module_name => sub {
        $interface_name = shift;
        return mock_one(id => 'interface');
    });

    my $model_mocks = {
        'Session::Invitation' => $session_model,
        'DB::Interface' => $interface_model
    };

    my $mocksie = mock_one(
        model => sub { return $model_mocks->{ shift() } },
    );

    my $mock_realm = mock_one(
        config => sub { return { interface => 'foo' }; },
        find_user => sub {
            my $c = shift;
            $realm_info = shift;

            return 'user';
        }
    );

    lives_ok {
        $user = $cred_module->authenticate($mocksie, $mock_realm, { token => 'abc' });
    } 'authenticate call does not except';

    is $validate_token, 'abc', 'token passthru';
    is $interface_name, 'foo', 'interface name passthru';
    is $user, 'user', 'user retrieved from store';
    is_deeply $realm_info, {
        source => 'interface',
        subject_id => 'subject'
    }, 'realm info passthru';

    # Model that returns undefined invitations
    $model_mocks->{ 'Session::Invitation' } = mock_one(validate => sub {
        return undef;
    });

    my $undef_invitation = exception {
        $cred_module->authenticate($mocksie, $mock_realm, { token => 'abc' });
    };

    isa_ok $undef_invitation, 'BTTW::Exception::Base', 'bttw-exception caught';
    is $undef_invitation->type, 'auth/token/token_invalid', 'undef invitation object implies invalid token';

    # Restore model
    $model_mocks->{ 'Session::Invitation' } = $session_model;

    # Setup next failure
    $model_mocks->{ 'DB::Interface' } = mock_one(find_by_module_name => sub {
        return undef;
    });

    my $undef_interface = exception {
        $cred_module->authenticate($mocksie, $mock_realm, { token => 'abc' });
    };

    isa_ok $undef_interface, 'BTTW::Exception::Base', 'bttw-exception caught';
    is $undef_interface->type, 'auth/token/interface_not_found', 'undef interface exception type';

    $model_mocks->{ 'DB::Interface' } = $interface_model;

    my $mock_shadow_realm_one = mock_one(
        config => undef
    );

    dies_ok { $cred_module->authenticate($mocksie, $mock_shadow_realm_one, { token => 'abc' }) }
        'invalid realm configuration dies';

    my $mock_shadow_realm_two = mock_one(
        config => sub { return { interface => 'foo' }; },
        find_user => sub { return undef }
    );

    my $undef_user = exception {
        $cred_module->authenticate($mocksie, $mock_shadow_realm_two, { token => 'abc' });
    };

    isa_ok $undef_user, 'BTTW::Exception::Base', 'bttw-exception caught';
    is $undef_user->type, 'auth/token/user_not_found', 'undef user exception type';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
