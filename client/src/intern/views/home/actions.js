import angular from 'angular';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import reject from 'lodash/reject';
import find from 'lodash/find';
import omit from 'lodash/omit';

export default
	angular.module('Zaaksysteem.intern.home.zsDashboard.actions', [
		mutationServiceModule
	])
	.run(['mutationService', ( mutationService ) => {

		mutationService.register( {
			type: 'create_widget',
			request: ( mutationData ) => {
				return {
					url: '/api/v1/dashboard/widget/create',
					data: mutationData
				};
			},
			reduce: ( data, mutationData ) => {
				let obj =
					{
						instance: mutationData,
						type: 'widget',
						reference: null
					};

				return data.concat(obj);
			}
		} );

		mutationService.register( {
			type: 'delete_widget',
			request: ( mutationData ) => {
				return {
					url: `/api/v1/dashboard/widget/${mutationData.uuid}/delete`
				};
			},
			reduce: ( data, mutationData ) => {
				return reject( data, {
					reference: mutationData.uuid
				});
			}
		} );

		mutationService.register( {
			type: 'update_widget',
			request: ( mutationData ) => {
				return {
					url: `/api/v1/dashboard/widget/${mutationData.uuid}/update`,
					data: mutationData
				};
			},
			reduce: ( data, mutationData ) => {
				
				let result = data.map(( widget ) => {

					let item = widget;
					
					if (widget.reference === mutationData.uuid) {
						item = widget.merge(
							{ instance: omit(mutationData, 'uuid') },
							{ deep: true }
						);
					}

					return item;
				});

				return result;
			}
		} );

		mutationService.register( {
			type: 'bulk_update',
			request: ( mutationData ) => {
				return {
					url: '/api/v1/dashboard/widget/bulk_update',
					data: mutationData
				};
			},
			reduce: ( data, mutationData ) => {

				let result = data.map(( widget ) => {

					let update = find(mutationData.updates, { uuid: widget.reference });
					
					return widget.merge({ instance: widget.instance.merge(update) });
				});

				return result;
			}
		} );

	}])
	.name;
