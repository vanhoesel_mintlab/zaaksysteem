import {
	getValue,
	getAddressValue,
	getCompanyTypeValue,
	getEmailValue,
	getSourceValue
} from './format';

const summary = true;

export default [
	{
		summary,
		key: 'external_subscription',
		label: 'Bron',
		getValue: getSourceValue
	},
	{
		summary,
		key: 'coc_number',
		label: 'KvK-nummer',
		getValue
	},
	{
		key: 'coc_location_number',
		label: 'Vestigingsnummer',
		getValue
	},
	{
		summary,
		key: 'company',
		label: 'Handelsnaam',
		getValue
	},
	{
		summary,
		key: 'company_type',
		label: 'Rechtsvorm',
		getValue: getCompanyTypeValue
	},
	{
		summary,
		key: 'address_residence',
		label: 'Vestigingsadres',
		getValue: getAddressValue
	},
	{
		key: 'phone_number',
		label: 'Nummer vaste telefoon',
		getValue
	},
	{
		key: 'mobile_phone_number',
		label: 'Nummer mobiele telefoon',
		getValue
	},
	{
		key: 'email_address',
		label: 'E-mailadres',
		getValue: getEmailValue
	}
];
