package Zaaksysteem::DB::HStore;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::DB::HStore - Inflator/deflator for PostgreSQL "hstore" fields

=head1 SYNOPSIS

    my $string = Zaaksysteem::DB::HStore::encode({some => 'hashref'});
    my $hashref = Zaaksysteem::DB::HStore::decode('"foo"=>"bar"');

=cut

sub encode {
    my $hashref = shift;

    my %rv;
    for my $key (keys %$hashref) {
        my $value = $hashref->{$key};

        $key   =~ s/(["\\])/\\$1/g;
        $value =~ s/(["\\])/\\$1/g
            if defined $value;

        $rv{$key} = $value;
    }

    return join(
        ",",
        map {
            defined $rv{$_}
                ? "\"$_\"=>\"$rv{$_}\""
                : "\"$_\"=>NULL"
        } sort keys %rv);
}

sub decode {
    my $hstore = shift;

    my %rv;

    while ($hstore =~ m/
        (")?                             # Maybe opening quote (1)
            (?<key>
                (?(1).*?|[^\s,=>]+)      # If quoted: anything, else: anything but special chars
            )
        (?(1)\1|)                        # Maybe closing quote (1)
        \s* => \s*                       # "=>"
        (?<value_quoted>")?              # Maybe opening quote (3)
            (?<value>
                (?(3).*?|[^\s,=>]+)      # If quoted: anything, else: anything but special characters
            )
        (?(3)\3|)                        # Maybe closing quote (3)
        \s*(?:,|$)                       # Followed by a comma, or end-of-line or end-of-string
    /gx) {
        my $key = $+{key};
        my $value = $+{value};

        $key   =~ s/\\(["\\])/$1/g;
        $value =~ s/\\(["\\])/$1/g;

        $value = undef
            if($value eq 'NULL' && !$+{value_quoted});

        $rv{$key} = $value;
    }

    return \%rv;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
