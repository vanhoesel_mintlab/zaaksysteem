export const resultOptions = element.all(by.css('.result-options li'));

export const getResult = () => new Promise(resolve => {
    resultOptions
        .each(resultOption =>
            resultOption
                .$('input')
                .isSelected()
                .then(selected => {
                    if (selected) {
                        resultOption
                            .getText()
                            .then(text =>
                                resolve(text)
                            );
                    }
                })
        );
});

export const getResultStatus = () => new Promise(resolve => {
    resultOptions
        .get(0)
        .$('input')
        .getAttribute('disabled')
        .then(disabled =>
            resolve(disabled ? 'disabled' : 'enabled')
        );
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
