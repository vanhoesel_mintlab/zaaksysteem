import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const requestorType00 = $('[data-name="aanvrager_type_00"]');
const requestorType01 = $('[data-name="aanvrager_type_01"]');
const requestorType10 = $('[data-name="aanvrager_type_10"]');
const requestorType11 = $('[data-name="aanvrager_type_11"]');

describe('when opening case 50 with the citizen as requestor', () => {

    beforeAll(() => {

        navigate.as('admin', 50);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

describe('when opening case 51 with the company as requestor', () => {

    beforeAll(() => {

        navigate.as('admin', 51);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

describe('when opening case 52 with the employee as requestor', () => {

    beforeAll(() => {

        navigate.as('admin', 52);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
