package Zaaksysteem::Object::Types::Employee;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation Zaaksysteem::BR::Subject::Types::Employee);

use BTTW::Tools;
use Zaaksysteem::Types qw(JSONBoolean Timestamp);

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use constant SUBJECT_TYPE => 'employee';
use constant SCHEMA_TABLE => 'Subject';

=head1 NAME

Zaaksysteem::Object::Types::Employee - Returns a subject of type "employee"

=head1 DESCRIPTION

Subject object, where bedrijven, medewerkers and natuurlijk personen are stored.

=head1 EMPLOYEE ATTRIBUTES

=head2 initials

The initials of a person

=cut

has initials => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Initials',
    required => 0,
);

=head2 first_names

The first names of a person

=cut

has first_names => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'First names',
    required => 0,
);

=head2 gender

The gender of a person

=cut

has gender => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Gender',
    required => 0,
);

=head2 surname

The result of use of the name. Real achternaam

=cut

has surname => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Surname',
    required => 0,
);

=head2 display_name

The displayed name of this employee

=cut

has display_name => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Display name',
    required => 0,
);

=head2 phone_number

This attribute holds a stringified telephonenumber of the subject.

B<Beware>: it cannot be assumed the number is a 'valid' or 'complete'
phonenumber, in some instances the extentionnumber of a landline is used.

=cut

has phone_number => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => 'Telephone number',
);

=head2 email_address

The e-mail address of the employee.

=cut

has email_address => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => 'E-mail address'
);

=head2 settings

This attribute contains free-form settings for and about the user. This may
include KCC-settings, favourite saved_search references, etc.

=cut

has settings => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[OA]],
    label => 'User settings'
);

=head1 RELATIONS

=head2 positions

Holds a collection of L<Zaaksysteem::Object::Types::Position> instances for
which belong to the employee.

=cut

has positions => (
    is => 'rw',
    type => 'position',
    embed => 1,
    label => 'Organisational position(s)',
    traits => [qw[OR]],
    isa_set => 1
);

=head2 authentication

=cut

has authentication => (
    is       => 'rw',
    type     => 'authentication',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Authentication parameters'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
