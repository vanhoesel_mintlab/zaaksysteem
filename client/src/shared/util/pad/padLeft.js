/**
 * @param {string} input
 * @param {string} pattern
 * @returns {string}
 */
export default function padLeft( input, pattern ) {
	[ input, pattern ].forEach(param => {
		const type = typeof param;

		if (type !== 'string') {
			throw new TypeError(`expected ${type} to be a string`);
		}
	});

	if (input.length < pattern.length) {
		const end = pattern.length - input.length;
		const prefix = pattern.slice(0, end);

		return `${prefix}${input}`;
	}

	return input;
}
