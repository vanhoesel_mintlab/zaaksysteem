import navigate from './../../../../functions/common/navigate';
import verifyRestricted from './../../../../functions/common/auth/verifyRestricted';

describe('when logging in as behandelaar', () => {

    beforeAll(() => {

        navigate.as('behandelaar');

    });

    describe('and opening the user page', () => {

        it('should show the no access page', () => {

            expect(verifyRestricted('/medewerker')).toBe(true);

        });

    });

    describe('and opening the log page', () => {

        it('should show the no access page', () => {

            expect(verifyRestricted('/beheer/logging')).toBe(true);

        });

    });

    describe('and opening the transaction page', () => {

        it('should show the no access page', () => {

            expect(verifyRestricted('/beheer/sysin/transactions')).toBe(true);

        });

    });

    describe('and opening the configuration page', () => {

        it('should show the no access page', () => {

            expect(verifyRestricted('/beheer/configuration')).toBe(true);

        });

    });

});

describe('when logging in as zaaksysteembeheerder', () => {

    beforeAll(() => {

        navigate.as('zaaksysteembeheerder');

    });

    describe('and opening the user page', () => {

        it('should not show the no access page', () => {

            expect(verifyRestricted('/medewerker')).toBe(false);

        });

    });

    describe('and opening the log page', () => {

        it('should not show the no access page', () => {

            expect(verifyRestricted('/beheer/logging')).toBe(false);

        });

    });

    describe('and opening the transaction page', () => {

        it('should not show the no access page', () => {

            expect(verifyRestricted('/beheer/sysin/transactions')).toBe(false);

        });

    });

    describe('and opening the configuration page', () => {

        it('should not show the no access page', () => {

            expect(verifyRestricted('/beheer/configuration')).toBe(false);

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
