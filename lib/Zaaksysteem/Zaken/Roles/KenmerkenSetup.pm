package Zaaksysteem::Zaken::Roles::KenmerkenSetup;
use Moose::Role;

use BTTW::Tools;
with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::Timer
);

around '_create_zaak' => sub {
    my $orig                = shift;
    my $self                = shift;
    my ($opts)              = @_;
    my ($zaak_kenmerken);

    my $zaak = $self->$orig(@_);

    ### Fix kenmerken
    my %kenmerken = map { my ($id, $values) = each %$_; $id => $values } @{ $opts->{kenmerken} };

    my @manual_attributes = keys %kenmerken;

    ### Mangle kenmerken with defaults
    my $mangled_properties  = $zaak
                            ->zaaktype_node_id
                            ->zaaktype_kenmerken
                            ->mangle_defaults(
                                \%kenmerken
                            );


    # mangle_defaults may return a hashref
    my $ref = ref $mangled_properties;
    if ($ref eq 'HASH' && keys %$mangled_properties) {
        $zaak->zaak_kenmerken->update_fields({
            zaak                        => $zaak,
            new_values                  => $mangled_properties,
            set_values_except_for_attrs => \@manual_attributes,
        });
    }

    # Ensure the case gets a location if configured.
    my $geolatlon = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.value_type' => 'geolatlon',
            'bibliotheek_kenmerken_id.id' => [ keys %kenmerken ]
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    )->first;

    my $set_location = $geolatlon ? $geolatlon->properties->{ map_case_location } : undef;
    my $value = $geolatlon ? $kenmerken{ $geolatlon->get_column('bibliotheek_kenmerken_id') } : undef;

    # Some callers of _create_zaak wrap values in arrays even when value
    # multiplicity is impossible.
    $value = ref $value eq 'ARRAY' ? $value->[0] : $value;

    if (defined $set_location && $value) {
        my $schema = $zaak->result_source->schema;
        my $queue_items = $schema->default_resultset_attributes->{ queue_items };
        my $case = $zaak->object_data;

        my ($lat, $lon) = split m[,], $value;

        push @{ $queue_items }, $case->queues->create_item(
            'update_case_location',
            {
                object_id => $case->id,
                label => 'Zaaklocatie instellen',
                data => {
                    latitude => $lat,
                    longitude => $lon
                }
            }
        );
    }

    return $zaak;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
