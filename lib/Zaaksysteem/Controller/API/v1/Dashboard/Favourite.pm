package Zaaksysteem::Controller::API::v1::Dashboard::Favourite;

use Moose;
use DateTime;

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Favourite - APIv1 controller for Dashboard Favourites

=head1 DESCRIPTION

This is the controller API class for C<api/v1/dashboard/favourite>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Dashboard::Favourites>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Dashboard::Favourites>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/dashboard/base') : PathPart('favourite') : CaptureArgs(1) : Scope('favourite') {
    my ($self, $c, $type)       = @_;

    $c->stash->{reference_type} = $type;
    $c->stash->{usersettings}   = $c->user->usersettings;
}


=head2 list

=head3 URL Path

C</api/v1/dashboard/widget>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $self->_get_all_favourites_by_type($c, $c->stash->{reference_type});
}

sub _get_all_favourites_by_type {
    my ($self, $c, $type) = @_;

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content     => [ $c->stash->{usersettings}->favourites->search({reference_type => $type}) ],
    );

    return $set->init_paging($c->request);
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve favourite from user settings
    ($c->stash->{ favourite }) = $c->stash->{usersettings}->favourites->search(
        {
            reference_type  => $c->stash->{reference_type},
            id              => $uuid,
        }
    );
}

=head2 get

=head3 URL Path

C</api/v1/dashboard/favourite/TYPE/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ favourite };
}


=head2 create

=head3 URL Path

C</api/v1/dashboard/favourite/TYPE/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c)      = @_;

    throw('api/v1/dashboard/favourite/no_post', 'HTTP Post method request for mutation calls') unless
        lc($c->req->method) eq 'post';

    my $params          = $c->stash->{usersettings}->favourites->validate_dynamic_class(
        'Zaaksysteem::UserSettings::Favourites::Row',
        {
            %{ $c->req->params },
            reference_type => $c->stash->{reference_type}
        }
    )->valid;

    try {
        $c->stash->{usersettings}->favourites->create($params);
        $c->user->settings($c->stash->{usersettings}->to_settings);
        $c->user->update;
    } catch {
        throw(
            'api/v1/dashboard/favourites/create_error',
            'There was a problem creating this favourite: ' . $_
        );
    };

    $c->stash->{ result }   = $self->_get_all_favourites_by_type($c, $c->stash->{reference_type});
}

=head2 update

=head3 URL Path

C</api/v1/dashboard/widget/TYPE/UUID/update>

=cut


sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c)      = @_;

    throw('api/v1/dashboard/widget/no_post', 'HTTP Post method request for mutation calls') unless
        lc($c->req->method) eq 'post';

    my $params          = $c->stash->{usersettings}->favourites->validate_dynamic_class(
        'Zaaksysteem::UserSettings::Favourites::Row',
        {
            %{ $c->req->params },
            reference_type => $c->stash->{reference_type}
        }
    )->valid;

    try {
        $c->stash->{usersettings}->favourites->update($c->stash->{favourite}, $params);
        $c->user->settings($c->stash->{usersettings}->to_settings);
        $c->user->update;
    } catch {
        throw(
            'api/v1/dashboard/favourites/update_error',
            'There was a problem updating this favourite: ' . $_
        );
    };


    $c->stash->{ result }   = $self->_get_all_favourites_by_type($c, $c->stash->{reference_type});
}


=head2 bulk_update

=head3 URL Path

C</api/v1/dashboard/favourites/TYPE/bulk_update>

=cut


sub bulk_update : Chained('base') : PathPart('bulk_update') : Args(0) : RW {
    my ($self, $c)      = @_;

    throw('api/v1/dashboard/favourites/no_post', 'HTTP Post method request for mutation calls') unless
        lc($c->req->method) eq 'post';

    throw('api/v1/dashboard/favourites/invalid', 'Need at least the json param "updates" containing an array') unless
        ($c->req->params->{updates} && ref $c->req->params->{updates} eq 'ARRAY');

    ### Pre-assert all profiles
    my @updates;
    for my $rawparams (@{ $c->req->params->{updates} }) {
        my $params          = $c->stash->{usersettings}->favourites->validate_dynamic_class(
            'Zaaksysteem::UserSettings::Favourites::Row',
            {
                %{ $rawparams },
                reference_type => $c->stash->{reference_type}
            }
        )->valid;

        push(@updates, $params);
    }

    try {
        $c->model('DB')->txn_do(
            sub {
                for my $params (@updates) {
                    ### Get row
                    my ($row) = $c->stash->{usersettings}->favourites->search(
                        {
                            reference_type  => $c->stash->{reference_type},
                            id              => $params->{id},
                        }
                    );

                    throw(
                        'api/v1/dashboard/favourites/bulk_update/row_not_found',
                        "Row with ID: " . $params->{id} . " not found." 
                    ) unless $row;

                    try {
                        $c->stash->{usersettings}->favourites->update($row, $params);
                    } catch {
                        throw(
                            'api/v1/dashboard/favourites/fault',
                            'There was a problem updating this favourite: ' . $_
                        );
                    };
                }

                $c->user->settings($c->stash->{usersettings}->to_settings);
                $c->user->update;
            }
        )
    } catch {
        throw(
            'api/v1/dashboard/favourites/bulk_update_error',
            'There was a problem bulk updating these favourites: ' . $_
        );
    };

    $c->stash->{ result }   = $self->_get_all_favourites_by_type($c, $c->stash->{reference_type});
}

=head2 delete

=head3 URL Path

C</api/v1/dashboard/favourites/TYPE/UUID/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    throw('api/v1/dashboard/widget/no_post', 'HTTP Post method request for mutation calls') unless
        lc($c->req->method) eq 'post';

    try {
        $c->stash->{usersettings}->favourites->delete($c->stash->{favourite});
        $c->user->settings($c->stash->{usersettings}->to_settings);
        $c->user->update;
    } catch {
        throw(
            'api/v1/dashboard/favourites/fault',
            'There was a problem deleting this favourite: ' . $_
        );
    };


    $c->stash->{ result }   = $self->_get_all_favourites_by_type($c, $c->stash->{reference_type});


}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
