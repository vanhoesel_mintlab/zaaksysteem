package Zaaksysteem::Controller::API::v1::Dashboard;

use Moose;
use DateTime;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Dashboard - APIv1 controller for Dashboard objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/dashboard>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Dashboard>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Dashboard>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('dashboard') : CaptureArgs(0) : Scope('dashboard') {}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
