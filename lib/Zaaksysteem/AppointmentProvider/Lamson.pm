package Zaaksysteem::AppointmentProvider::Lamson;
use Moose;
use namespace::autoclean;

with qw(
    Zaaksysteem::AppointmentProvider::Roles::TestConnection
    Zaaksysteem::AppointmentProvider
    MooseX::Log::Log4perl
);

=head1 NAME

Zaaksysteem::AppointmentProvider::Lamson - Appointment provider plugin for Lamson

=head1 DESCRIPTION

Adds support for using the L<Lamson|http://www.lamson.nl/> application for
appointments.

=cut

use DateTime::Format::ISO8601;
use JSON::XS;
use BTTW::Tools;
use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::Types qw(NonEmptyStr Boolean);
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 lamsonv1_endpoint

The https endpoint the Lamson application can be contacted at.

=cut

has lamsonv1_endpoint => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 lamsonv1_ca_certificate_use_system

Boolean value. If set to true, the system certificate store will be used.

=cut

has lamsonv1_ca_certificate_use_system => (
    is       => 'ro',
    isa      => Boolean,
    required => 1,
);

=head2 lamsonv1_ca_certificate

Path (or something that stringifies to the path) of the CA certificate to use
to connect to the Lamson application.

=cut

has lamsonv1_ca_certificate => (
    is       => 'ro',
    required => 1,
);

=head2 lamsonv1_channel

Configured value of "channel", an unexplained value from the Lamson API.

=cut

has lamsonv1_channel => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 lamsonv1_locations

A list of Lamson locations (with location ids).

=cut

has lamsonv1_locations => (
    is      => 'ro',
    isa     => 'ArrayRef[HashRef]',
    default => sub { [] },
);

=head2 ua

L<LWP::UserAgent> instance used to connect to the Lamson API. By default, a new
instance is created, with trace logging hooks for request and response.

=cut

has ua => (
    is       => 'rw',
    isa      => 'LWP::UserAgent',
    default  => sub {
        my $self = shift;
        
        my $ca_file = $self->lamsonv1_ca_certificate;
        my $ua = LWP::UserAgent->new(
            cookie_jar => {},
            ssl_opts => {
                verify_hostname => 1,
                $ca_file
                    ? (SSL_ca_file => "$ca_file")
                    : (SSL_ca_path => '/etc/ssl/certs'),
            },
        );

        if ($self->log->is_trace) {
            $ua->add_handler("request_send",  sub { $self->log->trace(shift->dump); return; });
            $ua->add_handler("response_done", sub { $self->log->trace(shift->dump); return; });
        }

        return $ua;
    },
    lazy     => 1,
    required => 0,
);

=head2 json

Reusable JSON::XS object, for encoding/decoding JSON.

=cut

has json => (
    is      => 'rw',
    isa     => 'JSON::XS',
    default => sub {
        return JSON::XS->new();
    },
    lazy => 1,
);

=head1 METHODS

=head2 shortname

Always returns the string C<lamsonv1>.

=cut

sub shortname { "lamsonv1" }

=head2 label

Always returns the string C<Lamson>.

=cut

sub label { "Lamson" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_lamsonv1_endpoint',
        type        => 'text',
        label       => 'Lamson API endpoint',
        required    => 1,
        description => '<p>URL waarop de Lamson API te bereiken is.</p>',
        data        => { pattern => '^https:\/\/.+' },
    ),
    ca_certificate(name => 'lamsonv1_ca_certificate'),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_lamsonv1_channel',
        type        => 'text',
        label       => 'Lamson API "kanaal"',
        required    => 0,
        default     => 0,
        data        => { pattern => '^\d+$' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_lamsonv1_locations',
        type        => 'multiple',
        label       => "Locaties",
        required    => 0,
        data => {
            fields => [
                {
                    name        => 'location_id',
                    type        => 'text',
                    label       => 'Locatie-id',
                    description => "<p>Nummer van de locatie in de Lamson-applicatie.</p>",
                    data        => { pattern => '^\d+$' },
                },
                {
                    name        => 'location_name',
                    type        => 'text',
                    label       => 'Locatie-naam',
                    description => "<p>Een korte beschrijvende naam voor deze locatie die getoond wordt bij het instellen van een kenmerk in de catalogus.</p>",
                },
            ],
        },
    ),

);

=head2 configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) that are necessary to configure the appointment interface to
use the Lamson application's API.

=cut

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

=head2 get_location_list

Return the configured list of locations.

=cut

sub get_location_list {
    my ($self) = @_;

    return [
        map {
            {
                id => $_->{location_id},
                label => $_->{location_name},
            }
        } @{ $self->lamsonv1_locations }
    ];
}

=head2 get_product_list

Return a list of all products the Lamson API offers for the configured location/channel.

=cut

sub get_product_list {
    my ($self, $location_id) = @_;

    # Lamson has a bit of a strange setup: you can't request a list of products
    # unless you have a "channel", location and department.
    #
    # We can request a list of departments, but not a list of channels or
    # locations: those will have to be set in the interface configuration.
    my %departments_data = $self->_lamson_http_request(
        'GET',
        'Department/GetAllDepartments',
    );
    my $departments = $self->json->decode($departments_data{response}->decoded_content);

    my @rv;
    for my $department (@$departments) {
        my %product_data = $self->_lamson_http_request(
            'GET',
            'Products/GetProducts',
            {
                channel      => $self->lamsonv1_channel,
                locationId   => $location_id,
                departmentId => $department->{Id},
            }
        );
        my $products = $self->json->decode($product_data{response}->decoded_content);

        for my $product (@$products) {
            push @rv, {
                id    => join("/", $product->{ProductId}, $product->{DepartmentId}),
                label => "$product->{ProductName} ($department->{Name})",
            };
        }
    }

    return \@rv;
}

=head2 get_dates

Retrieve a list of possible dates for appointments.

=head3 Arguments

=over

=item * product_id

Product to retrieve dates for

=item * location_id

Location to retrieve dates for

=back

=cut

sub get_dates {
    my ($self, $product_id_combined, $location_id) = @_;

    my ($product_id, $department_id) = split /\//, $product_id_combined;

    # Statically assume all appointments are for one person
    my %dates_data = $self->_lamson_http_request(
        'GET',
        'TimeSlots/GetAvailableDates',
        {
            "Products[0][ProductId]"       => $product_id,
            "Products[0][NumberOfPersons]" => 1,
        }
    );
    my $dates = $self->json->decode($dates_data{response}->decoded_content);

    return [
        map {
            my $date = DateTime::Format::ISO8601->parse_datetime($_->{Dates});
            $date->ymd;
        } @$dates
    ];
}

=head2 get_timeslots

Return a list of available time slots for the selects product/location.

=cut

sub get_timeslots {
    my ($self, $date, $product_id_combined, $location_id) = @_;

    my ($product_id, $department_id) = split /\//, $product_id_combined;

    # Statically assume all appointments are for one person
    my %slots_data = $self->_lamson_http_request(
        'GET',
        'TimeSlots/GetTimeSlots',
        {
            "Date"                         => $date->ymd,
            "Products[0][ProductId]"       => $product_id,
            "Products[0][NumberOfPersons]" => 1,
        }
    );
    my $slots = $self->json->decode($slots_data{response}->decoded_content);

    my @rv;
    for my $slot (@$slots) {
        my $start = DateTime::Format::ISO8601->parse_datetime($slot->{Start});
        my $end = DateTime::Format::ISO8601->parse_datetime($slot->{End});

        # Sometimes Lamson returns timeslots for the wrong day. Filter them out.
        next if $start->ymd ne $date->ymd;

        push @rv, {
            start_time => $start->set_time_zone('UTC')->iso8601 . 'Z',
            end_time   => $end->set_time_zone('UTC')->iso8601 . 'Z',
            plugin_data => {
                product_id    => $product_id,
                department_id => $department_id,
                location_id   => $location_id,
            },
        };
    }

    return \@rv;
}

=head2 book_appointment

Create a new appointment in the Lamson application.

=cut

sub book_appointment {
    my ($self, $appointment_data, $requestor) = @_;

    for (qw(product_id department_id location_id)) {
        throw("appointmentprovider/lamsonv1/invalid_data", "Invalid appointment: missing field '$_'")
            unless exists $appointment_data->{plugin_data}{$_};
    }

    unless($requestor->subject_type eq 'person') {
        throw(
            "appointmentprovider/lamsonv1/not_a_person",
            sprintf(
                "The Lamson calendar only works for persons. Supplied: '%s'",
                $requestor->subject_type,
            )
        );
    }

    my $subject = $requestor->subject;

    my @controls = (
        {
            Name                => 'gender',
            Label               => 'gender',
            CustomerData        => 'true',
            Required            => 'true',
            Text                => 1,
        },
        {
            Name                => 'firstname',
            Label               => 'voornaam',
            CustomerData        => 'true',
            Text                => $subject->first_names,
        },
        {
            Name                => 'lastname',
            Label               => 'achternaam',
            CustomerData        => 'true',
            Text                => $subject->surname,
        },
        {
            Name                => 'prefix',
            Label               => 'voorletter',
            CustomerData        => 'true',
            Text                => $subject->initials,
        },
        {
            Name                => 'infix',
            Label               => 'tussenvoegsel',
            CustomerData        => 'true',
            Text                => $subject->prefix,
        },

        # Undocumented: this is a required field "inside" Lamson
        # If it's not specified, creating the appointment will not work and
        # return 'null'.
        {
            Name                => 'email',
            Label               => 'E-mailadres',
            CustomerData        => 'true',
            Text                => $subject->email_address,
        },
    );

    my %customer_data = $self->_lamson_http_request(
        'POST',
        'Appointments/CreateCustomer',
        {
            controls => \@controls
        }
    );

    # The only call that doesn't return JSON data:
    my $customer_id = $customer_data{response}->decoded_content;

    my $start = $appointment_data->{start_time}->set_time_zone('UTC');
    my $end   = $appointment_data->{end_time}->set_time_zone('UTC');

    my %lamson_data = $self->_lamson_http_request(
        'POST',
        'Appointments/SaveAppointments',
        {
            ProductId  => $appointment_data->{plugin_data}{product_id} + 0,
            LocationId => $appointment_data->{plugin_data}{location_id} + 0,
            DepartmentId => $appointment_data->{plugin_data}{department_id} + 0,

            StartTime  => $start->strftime('%F %T+00:00'),
            EndTime    => $end->strftime('%F %T+00:00'),
            CustomerId => $customer_id + 0,

            NumberOfPersons        => 1,

            SelectedMultiProdUsers => "",

            appointmentFields => {
                controls => \@controls
            },
        },
    );
    my $lamson_appointment = $self->json->decode($lamson_data{response}->decoded_content);

    # {"AppointmentGuid":"825cbadd-22f2-4267-96e7-4472b26902b8","BookingNumber":"566346"}
    for my $field (keys %$lamson_appointment) {
        $appointment_data->{plugin_data}{$field} = $lamson_appointment->{$field};
    }

    return Zaaksysteem::Object::Types::Appointment->new(
        %$appointment_data,
        plugin_type => $self->shortname
    );
}

=head2 delete_appointment

Delete an existing appointment from the Lamson application.

=cut

sub delete_appointment {
    my ($self, $appointment) = @_;

    # API docs say "appointmentId", they mean the appointment *GUID*.
    my %delete_data = $self->_lamson_http_request(
        'POST' => sprintf(
            'Appointments/CancelAppointment?appointmentId=%s',
            $appointment->plugin_data->{AppointmentGuid},
        ),
        {},
    );

    if ($delete_data{response}->decoded_content eq 'true') {
        return {
            success => \1,
        };
    }

    throw(
        "appointmentprovider/lamsonv1/delete_failed",
        sprintf(
            "Appointment deletion failed: %s",
            $delete_data{response}->decoded_content
        ),
    );
}

=head2 _test_host_port

Return the configured endpoint's host and port.

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_host_port {
    my $self = shift;
    my $uri = URI->new($self->lamsonv1_endpoint);
    return ($uri->host, $uri->port);
}

=head2 _test_certificate

Return the configured CA certificate. 

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_certificate {
    my $self = shift;

    return (
        ca => $self->lamsonv1_ca_certificate,
    );
}

=head2 _lamson_http_request

Small wrapper around L<LWP::UserAgent> to make calling the Lamson API easier.

=cut

sub _lamson_http_request {
    my ($self, $method, $url_part, $params) = @_;

    my $url = URI->new($self->lamsonv1_endpoint . "/Api/" . $url_part);

    my $post_content;
    if ($method eq 'POST') {
        $post_content = encode_json($params);
    }
    else {
        $url->query_form($params);
    }

    my $request_method = HTTP::Request::Common->can($method);

    my $request = $request_method->(
        $url,
        "Accept" => "application/json",
        ($method eq 'POST')
            ? (
                "Content-Type" => "application/json",
                "Content"      => $post_content
              )
            : ()
    );

    my $response = $self->ua->request($request);

    if ($response->is_success) {
        return (
            request  => $request,
            response => $response,
        );
    }
    else {
        throw(
            "appointmentprovider/lamsonv1/http_error",
            "Lamson v1 - HTTP error: " . $response->code,
            {
                request  => $request,
                response => $response,
            }
        );
    }
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
