import get from 'lodash/get';

/*
 * Flip the required bit if certain fields are set for remote
 * searches
 *
 * @lookup   - hash/dictionary of fields
 * @required - array with fields that lift the required bit
 */

export default function isRequired(lookup, required) {
	// When opening the screen lookup does not have a value
	// so undefined is the same as false
	const l = get(lookup, 'remote');
	if (l === undefined || l === false) {
		return false;
	}

	const max = required.length;
	let v;
	for (let k = 0; k < max; k++) {
		v = get(lookup, required[k]);
		if (v !== undefined && v.length) {
			return false;
		}
	}
	return true;
}

