#! /bin/bash

set -e
SSLPATH=etc/nginx/ssl

CA_SUBJ='/C=NL/O=Devlab/OU=CA/CN=Zaaksysteem Ontwikkeling CA'
CERT_SUBJ='/C=NL/O=Devlab/OU=Dev/CN=development.zaaksysteem.nl'

if [ -e ${SSLPATH}/ca.key -a -e ${SSLPATH}/ca.crt ]; then
    echo "CA key already exists. Not rebuilding."
else
    openssl genrsa -out ${SSLPATH}/ca.key 2048
    openssl req -new -x509 -days 3650 -sha256 -key ${SSLPATH}/ca.key -out ${SSLPATH}/ca.crt -subj "${CA_SUBJ}"

    echo "CA created. Import ${SSLPATH}/ca.crt if you don't like warnings."
fi

if [ -e ${SSLPATH}/server.key -a -e ${SSLPATH}/server.crt ]; then
    echo "Server key already exists. Not rebuilding."
else
    openssl genrsa -out ${SSLPATH}/server.key 2048
    openssl req -new -subj "${CERT_SUBJ}" -key ${SSLPATH}/server.key -out ${SSLPATH}/server.csr
    openssl x509 -req -days 3650 -sha256 -in ${SSLPATH}/server.csr -CA ${SSLPATH}/ca.crt -CAkey ${SSLPATH}/ca.key -set_serial "${RANDOM}" -out ${SSLPATH}/server.crt

    echo "Server certificate created: ${SSLPATH}/server.crt"
fi

cd $SSLPATH
c_rehash .
