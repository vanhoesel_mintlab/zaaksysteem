package Zaaksysteem::Controller::Filestore;

use Moose;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Filestore - Zaaksysteem Filestore API

=head1 SYNOPSIS

 # /filestore/upload

=head1 DESCRIPTION

Interacts with our filestore

=head1 API

=head2 /filestore/upload [POST]

    {
       "next" : null,
       "status_code" : "200",
       "prev" : null,
       "num_rows" : 1,
       "rows" : 1,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "date_created" : null,
             "original_name" : "useravatar.png",
             "uuid" : "327a0482-aea6-4f58-b7b2-91427535c0fa",
             "size" : 2996,
             "id" : 5,
             "thumbnail_uuid" : null,
             "mimetype" : "image/png",
             "md5" : "d43338446c75425cbcb9fc2ef2bd763c"
          }
       ]
    }

Uploads a file in our filestore, and returns the json data for the result.

Make sure the file gets uploaded in a C<POST> request, and the input name for
this file is embedded in the C<POST> variable C<file>

=cut

sub upload
    : Chained('/')
    : PathPart('filestore/upload')
    : Args(0)
    : ZAPI
{
    my ($self, $c)  = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my $upload          = $c->req->upload('file');

    $c->stash->{zapi}   = $c->model('DB')
                        ->resultset('Filestore')
                        ->filestore_create(
                            {
                                original_name       => $upload->filename,
                                file_path           => $upload->tempname,
                            }
                        );
}

# sub uploadtest : Local {
#     my ($self, $c) = @_;

#     $c->stash->{current_view} = 'TT';
#     $c->stash->{template} = 'uploadtest.tt';
# }
#
# Insert this html in uploadtest.tt
#
# <form method='POST' enctype='multipart/form-data' action="/filestore/upload">
# File to upload: <input type=file name=file><br>
# Notes about the file: <input type=text name=note><br>
# <br>
# <input type=submit value=Press> to upload the file!
# </form>

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
