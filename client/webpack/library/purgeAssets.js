const fsExtra = require('fs-extra');
const rimraf = require('rimraf');
const { cyan, red, yellow } = require('chalk');
const { join, resolve } = require('path');
const { PUBLIC_PATH, ROOT, VENDOR } = require('./constants');

function catchError(error, message) {
  console.error(`${red('Fatal error:')} ${error.message}`);

  if (message) {
    console.info(message);
  }

  process.exit(1);
};

try {
  rimraf.sync(`${ROOT}/${PUBLIC_PATH}/**/*`);
} catch (error) {
  catchError(error);
}

try {
  fsExtra
    .copySync(
      resolve('webpack', 'distribution', VENDOR),
      join(ROOT, PUBLIC_PATH, VENDOR)
    );
} catch (error) {
  catchError(error, `Did you build the ${cyan('vendors')} bundle ($ ${yellow('npm run build-vendors')})?`);
}
