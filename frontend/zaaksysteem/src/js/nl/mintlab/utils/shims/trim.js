/*global define*/
(function ( ) {
	
window.zsDefine('nl.mintlab.utils.shims.trim', function ( ) {
		
		if(String.prototype.trim !== undefined) {
			return function ( string ) {
				return string && typeof string === 'string' ? string.trim() : undefined;
			};
		} else {
			return function ( string ) {
				return string && typeof	string === 'string' ? string.replace(/^\s+|\s+$/g,'') : undefined;
			};
		}
		
	});
	
})();
