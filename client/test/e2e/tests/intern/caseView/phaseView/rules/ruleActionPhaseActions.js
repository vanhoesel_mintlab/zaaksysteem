import navigate from './../../../../../functions/common/navigate';
import inputDate from './../../../../../functions/common/input/inputDate';
import {
    advance
} from './../../../../../functions/intern/caseView/casePhase';
import {
    getAboutValue
} from './../../../../../functions/intern/caseView/caseMenu';

const choice = $('[data-name="fase_actie_keuze"]');
const date = $('[data-name="fase_actie_datum"]');
const phaseTemplate = $('[action-type="template"]');
const phaseEmail = $('[action-type="email"]');
const phaseCase = $('[action-type="case"]');
const phaseAllocation = $('[action-type="allocation"]');
const phaseSubject = $('[action-type="subject"]');

describe('when opening case 67 with the phase actions', () => {

    beforeAll(() => {

        navigate.as('admin', 67);

    });

    describe('and when manually disabling the template and activating the rule', () => {

        beforeAll(() => {

            phaseTemplate.$('input').click();

            choice.$('[value="Template"]').click();

        });

        it('the template checkbox should not be checked', () => {

            expect(phaseTemplate.$('input[checked="checked"]').isPresent()).toBe(false);

        });

        describe('and when unlocking the template settings', () => {

            beforeAll(() => {

                phaseTemplate.$('.sidebar-item-tainted').click();

            });

            it('the template checkbox should be checked', () => {

                expect(phaseTemplate.$('input[checked="checked"]').isPresent()).toBe(true);

            });

        });

    });

    describe('and when manually disabling the email and activating the rule', () => {

        beforeAll(() => {

            phaseEmail.$('input').click();

            choice.$('[value="Email"]').click();

        });

        it('the email checkbox should not be checked', () => {

            expect(phaseEmail.$('input[checked="checked"]').isPresent()).toBe(false);

        });

        describe('and when unlocking the email settings', () => {

            beforeAll(() => {

                phaseEmail.$('.sidebar-item-tainted').click();

            });

            it('the email checkbox should be checked', () => {

                expect(phaseEmail.$('input[checked="checked"]').isPresent()).toBe(true);

            });

        });

    });

    describe('and when manually disabling the case and activating the rule', () => {

        beforeAll(() => {

            phaseCase.$('input').click();

            choice.$('[value="Case"]').click();

        });

        it('the case checkbox should not be checked', () => {

            expect(phaseCase.$('input[checked="checked"]').isPresent()).toBe(false);

        });

        describe('and when unlocking the case settings', () => {

            beforeAll(() => {

                phaseCase.$('.sidebar-item-tainted').click();

            });

            it('the case checkbox should be checked', () => {

                expect(phaseCase.$('input[checked="checked"]').isPresent()).toBe(true);

            });

        });

    });

    describe('and when manually disabling the allocation and activating the rule', () => {

        beforeAll(() => {

            phaseAllocation.$('input').click();

            choice.$('[value="Allocation"]').click();

        });

        it('the allocation checkbox should not be checked', () => {

            expect(phaseAllocation.$('input[checked="checked"]').isPresent()).toBe(false);

        });

        describe('and when unlocking the allocation settings', () => {

            beforeAll(() => {

                phaseAllocation.$('.sidebar-item-tainted').click();

            });

            it('the allocation checkbox should be checked', () => {

                expect(phaseAllocation.$('input[checked="checked"]').isPresent()).toBe(true);

            });

            it('the allocation should be changed to the frontoffice department', () => {

                expect(phaseAllocation.$('.sidebar-item-title').getText()).toEqual('Frontoffice, Behandelaar');

            });

        });

    });

    describe('and when manually disabling the contact relating and activating the rule', () => {

        beforeAll(() => {

            phaseSubject.$('input').click();

            choice.$('[value="Contact"]').click();

        });

        it('the contact checkbox should not be checked', () => {

            expect(phaseSubject.$('input[checked="checked"]').isPresent()).toBe(false);

        });

        describe('and when unlocking the subject settings', () => {

            beforeAll(() => {

                phaseSubject.$('.sidebar-item-tainted').click();

            });

            it('the subject checkbox should be checked', () => {

                expect(phaseSubject.$('input[checked="checked"]').isPresent()).toBe(true);

            });

        });

    });

    describe('and when executing the change date of registration rule', () => {

        beforeAll(() => {

            phaseAllocation.$('[checked="checked"]').click();

            inputDate(date, '01-01-2017');

            choice.$('[value="Change date of registration"]').click();

            advance();

        });

        it('the date of registration and target date should be updated', () => {

            expect(getAboutValue('Registratiedatum')).toEqual('01-01-2017');
            expect(getAboutValue('Streefafhandeldatum')).toEqual('02-01-2017');

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
