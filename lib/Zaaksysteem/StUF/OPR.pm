package Zaaksysteem::StUF::OPR;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'wpl.identificatie'                             => 'woonplaats_identificatie',
    'wpl.woonplaatsNaam'                            => 'woonplaats_naam',

    'identificatie'                                 => 'openbareruimte_identificatie',
    'gor.openbareRuimteNaam'                        => 'openbareruimte_naam',
    'ingangsdatumObject'                            => 'openbareruimte_begindatum'
};

=head2 METHODS

=head2 get_params_for_r02

Gets a set of params for manipulating natuurlijk_persoon

=cut


sub get_params_for_opr {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{OPR};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my $object_key  = $key;
        my $object_value;

        if ($object_key =~ /^extra\./) {
            $object_key =~ s/^extra\.//;

            next unless exists($object_params->{ 'extraElementen' }-> { $object_key });
            $object_value = $object_params->{ 'extraElementen' }-> { $object_key };
        } else {
            next unless exists($object_params->{ $object_key });

            $object_value = $object_params->{ $object_key };
        }

        $params->{ADDRESS_MAP->{$key}} = $object_value;
    }

    if ($params->{openbareruimte_begindatum} && $params->{openbareruimte_begindatum} =~ /^\d{8}$/) {
        $params->{openbareruimte_begindatum} =~ s/^(\d{4})(\d{2})(\d{2})$/$1-$2-$3/;
    }

    return $params;
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_params_for_opr

TODO: Fix the POD

=cut

