import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './index.html';
import resourceModule from '../../../shared/api/resource';
import seamlessImmutable from 'seamless-immutable';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import externalSearchServiceModule from '../../../shared/ui/zsSpotEnlighter/externalSearchService';
import externaldocumentViewModule from './externaldocumentView';
import get from 'lodash/get';
import find from 'lodash/find';

export default angular
	.module('Zaaksysteem.intern.externaldocument.route', [
		angularUiRouterModule,
		resourceModule,
		snackbarServiceModule,
		externalSearchServiceModule,
		externaldocumentViewModule
	])
	.config([
		'$stateProvider', '$urlMatcherFactoryProvider',
		( $stateProvider, $urlMatcherFactoryProvider ) => {
			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('externaldocument', {
					url: '/externaldocument/:next2knowSearchIndex/:next2knowdocumentId',
					template,
					controllerAs: 'vm',
					controller: [
						'$scope', 'externalDocument', 'externalDocumentThumb', 'externalRelatedSearchResults',
						( $scope, externalDocument, externalDocumentThumb, externalRelatedSearchResults ) => {
							$scope.document = externalDocument.data();
							$scope.thumbnail = externalDocumentThumb.data();
							$scope.relatedDocuments = externalRelatedSearchResults.data();
						}],
					resolve: {
						externalDocument: [
							'$rootScope', '$stateParams', 'resource', '$q', 'externalSearchService', 'snackbarService',
							( $rootScope, $stateParams, resource, $q, externalSearchService, snackbarService ) => {
								const externalDocumentResource = resource(
									() =>
										externalSearchService
											.getDocumentRequestOptions(
												$stateParams.next2knowSearchIndex,
												$stateParams.next2knowdocumentId
											),
									{
										scope: $rootScope,
										cache: {
											disabled: true
										}
									}
								)
									.reduce(
										( requestOptions, data ) =>
											get(data, 'data') || seamlessImmutable([])
									);

								return externalDocumentResource
									.asPromise()
									.then(() => externalDocumentResource)
									.catch(err => {
										snackbarService.error('Het externe document kon niet geladen worden.');

										return $q.reject(err);
									});
							}],
						externalDocumentThumb: [
							'$rootScope', '$stateParams', 'resource', '$q', 'externalDocument', 'externalSearchService', 'snackbarService',
							( $rootScope, $stateParams, resource, $q, externalDocument, externalSearchService, snackbarService ) => {
								const externalDocumentThumbResource = resource(
									() =>
										externalSearchService
											.getDocumentThumbRequestOptions(
												$stateParams.next2knowSearchIndex,
												$stateParams.next2knowdocumentId
											),
									{
										scope: $rootScope,
										cache: {
											disabled: true
										}
									}
								)
									.reduce(( requestOptions, data ) => {
										return data !== null ?
											`data:image/png;base64,${get(data, 'data')}`
											: null;
									});

								return externalDocumentThumbResource
									.asPromise()
									.then(() => externalDocumentThumbResource)
									.catch(err => {
										snackbarService.error('De thumbnail van dit document kon niet geladen worden.');

										return $q.reject(err);
									});
							}],
						externalRelatedSearchResults: [
							'$rootScope', '$stateParams', 'resource', '$q', 'externalDocument', 'externalSearchService', 'snackbarService',
							( $rootScope, $stateParams, resource, $q, externalDocument, externalSearchService, snackbarService ) => {
								let externalRelatedSearchResultsResource = resource(
									() => {
										const dossierIdentifier = get(
											find(
												externalDocument.data(),
												field => (
													(field.name === 'Externe_identificatiekenmerken_nummer_binnen_systeem')
													&& (field.value !== '')
												)
											),
											'value'
										);

										return dossierIdentifier ?
											externalSearchService.getRequestOptions(
												dossierIdentifier,
												$stateParams.next2knowSearchIndex
											)
											: null;
									},
									{
										scope: $rootScope,
										cache: {
											disabled: true
										}
									}
								)
									.reduce(( requestOptions, data ) => {
										return get(data, 'data.hits') || seamlessImmutable([]);
									});

								return externalRelatedSearchResultsResource
									.asPromise()
									.then(() => externalRelatedSearchResultsResource)
									.catch(err => {
										snackbarService.error('De gerelateerde documenten konden niet geladen worden.');

										return $q.reject(err);
									});
							}]
					},
					title: [() => 'Extern zoekresultaat']
				});
		}
	])
	.name;
