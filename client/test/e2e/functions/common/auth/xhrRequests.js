export const getUsername = () =>

    browser
        .driver
        .executeAsyncScript(function() {
            const callback = arguments[arguments.length - 1];
            const xhr = new XMLHttpRequest();

            xhr.open('GET', '/api/v1/session/current', true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    callback(xhr.responseText);
                }
            };
            xhr.send('');
        })
        .then(str => {
            return JSON
                .parse(str)
                .result
                .instance
                .logged_in_user === null ?
                    undefined
                    : JSON.parse(str).result.instance.logged_in_user.display_name.toLowerCase();
        });

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
