package Zaaksysteem::Controller::Search::Export;

use Moose;

use File::stat;
use HTTP::Request::Common;
use Encode qw(encode);

use Zaaksysteem::Constants;

BEGIN { extends 'Zaaksysteem::Controller' }

sub export_by_zql :Chained("/search/base") :PathPart("export/zql") :Args() {
    my ($self, $c, $filetype) = @_;

    my $params = $c->req->params();

    $self->_document($c, {filetype => $filetype, query => $params->{zql}});
}

# ---- end of controllers ------- #

Params::Profile->register_profile(
    method  => '_document',
    profile => {
        required => [ qw/filetype/ ],
        optional => [ qw/query/ ]
    }
);

sub _document : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _document" unless $dv->success;

    throw('export/zql_only_documents', 'Only documents can be exported with ZQL')
        if ($params->{filetype} ne 'documents');

    my ($resultset, $display_fields);

    my $object_rs = $c->model('Object')->zql_search($params->{query})->rs->search(
        undef,
        { select => 'object_id', group_by => 'object_id' }
    );

    $resultset = $c->model('DB::Zaak')->search(
        {
            'me.id' => { -in => $object_rs->get_column('object_id')->as_query },
        },
    );

    my @results = ();
    my @header_row = qw/Bestandsnaam Bestandstype Aangemaakt Reden Status Documentstatus ZaakID Versie/;
    push @results, \@header_row;

    while (my $zaak = $resultset->next) {
        push @results, $zaak->export_files;
    }

    $self->_render_file($c, {
        results => \@results,
    });
}


Params::Profile->register_profile(
    method  => '_render_file',
    profile => {
        required => [ qw/results/ ]
    }
);

sub _render_file : Private {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _render_file" unless $dv->success;

    my $results = $params->{results};

    # generate a csv string
    $c->stash->{csv} = { data => $results };

    my $csv = $c->view('CSV')->render($c, $c->stash);
    $csv = encode('UTF-8', $csv);

    # cough up a file
    $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
    $c->res->headers->header(
        'Content-Disposition'  =>
            "attachment;filename=zaaksysteem-" . time() . ".csv" 
    );

    $c->res->body($csv);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 export_by_zql

TODO: Fix the POD

=cut
