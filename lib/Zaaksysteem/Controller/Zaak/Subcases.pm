package Zaaksysteem::Controller::Zaak::Subcases;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant SUBCASE_FIELDS => [qw/eigenaar_type kopieren_kenmerken ou_id role_id required/];

=head2 start_subcase

Start a new subcase.

=cut

sub start_subcase : Chained('/zaak/base') : PathPart('start_subcase') : Args(2) {
    my ($self, $c, $zaaktype_relatie_id, $status) = @_;

    tombstone('20170209', 'rudolf');

    my $params = $c->req->params();

    unless ($params->{update}) {
        $c->stash->{action} = '/zaak/' . $c->stash->{zaak}->nr .'/start_subcase/' . $zaaktype_relatie_id . '/' . $status . '/';
        $c->detach('edit');
    }


    my $zaaktype_relatie = $c->model('DB::ZaaktypeRelatie')->find($zaaktype_relatie_id);
    die "couldn't load zaaktype_relatie using id $zaaktype_relatie_id" unless($zaaktype_relatie);

    my $args = {
        aanvrager_type                  => $params->{relaties_eigenaar_type},
        actie_automatisch_behandelen    => $params->{relaties_automatisch_behandelen},
        actie_kopieren_kenmerken        => $params->{relaties_kopieren_kenmerken},
        ou_id                           => $params->{relaties_ou_id},
        role_id                         => $params->{relaties_role_id},
        type_zaak                       => $zaaktype_relatie->relatie_type || 'deelzaak',
        zaaktype_id                     => $zaaktype_relatie->relatie_zaaktype_id->id,
    };

    my $extra_zaak;

    eval {
        $extra_zaak = $c->model('DB::Zaak')->create_relatie(
            $c->stash->{zaak},
            %$args
        );
    };

    if ($@) {
        my $msg = 'Zaakrelatie kon niet worden aangemaakt';

        if ($@ =~ /create_relatie: need zaak->behandelaar_object/) {
            $msg .= ': huidige zaak heeft geen behandelaar';
        }

        $msg .= '.';

        $c->push_flash_message($msg);
        $c->res->redirect('/zaak/' . $c->stash->{zaak}->id);
        $c->detach;
    }

    my $current_zaak    = $c->stash->{zaak};
    $c->stash->{zaak}   = $extra_zaak;

    delete($c->session->{_zaak_create});

    $c->forward('/zaak/handle_fase_acties', []);

    my $subcase         = $c->stash->{zaak};
    $c->stash->{zaak}   = $current_zaak;


    if($params->{relaties_required}) {
        $current_zaak->register_required_subcase({
            subcase_id              => $subcase->id,
            required                => $params->{relaties_required},
            parent_advance_results  => $params->{relaties_parent_advance_results},
        });
    }

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->id);
}



sub edit : Chained('/zaak/base') : PathPart('subcase') : Args() {
    my ($self, $c, $zaaktype_relatie_id) = @_;

    my $zaaktype_relatie = $c->model('DB::ZaaktypeRelatie')->find($zaaktype_relatie_id);
    die "zaaktype_relatie_id with id $zaaktype_relatie_id not found" unless($zaaktype_relatie);

    my $relatie_params = { $zaaktype_relatie->get_columns };

    $c->stash->{params} = $relatie_params;

    $c->stash->{submit_button} = 'Starten';
    $c->stash->{template}  = 'beheer/zaaktypen/milestones/edit_relatie.tt';
    $c->stash->{nowrapper} = 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 edit

TODO: Fix the POD

=cut

