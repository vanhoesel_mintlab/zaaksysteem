package Zaaksysteem::Object::Queue::Model;

use Moose;

use DateTime;

use BTTW::Tools;

with qw[
    MooseX::Log::Log4perl

    Zaaksysteem::Object::Queue::Model::Case
    Zaaksysteem::Object::Queue::Model::Email
    Zaaksysteem::Object::Queue::Model::File
    Zaaksysteem::Object::Queue::Model::Filestore
    Zaaksysteem::Object::Queue::Model::ObjectMutation
    Zaaksysteem::Object::Queue::Model::ObjectSubscription
    Zaaksysteem::Object::Queue::Model::Meta
    Zaaksysteem::Object::Queue::Model::Subject
    Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon
    Zaaksysteem::Object::Queue::Model::SyncObject
];

=head1 NAME

Zaaksysteem::Object::Queue::Model - Interface for processing queue items

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 base_uri

Required L<URI> instance which gives the queue model info about the URI to use.

=cut

has base_uri => (
    is => 'rw',
    isa => 'URI',
    required => 1
);

=head2 table

Required L<DBIx::Class::ResultSet> instance which handles data storage.

=cut

has table => (
    is => 'rw',
    isa => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 object_model

L<Zaaksysteem::Object::Model> instance for handling C<mutate_object> queue
items.

=cut

has object_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Model',
    predicate => 'has_object_model',
    accessor => '_object_model'
);

=head2 subject_model

L<Zaaksysteem::BR::Subject> instance for handling calls to the subject model.

=cut

has subject_model => (
    is => 'rw',
    isa => 'Zaaksysteem::BR::Subject',
    predicate => 'has_subject_model',
    accessor => '_subject_model',
);

=head2 betrokkene_model

L<Zaaksysteem::Betrokkene> instance for handling C<create_case_subcase> queue
items.

=cut

has betrokkene_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Betrokkene',
    predicate => 'has_betrokkene_model',
    accessor => '_betrokkene_model',
);

=head2 subject_table

L<DBIx::Class::ResultSet> instance for handling C<create_case_subcase> queue
items.

=cut

has subject_table => (
    is => 'rw',
    isa => 'DBIx::Class::ResultSet',
    predicate => 'has_subject_table',
    accessor => '_subject_table'
);

=head2 geocoder

L<Zaaksysteem::Geo::Model> instance for queue items that require geocoding
services.

=cut

has geocoder => (
    is => 'rw',
    isa => 'Zaaksysteem::Geo::Model',
    predicate => 'has_geocoder',
);

=head2 schema

An L<Zaaksysteem::Schema> object, not required.

=cut

has schema => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Schema',
    lazy    => 1,
    # Builder and non-requiredness because of prep work for ZS-13618
    # Making it required breaks plenty of Queue things that I don't
    # want to touch right now.
    builder => '_build_schema',
    required => 0,
);

sub _build_schema {
    my $self = shift;
    return $self->table->result_source->schema;
}

=head1 METHODS

=head2 find_subject

Helper method to find a subject based on the ID in the subject table

=cut

sub find_subject {
    my ($self, $id) = @_;
    return $self->subject_table->find($id),
}

=head2 fetch_item

Retrieves an item from the database. The returned row will be selected with
an update lock, so no other processes should interfere.

=cut

sig fetch_item => 'Str';

sub fetch_item {
    my $self = shift;
    my $id = shift;

    return $self->table->find($id, { for => 'update' });
}

=head2 run_item

Convenience wrapper around L</run>. Retrieves a queue item by its C<id> and
locks further updates on the row within the current transaction.

=cut

sig run_item => 'Str';

sub run_item {
    my $self = shift;
    my $id = shift;

    return $self->run($self->fetch_item($id));
}

=head2 mark_item

Convenience method that can mark specific queued items with the specified
status, in a locking fashion to prevent races.

Optionally returns a fresh copy of the item (depends on context of the call).

    # Safely update the status of the specified item and re-fetches the row
    # from db.
    my $item = $model->mark_item($uuid, 'waiting');

    # Also safely updates the status, but won't refetch
    $model->mark_item($uuid, 'failed');

=cut

sig mark_item => 'Str, Str';

sub mark_item {
    my $self = shift;
    my $id = shift;
    my $status = shift;

    my $item = $self->fetch_item($id);

    $item->update({ status => $status });

    # Only re-fetch when required.
    return $item->discard_changes if defined wantarray;
}

=head2 run_pending

This method will query for all pending queue items, and run them sequentially.

Takes an optional L<DateTime> argument that limits results to rows created
before or at the provided timestamp.

=cut

sig run_pending => '?DateTime';

sub run_pending {
    my $self = shift;
    my $since = shift // DateTime->now();

    $since = $self->schema->format_datetime_object($since);

    my $items = $self->table->search(
        {
            status       => 'pending',
            date_created => { '<=' => $since }
        },
        { rows => 200, order_by => [ { -desc => 'priority' }, { '-desc' => 'date_created' } ] }
    );

    for my $id ($items->get_column('id')->all) {
        $self->run_item($id);
    }

    return;
}

=head2 run

=cut

sig run => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run {
    my $self = shift;
    my $item = shift;

    unless ($item->is_ready) {
        throw('queue/run/item_not_ready', sprintf(
            'Unable to run queue item "%s", unexpected status "%s"',
            $item->label,
            $item->status
        ));
    }

    # void context signals we don't care about new state
    $item->update({
        status => 'running',
        date_started => \"statement_timestamp() at time zone 'UTC'" # easy millisec precision
    });

    my $ret;

    if ($self->log->is_trace) {
        $self->log->trace(sprintf(
            'Starting dispatch for queue item "%s"',
            $item->stringify
        ));
    }

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    try {
        my $handler = $self->can($item->type);

        unless ($handler) {
            throw('queue/run/item_type_not_supported', sprintf(
                'Unable to handle item "%s"',
                $item->stringify
            ));
        }

        $handler->($self, $item);

        $item->status('finished');
    } catch {
        my $error = $_;

        $self->log->warn(sprintf(
            'Exception caught in queue item handler for item "%s": %s',
            $item->stringify,
            $error
        ));

        if ($item->get_column('object_id')) {
            $item->object_id->trigger_logging('object/queue/item_failed', {
                data => {
                    item => $item->TO_JSON,
                    item_label => $item->stringify,
                    exception => "$error"
                }
            });
        }

        $item->status('failed');
    } finally {
        $ret = $item->update({
            date_finished => \"statement_timestamp() at time zone 'UTC'"
        })->discard_changes;
    };

    my $end = Zaaksysteem::StatsD->statsd->end('queue.run.' . $item->type . '.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('queue.run.' . $item->type, 1);

    $self->log->info(sprintf("Dispatch duration for %s: %d ms", $item->stringify, $end));

    return $ret;
}

=head2 gc

Garbage collect the queue item table. Cleanup all succesfully executed rows
older than the required L<DateTime> argument.

=cut

sig gc => 'DateTime';

sub gc {
    my $self = shift;
    my $since = shift;

    $self->table->search({
        status => 'finished',
        date_finished => { '<=' => $since }
    })->delete;

    return;
}

=head2 object_model

Read accessor for the C<object_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/object_model_required

Thrown when the object model is not set.

=back

=cut

sub object_model {
    my $self = shift;

    unless ($self->has_object_model) {
        throw(
            'queue/run/object_model_required',
            'A request for an object model was made, but it was not set'
        );
    }

    return $self->_object_model;
}

=head2 subject_model

Read accessor for the C<subject_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/subject_model_required

Thrown when the subject model is not set.

=back

=cut

sub subject_model {
    my $self = shift;

    unless ($self->has_subject_model) {
        throw(
            'queue/run/subject_model_required',
            'A request for a subject model was made, but it was not set'
        );
    }

    return $self->_subject_model;
}

=head2 betrokkene_model

Read accessor for the C<betrokkene_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/betrokkene_model_required

Thrown when the betrokkene model is not set.

=back

=cut

sub betrokkene_model {
    my $self = shift;

    unless ($self->has_betrokkene_model) {
        throw(
            'queue/run/betrokkene_model_required',
            'A request for a betrokkene model was made, but it was not set'
        );
    }

    return $self->_betrokkene_model;
}

=head2 subject_table

Read accessor for the C<subject_table> attribute.

=head3 Exceptions

=over 4

=item queue/run/subject_table_required

Thrown when the subject table is not set.

=back

=cut

sub subject_table {
    my $self = shift;

    unless ($self->has_subject_table) {
        throw(
            'queue/run/subject_table_required',
            'A request for a subject_table was made, but it was not set'
        );
    }

    return $self->_subject_table;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
