package Zaaksysteem::API::v1::Serializer::ReaderRole;

use Moose::Role;

use BTTW::Tools qw[sig];

requires qw[class read];

sig class => '=> Str';
sig read  => '=> Defined';

=head1 NAME

Zaaksysteem::API::v1::Serializer::ReaderRole - Convenience role for simple
reader implementations

=head1 SYNOPSIS

    package Zaaksysteem::API::v1::Serializer::Reader::MyReader;

    use Moose;

    with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

    sub class { 'Zaaksysteem::Zaken::ComponentZaak' }

    sub read {
        my ($class, $serializer, $zaak) = @_;

        ...

        return {
            foo => 'bar'
        };
    }

=head1 DESCRIPTION

This role assists with building reader classes that handle one type
specifically.

The role requires that the methods C<class> and C<read> are implemented by
the consuming class.

The C<class> method must return a value that can be used for is-a predicate
checks.

The C<read> method must return a hashref that can be passed to an encoder.

This role should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements the interface required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    return sub { $class->read(@_) };
}

=head2 to_bool

Helper function for boolean values

=cut

sub to_bool {
    my ($class, $v) = @_;
    return $v ? \1 : \0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
