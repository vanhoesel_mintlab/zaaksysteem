import {
    openCase,
    clickTransitionButton,
    getTransitionButtonText,
    closeCaseView,
    listCases
} from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app and opening a new case and accepting it', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/open');
        openCase(134);
        clickTransitionButton();
    });

    it('should have have a button for completing', () => {
        expect(getTransitionButtonText()).toEqual('afhandelen');
    });

    describe('and when closing the case view', () => {
    
        beforeAll(() => {
            closeCaseView();
        });

        it('the open caseList should contain the case', () => {
            expect(listCases(1)).toContain(134);
        });

        it('the new caseList should not contain the case', () => {
            expect(listCases(2)).not.toContain(134);
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
