package Zaaksysteem::Model::Session::Invitation;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::Session::Invitation - Per-request
L<Zaaksysteem::Session::Invitation::Model> factory.

=head1 SYNOPSIS

    my $model = $c->model('Session::Invitation');

=cut

__PACKAGE__->config(
    class => 'Zaaksysteem::Session::Invitation::Model',
    constructor => 'new'
);

=head1 METHODS

=head2 prepare_arguments

Prepares the instantiation arguments for the model instance.

=cut

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    return {
        store => $c->model('DB::SessionInvitation')->store
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
