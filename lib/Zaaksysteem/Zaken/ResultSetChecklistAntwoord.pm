package Zaaksysteem::Zaken::ResultSetChecklistAntwoord;

use Moose;
use Data::Dumper;

extends 'DBIx::Class::ResultSet';

sub update_checklist {
    my ($self, $options, $args, $zaak) = @_;

    return unless ($options && UNIVERSAL::isa($options, 'HASH'));

    die('No zaak given') unless $zaak;

    my $status_id = $args->{fase_id};

    while (my ($vraag_id, $option) = each %{ $options }) {
        ### Retrieve vraag
        my $vraag = $self->result_source
            ->schema
            ->resultset('ChecklistVraag')
            ->search({
                'id'                => $vraag_id,
                'zaaktype_node_id'  => $zaak->zaaktype_node_id->id
            }
        );

        next unless $vraag->count;

        ## For later reference
        $vraag = $vraag->first;

        ### Check change
        if (
            $self->search({
                vraag_id => $vraag->id,
            })->count
        ) {
            my $antwoord = $self->search({
                vraag_id    => $vraag->id
            })->first;

            if ($antwoord->antwoord ne $option) {
                $antwoord->zaak_id->trigger_logging('case/checklist/item/update', {
                    component => 'checklist',
                    data => {
                        case_id => $antwoord->zaak_id->id,
                        checklist_item_name => $vraag->vraag,
                        checklist_item_id => $vraag->id,
                        checklist_item_value => $option
                    }
                });
            }
        } else {
            $zaak->trigger_logging('case/checklist/item/update', {
                component => 'checklist',
                data => {
                    case_id => $zaak->id,
                    checklist_item_name => $vraag->vraag,
                    checklist_item_id => $vraag->id,
                    checklist_item_value => $option
                }
            });
        }

        ### Delete antwoorden
        $self->search({
            'vraag_id'  => $vraag->id
        })->delete;

        ### Add antwoord
        $self->create({
            'vraag_id'      => $vraag->id,
            'antwoord'      => $option,
        });
    }

    ### Check unlisted items
    my $vragen = $self->result_source
        ->schema
        ->resultset('ChecklistVraag')
        ->search({
        'zaaktype_node_id'      => $zaak->zaaktype_node_id->id,
        'zaaktype_status_id'    => $status_id
    });

    while (my $vraag = $vragen->next) {
        if (
            $options->{$vraag->id} ||
            !$vraag->checklist_antwoords->search({
                zaak_id => $zaak->id
            })->count
        ) {
            next;
        }

        my $antwoorden = $self->search({
            'vraag_id'  => $vraag->id
        });

        if ($antwoorden->count) {
            $antwoorden->delete;

            $self->result_source
                ->schema
                ->resultset('Logging')->add({
                    component    => 'checklist',
                    component_id => $vraag->zaaktype_status_id->id,
                    zaak_id      => $zaak->id,
                    onderwerp    => 'Antwoord voor vraag: "'
                        . substr($vraag->vraag, 0, 150)
                        . '" gewijzigd naar "geen antwoord"'
            });
        }
    }
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 update_checklist

TODO: Fix the POD

=cut
