package Zaaksysteem::Search::ObjectQueryResultSetShim;

use Moose;
use namespace::autoclean;

with 'Zaaksysteem::Interface::Store';

=head1 NAME

Zaaksysteem::Search::ObjectQueryResultSetShim - Glue layer that translates
L<Zaaksysteem::Object::Query> instances to
L<DBIx::Class::ResultSet>-compatible interactions.

=head1 DESCRIPTION

=cut

use BTTW::Tools;

=head1 ATTRIBUTES

=head2 resultset

=cut

has resultset => (
    is => 'rw',
    isa => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 type

L<Zaaksysteem::Object/type>-compatible type specifier for the wrapped
resultset.

=cut

has type => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 inflator

C<CodeRef> that can inflate a given L<DBIx::Class::Row> to a
L<Zaaksysteem::Object> instance.

=cut

has inflator => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    required => 1,
    handles => {
        inflate => 'execute'
    }
);

=head2 deflator

C<CodeRef> that can deflate a given L<Zaaksysteem::Object> to a C<HashRef>
of parameters supported by the configured C<DBIx::Class::ResultSet>.

=cut

has deflator => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    required => 1,
    handles => {
        deflate => 'execute'
    }
);

=head1 METHODS

=head2 search

Implements L<Zaaksysteem::Interface::Store/search>.

=cut

sub search {
    my $self = shift;
    my $query = shift;

    my $rs = $self->resultset->search(
        $self->query_to_sql_abstract($query)
    );

    # Current API requires all results to be returned
    # TODO paging/retrieve on request
    return map { $self->inflate($_) } $rs->all;
}

=head2 create

Implements L<Zaaksysteem::Interface::Store/create>.

=cut

sub create {
    my $self = shift;
    my $entry = shift;

    my $row = $self->resultset->create($self->deflate($entry));

    return $row->id;
}

=head2 retrieve

Implements L<Zaaksysteem::Interface::Store/retrieve>.

=cut

sub retrieve {
    my $self = shift;
    my $id = shift;

    return $self->inflate($self->resultset->find($id));
}

=head2 update

Implements L<Zaaksysteem::Interface::Store/update>.

=cut

sub update {
    my $self = shift;
    my $query = shift;
    my $update = shift;

    my $rs = $self->resultset->search(
        $self->query_to_sql_abstract($query)
    );

    my $keys_rsc = $rs->get_column($rs->result_source->primary_columns);

    $rs->update($self->deflate($update));

    return unless wantarray;
    return $keys_rsc->all;
}

=head2 delete

Implements L<Zaaksysteem::Interface::Store/delete>.

=cut

sub delete {
    my $self = shift;
    my $query = shift;

    my $rs = $self->resultset->search(
        $self->query_to_sql_abstract($query)
    );

    my @keys;

    if (wantarray) {
        @keys = $rs->get_column($rs->result_source->primary_columns)->all;
    }

    $rs->delete;

    return @keys;
}

=head2 exists

Implements L<Zaaksysteem::Interface::Store/exists>.

=cut

sub exists {
    my $self = shift;
    my $query = shift;

    my $rs = $self->resultset->search(
        $self->query_to_sql_abstract($query)
    );

    return $rs->get_colum($rs->result_source->primary_columns)->all;
}

=head2 query_to_sql_abstract

Translates a L<Zaaksysteem::Object::Query> object to
L<SQL::Abstract>-compatible parameters.

    my ($search, $opts) = $rs->query_to_sql_abstract(qb('my_object', { ... }));

=cut

sub query_to_sql_abstract {
    my $self = shift;
    my $query = shift;

    unless ($query->type eq $self->type) {
        throw('object/query/resultset/type_mismatch', sprintf(
            'Cannot query "%s" object on a "%s" resultset',
            $query->type,
            $self->type
        ));
    }

    my $search;
    my $opts;

    if ($query->has_cond) {
        $search = _unfold_query_expr($query->cond);
    }

    if ($query->has_sort) {
        my $order = $query->sort->reverse ? '-desc' : '-asc';

        $opts = {
            order_by => {
                $order => _unfold_query_expr($query->sort->expression)
            }
        };
    }

    return ($search, $opts);
}

sub _unfold_query_expr {
    my $expr = shift;
    my $field_prefix = shift || '';

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Conjunction')) {
        return { -and => [ map { _unfold_query_expr($_) } $expr->all_expressions ] };
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Disjunction')) {
        return { -or => [ map { _unfold_query_expr($_) } $expr->all_expressions ] };
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Comparison')) {
        my %mode_map = (
            equal                 => '=',
            not_equal             => '!=',
            less_than             => '<',
            greater_than          => '>',
            equal_or_less_than    => '<=',
            equal_or_greater_than => '>='
        );

        my ($first, $second, @rest) = $expr->all_expressions;

        unless (defined $first && defined $second) {
            throw('object/query/dbix_shim/empty_comparison', sprintf(
                '"%s": not enough comparison operands',
                $expr->stringify
            ));
        }

        if (scalar @rest) {
            throw('object/query/dbix_shim/unsupported_comparison', sprintf(
                '"%s": too many comparison operands',
                $expr->stringify
            ));
        }

        # Minor optimization of SQL::Abstract generator, use field => { ... }
        # style when we can, otherwise resort to bare SQL.
        if ($first->isa('Zaaksysteem::Object::Query::Expression::Field')) {
            my $fieldname = sprintf('%s%s', $field_prefix, $first->name);

            return {
                $fieldname => {
                    $mode_map{ $expr->mode } => _unfold_query_expr($second)
                }
            };
        }

        # Default case, merge all SQL blobs literally
        return _merge_sql_statements(' ', _merge_sql_exprs(
            _unfold_query_expr($first),
            $mode_map{ $expr->mode },
            _unfold_query_expr($second)
        ));
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Literal')) {
        if ($expr->type eq 'datetime') {
            return \[ "?::timestamp without time zone", $expr->value->iso8601 ];
        }

        if ($expr->type eq 'text' || $expr->type eq 'string') {
            return \[ "?::text", $expr->value ];
        }

        if ($expr->type eq 'object_ref') {
            return \[ "?", $expr->value->id ];
        }

        throw('object/query/dbix_shim/unsupported_literal', sprintf(
            '"%s": literal value not suported',
            $expr->stringify
        ));
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Set')) {
        return [ map { _unfold_query_expr($_) } $expr->all_expressions ];
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::Field')) {
        return \[ sprintf('%s%s', $field_prefix, $expr->name) ];
    }

    if ($expr->isa('Zaaksysteem::Object::Query::Expression::MemberRelation')) {
        if ($expr->expression->isa('Zaaksysteem::Object::Query::Expression::Field')) {
            my $fieldname = sprintf('%s%s', $field_prefix, $expr->expression->name);

            return {
                $fieldname => {
                    -in => _unfold_query_expr($expr->set)
                }
            };
        }

        return _merge_sql_statements(' ', _merge_sql_exprs(
            _unfold_query_expr($expr->expression),
            'IN',
            _merge_sql_statements(', ', _merge_sql_exprs(
                map { _unfold_query_expr($_) } $expr->set->all_expressions
            ))
        ));
    }

    throw('object/query/dbix_shim/unsupported_query', sprintf(
        '"%s": query not supported for automatic SQL::Abstract translation',
        $expr->stringify
    ));
}

sub _unfold_query_sort {
    # TODO implement sort transformation
    return undef;
}

sub _merge_sql_statements {
    my ($separator, $statements, @bind) = @_;

    return \[ sprintf('( %s )', join($separator, @{ $statements })), @bind ];
}

sub _merge_sql_exprs {
    my @statements;
    my @bind;

    for my $literal (@_) {
        if (ref $literal eq 'REF') {
            my ($part_stmt, @part_bind) = @{ ${ $literal } };

            push @statements, $part_stmt;
            push @bind, @part_bind;

            next;
        }

        unless (ref $literal) {
            push @statements, $literal;

            next;
        }

        throw('object/query/dbix_shim/unsupported_literal_sql', sprintf(
            'Cannot merge literal SQL "%s"',
            $literal // '<undefined>'
        ));
    }

    return \@statements, @bind;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
