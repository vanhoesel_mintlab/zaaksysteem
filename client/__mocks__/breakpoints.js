const { readFileSync } = require('fs');
const { join } = require('path');

const filePath = join(process.cwd(), 'src', 'shared', 'styles', '_breakpoints.scss');

module.exports = readFileSync(filePath, 'utf-8');
