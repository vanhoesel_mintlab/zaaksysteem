import {
    createCase
} from './../../../functions/intern/documentintake';
import navigate from './../../../functions/common/navigate';
import {
    setDocumentintakeSettings,
    setDocumentintakeCasedocument,
    goNext
} from './../../../functions/common/form';
import {
    checkDocument,
    checkDocumentSettings,
    getDocumentLabels
} from './../../../functions/intern/caseView/caseDocuments';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';

const testData = [
    {
        type: 'without settings',
        documentName: 'document zaak registreren 1.odt',
        documentNameExpected: 'document zaak registreren 1.odt'
    },
    {
        type: 'with settings',
        documentName: 'document zaak registreren 2.odt',
        setOnForm: false,
        settings: {
            documentName: 'document zaak registreren 3 gewijzigd',
            description: 'description',
            origin: 'Uitgaand',
            originDate: '01-01-2010'
        },
        documentNameExpected: 'document zaak registreren 2.odt'
    },
    {
        // caseNumber: 171,
        type: 'with changing the settings on form',
        documentName: 'document zaak registreren 3.odt',
        setOnForm: true,
        settings: {
            documentName: 'document zaak registreren 3 gewijzigd',
            description: 'description',
            origin: 'Inkomend',
            originDate: '01-01-2010'
        },
        documentNameExpected: 'document zaak registreren 3 gewijzigd.odt'
    },
    {
        // caseNumber: 172,
        type: 'with adding the document as casedocument',
        documentName: 'document zaak registreren 4.odt',
        caseDocument: 'Document',
        documentNameExpected: 'document zaak registreren 4.odt'
    }
];

const newCase = {
    casetype: 'documentintake zaak registreren',
    requestorType: 'natuurlijk_persoon',
    requestor: '123456789'
};

for ( const index in testData ) {

    describe(`when registering a case from the documentintake ${testData[index].type}`, () => {

        beforeAll(() => {

            navigate.as('admin', '/intern');

            navigate.as('admin', '/zaak/intake?scope=documents');

            createCase(testData[index].documentName, newCase);

            if ( testData[index].setOnForm ) {

                setDocumentintakeSettings(testData[index].settings);

            }

            if ( testData[index].caseDocument ) {

                setDocumentintakeCasedocument(testData[index].caseDocument);

            }

            goNext(3);

            // browser.get(`/intern/zaak/${testData[index].caseNumber}`);

            waitForElement('.case-view');

            openTab('docs');

        });

        it('the case should have the document', () => {

            expect(checkDocument(testData[index].documentNameExpected)).toBe(true);

        });

        if ( testData[index].settings ) {

            it('the document should have the settings', () => {

                expect(checkDocumentSettings(testData[index])).toBe(true);

            });
            
        }

        if ( testData[index].caseDocument ) {

            it(`the document should be set as casedocument "${testData[index].caseDocument}"`, () => {

                expect(getDocumentLabels(testData[index].documentName)).toEqual(testData[index].caseDocument);

            });
            
        }

    });
    
}
