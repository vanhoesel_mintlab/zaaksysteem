#!/bin/sh

#
# USAGE: ZAAKSYSTEEM_API_HOST=zaysteem-api.default.svc.cluster.local ./entrypoint.sh
#

if [ ! -e /etc/nginx/ssl/server.crt ]; then
    echo "First run. Generating certificates."

    (
        cd /etc/nginx/ssl
        cat > openssl.cnf <<'EOT'
[ req ]
default_bits            = 2048
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:true
keyUsage = critical,digitalSignature,cRLSign,keyCertSign

[ server_cert ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = critical,CA:false
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth,clientAuth
subjectAltName = @alt_names

[alt_names]
DNS.1 = dev.zaaksysteem.nl
DNS.2 = *.dev.zaaksysteem.nl
EOT

        openssl genrsa -out ca.key 2048
        openssl req -config openssl.cnf -extensions v3_ca -new -x509 -days 3650 -sha256 -key ca.key -out ca.crt -subj "/CN=Zaaksysteem Development CA"

        openssl genrsa -out server.key 2048
        openssl req -config openssl.cnf -extensions server_cert -new -subj "/CN=dev.zaaksysteem.nl" -key server.key -out server.csr
        openssl x509 -extfile openssl.cnf -extensions server_cert -req -days 3650 -sha256 -in server.csr -CA ca.crt -CAkey ca.key -set_serial $(shuf -i 2000-65000 -n1) -out server-only.crt

        cat server-only.crt ca.crt > server.crt

        rm openssl.cnf
    )
fi


cd /etc/nginx
cp zaaksysteem.conf.tpl zaaksysteem.conf

DNS_RESOLVER=$(grep nameserver /etc/resolv.conf| head -n 1| awk '{ print $2; }');

sed -i -e "s/ZAAKSYSTEEM_HOST_API/${ZAAKSYSTEEM_HOST_API-backend}/" zaaksysteem.conf
sed -i -e "s/ZAAKSYSTEEM_HOST_CSV/${ZAAKSYSTEEM_HOST_CSV-api2csv}/" zaaksysteem.conf
sed -i -e "s/ZAAKSYSTEEM_HOST_BBV/${ZAAKSYSTEEM_HOST_BBV-bbvproxy}/" zaaksysteem.conf
sed -i -e "s/DNS_RESOLVER/${DNS_RESOLVER}/" zaaksysteem.conf
sed -i -e "s/CONNECTION_PER_INSTANCE_LIMIT/${CONNECTION_PER_INSTANCE_LIMIT-10}/" zaaksysteem.conf

exec "$@"
