package Zaaksysteem::Controller::API::v1::Dashboard::Widget;

use Moose;
use DateTime;

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID NonEmptyStr Boolean];

use Zaaksysteem::Object::Types::Widget;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

use constant WIDGET_PROFILE => {
        widget              => NonEmptyStr,
        data                => 'HashRef',
        row                 => 'Int',
        column              => 'Int',
        size_x              => 'Int',
        size_y              => 'Int',
};

=head1 NAME

Zaaksysteem::Controller::API::v1::Dashboard - APIv1 controller for Dashboard objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/dashboard>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Dashboard::Widget>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Dashboard::Widget>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/dashboard/base') : PathPart('widget') : CaptureArgs(0) : Scope('widget') {
    my ($self, $c)      = @_;

    $self->get_all_widgets_for_user($c);
}

=head2 get_all_widgets_for_user

Sets the necessary stash variables containing all widgets from the database

=cut

sub get_all_widgets_for_user {
    my ($self, $c)      = @_;

    my $zql             = 'SELECT {} FROM widget WHERE subject_id = ' . $c->user->id;
    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        );
    } catch {
        $c->log->warn($_);

        if (eval { $_->isa('Throwable::Error') }) {
            $_->throw();
        }
        else {
            throw(
                'api/v1/widgets/unknown_error',
                'API error, impossible to continue'
            );
        }
    };

    $c->stash->{ set }             = $set;
    $c->stash->{ widgets }         = $set->build_iterator->rs;

}

sub _is_initial_login {
    my ($self, $c) = @_;
    if (!$c->stash->{widgets}->search_rs->first && !exists $c->user->properties->{default_dashboard}) {
        return 1;
    }
    return 0;
}

=head2 list

=head3 URL Path

C</api/v1/dashboard/widget>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $self->get_all_widgets_for_user($c);
    if ($self->_is_initial_login($c)) {
        $self->_set_default_widgets_for_behandelaar($c);
    }
    $c->stash->{ result }  = $c->stash->{ set }->init_paging($c->request);
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ widget } = try {
        return $c->stash->{ widgets }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/dashboard/widget/not_found', sprintf(
            "The widget object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ widget } && $c->stash->{ widget }->object_class eq 'widget') {
        throw('api/v1/dashboard/widget/not_found', sprintf(
            "The widget object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 get

=head3 URL Path

C</api/v1/dashboard/widget/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ widget };
}


=head2 create

=head3 URL Path

C</api/v1/dashboard/widget/create>

=cut

define_profile _create_widget => (
    required => {
        %{ WIDGET_PROFILE() }
    },
    optional => {},
);

sub _create_widget {
    my ($self, $c, $behandelaar, $params) = @_;

    $params = assert_profile($params)->valid;
    return try {
        $c->model('DB')->txn_do(
            sub {
                my $object = $c->model('Object')->save_object(
                    object => Zaaksysteem::Object::Types::Widget->new(
                        %$params, subject_id => $c->user->id
                    )
                );
                $object->permit($behandelaar, qw/read write/);
                return $object;
            }
        );
    }
    catch {
        throw(
            'api/v1/dashboard/widget/fault',
            "There was a problem creating this widget: $_"
        );
    };
}

sub _get_behandelaar {
    my ($self, $c) = @_;
    my ($behandelaar)   = grep (
        { $_->system_role && $_->name eq 'Behandelaar' }
        @{ $c->model('DB::Roles')->get_all_cached($c->stash) }
    );
    return $behandelaar;
}


sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    my $behandelaar = $self->_get_behandelaar($c);
    $self->_create_widget($c, $behandelaar, $c->req->params);
    $c->forward('list');
}

=head2 update

=head3 URL Path

C</api/v1/dashboard/widget/UUID/update>

=cut

define_profile update => (
    required => {},
    optional => {
        %{ WIDGET_PROFILE() }
    },
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    my $params          = assert_profile($c->req->params)->valid;

    $self->update_widget($c, $c->stash->{widget}, $params);

    $self->get_all_widgets_for_user($c);
    $c->stash->{ result }   = $c->stash->{ set }->init_paging($c->request);
}

=head2 update_widget

Arguments: $c, $object_to_update, \%params

Return value: $TRUE

Updates the widget with the giver params

=cut

sub update_widget {
    my ($self, $c, $object, $params) = @_;

    eval {
        $c->model('DB')->txn_do(
            sub {
                my $widget = $c->model('Object')->retrieve(uuid => $object->id);

                for my $param (grep { exists $params->{$_} } keys %{ WIDGET_PROFILE() }) {
                    my $clearer = '_clear_' . $param;
                    if (!defined $params->{ $param } && $widget->can($clearer)) {
                        $widget->$clearer();
                        next;
                    }

                    $widget->$param($params->{ $param });
                }

                $c->model('Object')->save(object => $widget);
            }
        );
    };

    if ($@) {
        throw(
            'api/v1/dashboard/widget/fault',
            'There was a problem updating this widget: ' . $@
        );
    }

    return 1;
}

=head2 bulk_update

=head3 URL Path

C</api/v1/dashboard/widget/bulk_update>

=cut

define_profile bulk_update => (
    required => {
        uuid    => UUID,
    },
    optional => {
        %{ WIDGET_PROFILE() }
    },
);

sub bulk_update : Chained('base') : PathPart('bulk_update') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    throw('api/v1/dashboard/widget/invalid', 'Need at least the json param "updates" containing an array') unless
        ($c->req->params->{updates} && ref $c->req->params->{updates} eq 'ARRAY');

    ### Pre-assert all profiles
    my @updates;
    for my $rawparams (@{ $c->req->params->{updates} }) {
        my $params = assert_profile($rawparams)->valid;

        push(@updates, $params);
    }

    eval {
        $c->model('DB')->txn_do(
            sub {
                for my $params (@updates) {
                    my $object          = try {
                        return $c->stash->{ widgets }->find($params->{uuid});
                    } catch {
                        $c->log->warn($_);

                        throw('api/v1/dashboard/widget/not_found', sprintf(
                            "The widget object with UUID '%s' could not be found.",
                            $params->{uuid}
                        ), { http_code => 404 });
                    };

                    $self->update_widget($c, $object, $params);
                }
            }
        )
    };

    if ($@) {
        throw(
            'api/v1/dashboard/widget/fault',
            'There was a problem bulk updating these widgets: ' . $@
        );
    }

    $c->forward('list');
}

=head2 delete

=head3 URL Path

C</api/v1/dashboard/widget/UUID/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    my $object          = $c->stash->{widget};
    eval {
        $c->model('DB')->txn_do(
            sub {
                $c->stash->{widget}->delete;
            }
        );
    };

    if ($@) {
        throw(
            'api/v1/dashboard/widget/fault',
            'There was a problem deleting this widget: ' . $@
        );
    }

    $c->forward('list');
}

=head2 set_default

Set default widgets for a user.

=head3 URL Path

C</api/v1/dashboard/widget/set_default>

=cut

sub set_default : Chained('base') : PathPart('set_default') : Args(0) : RW {
    my ($self, $c) = @_;

    # Prevent people from getting the page and resetting their carefully
    # crafted widgets.
    $self->assert_post($c);
    $self->_delete_all_widgets($c);
    $self->_set_default_widgets_for_behandelaar($c);
    $c->forward('list');
}

sub _delete_all_widgets {
    my ($self, $c) = @_;
    my @widgets = $c->stash->{widgets}->search_rs->all();
    foreach (@widgets) {
        $_->delete;
    }
    return 1;
}

sub _set_default_widgets_for_behandelaar {
    my ($self, $c) = @_;

    if ($self->_is_initial_login($c)) {
        my $p = $c->user->properties;
        $p->{default_dashboard} //=1;
        $c->user->update({properties => $p});
    }

    my $behandelaar = $self->_get_behandelaar($c);
    # Assignee intake
    $self->_create_widget($c, $behandelaar, {
        widget => 'search',
        data   => { search_id => 'mine' },
        row    => 0,
        column => 0,
        size_x => 10,
        size_y => 10,
    });
    # Deptartment/role intake
    $self->_create_widget($c, $behandelaar, {
        widget => 'search',
        data   => { search_id => 'intake' },
        row    => 10,
        column => 0,
        size_x => 10,
        size_y => 10,
    });
    # Favorite casetype widget
    $self->_create_widget($c, $behandelaar, {
        widget => 'casetype',
        data   => { },
        row    => 20,
        column => 0,
        size_x => 5,
        size_y => 5,
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
