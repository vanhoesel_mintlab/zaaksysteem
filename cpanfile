requires 'Archive::Tar::Stream';
requires 'Archive::Zip';
requires 'Array::Compare';
requires 'Authen::Passphrase';
requires 'Authen::SASL';
requires 'Cache::Redis';
requires 'Captcha::reCAPTCHA';
requires 'Catalyst::Action::RenderView';
requires 'Catalyst::Action::REST';
requires 'Catalyst::Controller::ActionRole';
requires 'Catalyst::Controller::SOAP';
requires 'Catalyst::Model::Adaptor';
requires 'Catalyst::Model::DBIC::Schema';
requires 'Catalyst::Plugin::Authorization::Roles';
requires 'Catalyst::Plugin::Cache';
requires 'Catalyst::Plugin::ConfigLoader';
requires 'Catalyst::Plugin::CustomErrorMessage';
requires 'Catalyst::Plugin::I18N';
requires 'Catalyst::Plugin::Params::Profile';
requires 'Catalyst::Plugin::Session';
requires 'Catalyst::Plugin::Session::Store::Redis';
requires 'Catalyst::Plugin::Static::Simple';
requires 'Catalyst::Runtime';
requires 'Catalyst::TraitFor::Controller::reCAPTCHA';
requires 'Catalyst::View::Download';
requires 'Catalyst::View::Email';
requires 'Catalyst::View::JSON';
requires 'Catalyst::View::TT';
requires 'CatalystX::RoleApplicator';
requires 'Class::Load';
requires 'Clone';
requires 'Config::Auto';
requires 'Config::General';
requires 'Crypt::OpenSSL::RSA';
requires 'Crypt::SaltedHash';
requires 'Data::Compare';
requires 'Data::FormValidator';
requires 'Data::Serializer';
requires 'Data::UUID';
requires 'DateTime';
requires 'DateTime::Format::DateParse';
requires 'DateTime::Format::Pg';
requires 'DBI';
requires 'DBD::Pg';
requires 'DBIx::Class';
requires 'DBIx::Class::Cursor::Cached';
requires 'DBIx::Class::Helper::ResultSet::SetOperations';
requires 'DBIx::Class::TimeStamp';
requires 'Digest::MD5::File';
requires 'Email::Outlook::Message';
requires 'Email::Valid';
requires 'Exception::Class';
requires 'FCGI';
requires 'FCGI::ProcManager';
requires 'FCGI::ProcManager::MaxRequests';
requires 'File::ArchivableFormats';
requires 'File::BOM';
requires 'File::Copy';
requires 'File::MimeInfo';
requires 'File::UStore';
requires 'Geo::Coder::Google';
requires 'Getopt::Long';
requires 'GnuPG::Interface';
requires 'Hash::Merge::Simple';

requires 'List::MoreUtils';
requires 'List::Util';

# These 2 do the same. We need to pick one.
requires 'HTML::Strip';
requires 'HTML::TagFilter';

requires 'HTML::Tiny';
requires 'HTTP::BrowserDetect' => '>= 1.63';

# This is already pulled in by Data::FormValidator. Use it.
requires 'Image::Size';

requires 'IO::All';
requires 'IO::Socket::SSL';
requires 'JSON::MaybeXS';
requires 'JSON::Path';
requires 'Lingua::Identify';
requires 'Lingua::Stem::Snowball';
requires 'Lingua::StopWords';
requires 'LockFile::Simple';
requires 'Log::Dispatch';
requires 'Log::Log4perl';
requires 'Log::Log4perl::Appender::Fluent';
requires 'Log::Log4perl::Layout::JSON';
requires 'LWP';
requires 'LWP::Protocol::https';
requires 'Mail::DKIM';
requires 'Math::Base36';
requires 'Memory::Usage';
requires 'MIME::Lite';
requires 'MIME::Tools';
requires 'MIME::Types';
requires 'Module::Install';
requires 'Module::Pluggable::Fast';
requires 'Moose';
requires 'MooseX::Log::Log4perl';
requires 'MooseX::NonMoose';
requires 'MooseX::Types';
requires 'MooseX::Types::Common';
requires 'Net::FTPSSL';
requires 'Net::OpenStack::Swift';
requires 'Net::SAML2';
requires 'Net::SCP::Expect';
requires 'Net::Statsd';
requires 'NetAddr::IP';
requires 'Number::Format';
requires 'Number::Phone';
requires 'OpenOffice::OODoc';
requires 'Params::Profile';
requires 'Parse::FixedLength';
requires 'Perl::Version';
requires 'Pod::Usage';
requires 'String::Random';
requires 'String::CamelCase';
requires 'Template';
requires 'Template::Provider::Encoding';
requires 'Term::ReadPassword::Win32';
requires 'Text::CSV';
requires 'Text::CSV_XS';
requires 'Text::Unidecode';
requires 'Unicode::String';
requires 'URI';
requires 'UUID';
requires 'UUID::Tiny';
requires 'WWW::Mechanize';
requires 'WebService::OverheidIO';
requires 'XML::Compile' => '> 1.56';
requires 'XML::Compile::SOAP';
requires 'XML::Compile::SOAP12';
requires 'XML::Compile::SOAP::WSA';
requires 'XML::Compile::WSDL11';
requires 'XML::Dumper';
requires 'XML::Parser';
requires 'XML::Tidy';
requires 'XML::Twig';

# Modules required for our test suite
on 'test' => sub {
    requires 'Mock::Quick';
    requires 'Pod::Elemental';
    requires 'Sub::Override';
    requires 'Test::Class::Moose';
    requires 'Test::Compile';
    requires 'Test::LWP::UserAgent';
    requires 'Test::Mock::One';
    requires 'Test::Pod';
    requires 'Test::Pod::Coverage';
};

# To remove/deprecate. No longer maintained upstream, multiple modules
# doing similar things, etc.
requires 'JSON';     # use JSON::MaybeXS
requires 'JSON::XS'; # use JSON::MaybeXS
requires 'Locales';  # Only used in one place. Hardcode for now.
requires 'Archive::Extract'; # Just use Archive::Zip...
