#!/bin/sh

set -e

ROOT_DIR=$(dirname $0)'/../'

# Check DBIx::Class version
perl -e 'use DBIx::Class 0.08196'

# Ensure we have the expected libraries to build a schema from a live database.
# Hash::Merge pinning is required for DBIx::Class::Schema::Loader, which
# expects old behavior (undef == empty hash) in the merge() function.
cpanm Hash::Merge@0.200 DBIx::Class::Schema::Loader Catalyst::Helper

if [ -z "$1" ]; then
    echo "This script needs a DSN. Please supply it as the first argument."
    echo "Username and password are also accepted, as further arguments."
    exit 1
fi

echo "    Using database '${DB_NAME}'"

${ROOT_DIR}/script/zaaksysteem_create.pl model DB DBIC::Schema Zaaksysteem::Schema \
    create=static \
    result_base_class='Zaaksysteem::Result' \
    overwrite_modifications=1 \
    skip_load_external=1 \
    datetime_timezone='UTC' \
    "$@"

rm ${ROOT_DIR}/lib/Zaaksysteem/Model/DB.pm.new
rm ${ROOT_DIR}/t/model_DB.t
