package Zaaksysteem::Test::Object::Queue::Model;

use URI;

use Zaaksysteem::Test;
use Zaaksysteem::StatsD;

use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model - Test Queue model

=head1 DESCRIPTION

Test queue model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model

=head2 test_run

=cut

sub test_run {
    my $log = {};

    ### Mock
    my $role  = mock_moose_class(
        {
            superclasses => ['Zaaksysteem::Object::Queue::Model'],
            methods => {
                test_handler => sub {
                    $log->{test_handler} = 1;
                }
            },
        },
        {
            table => mock_one(),
            base_uri => URI->new('http://localhost/')
        }
    );

    my $status = 'pending';
    my $args;
    my $item = mock_one(
        data  => { natuurlijk_persoon_id => 44, },
        label => 'Update cases of deceased persons',
        type  => 'test_handler',
        update => sub {
            my $a = shift;
            foreach (keys %$a) {
                $args->{$_} = $a->{$_};
            }
            return mock_one('X-Mock-Strict' => 1, discard_changes => 1);
        },
        status => sub { my $s = shift; $status = $s if defined $s; },
    );

    ok($role->run($item), "Item ran succesful");

    is($status, 'finished', 'Succesfully finished handler');

    cmp_deeply(
        $args,
        {
            date_started => \"statement_timestamp() at time zone 'UTC'",
            date_finished => \"statement_timestamp() at time zone 'UTC'",
            status        => 'running'
        },
        "Item update works"
    );

    ok($log->{test_handler}, 'Succesfully ran "test_handler" queue item');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
