package Zaaksysteem::Controller::API::Appointment;

use Moose;
use namespace::autoclean;

use DateTime::Format::ISO8601;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 index

Base controller for "Appointment" API-calls

=cut

sub base : Chained('/api/base') : PathPart('appointment') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless($c->session->{pip} || $c->session->{_zaak_create}{ztc_aanvrager_id}) {
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/not_logged_in',
            messages => []
        );
        $c->detach;
    }

    $c->stash->{appointment_interface} = $c->model('DB::Interface')->search_active({ module => 'appointment' })->first;

    if (!$c->stash->{appointment_interface}) {
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/interface_not_found',
            messages => []
        );
        $c->detach;
    }
}

sub get_dates : Chained('base') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $params = {
        request_params => {
            product_id  => $c->req->params->{product_id},
            location_id => $c->req->params->{location_id},
        },
    };

    try {
        my $response = $c->stash->{appointment_interface}->process_api_trigger(
            'get_dates',
            $params,
        );
        $c->stash->{zapi} = $response->data;
    } catch {
        $c->log->info("Error getting date list: $_");
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/internal_error',
            messages => []
        );
    };
}

sub get_timeslots : Chained('base') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $date = DateTime::Format::ISO8601->parse_datetime(
        $c->req->params->{date},
    );

    my $params = {
        request_params => {
            date        => $date,
            product_id  => $c->req->params->{product_id} || 0,
            location_id => $c->req->params->{location_id} || 0,
        },
    };

    try {
        my $response = $c->stash->{appointment_interface}->process_api_trigger(
            'get_timeslots',
            $params,
        );

        $c->stash->{zapi} = $response->data;
    } catch {
        $c->log->info("Error getting timeslots: $_");
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/internal_error',
            messages => []
        );
    };
}

sub book_appointment : Chained('base') : Args(0) : DisableACL : ZAPI {
    my ($self, $c) = @_;

    my $requestor_id = 
              $c->session->{pip}{ztc_aanvrager}
           // $c->session->{_zaak_create}{ztc_aanvrager_id};

    my $requestor = $c->model('Betrokkene')->get({ }, $requestor_id);
    my $subject = $c->model('BR::Subject')->find($requestor->uuid);

    my @roles = $c->model('DB::Roles')->search({ system_role => 1, name => 'Behandelaar' });

    my $params = {
        request_params => {
            references => {
                requestor => $subject
            },
            security_entities => \@roles,
            appointment_data => $c->req->params->{appointment},
        },
        object_model => $c->model('Object'),
    };

    try {
        my $appointment = $c->stash->{appointment_interface}->process_api_trigger(
            'book_appointment',
            $params,
        );

        $c->stash->{zapi} = [
            { id => $appointment->id }
        ];
    } catch {
        $c->log->info("Error booking appointment: $_");
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/internal_error',
            messages => []
        );
    };
}

sub delete_appointment : Chained('base') : Args(0) : DisableACL : ZAPI {
    my ($self, $c) = @_;

    try {
        my $params = {
            request_params => {
                appointment => {
                    reference => $c->req->params->{appointment},
                    type      => 'appointment',
                    instance  => undef,
                }
            },
            object_model => $c->model('Object'),
        };

        my $res = $c->stash->{appointment_interface}->process_api_trigger(
            'delete_appointment',
            $params,
        );
        $c->stash->{zapi} = [ { success => \1 } ];
    } catch {
        $c->log->info("Error deleting appointment: $_");
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/internal_error',
            messages => []
        );
    };
}

sub get_appointment : Chained('base') : Args(0) : DisableACL : ZAPI {
    my ($self, $c) = @_;

    try {
        my $ref = $c->model('Object')->inflate(hash => {
            reference => $c->req->params->{appointment_id},
            type      => 'appointment',
            instance  => undef,
        });
        my $appointment = $c->model('Object')->get(ref => $ref);
        
        $c->stash->{zapi} = [
            {
                start_time => $appointment->start_time->set_time_zone('UTC'),
                end_time   => $appointment->end_time->set_time_zone('UTC'),
            }
        ];
    } catch {
        $c->log->info("Error retrieving appointment: $_");
        $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
            type     => 'appointment/internal_error',
            messages => []
        );
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
