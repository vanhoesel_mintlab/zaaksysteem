FROM registry.gitlab.com/zaaksysteem/platform:c0777198

# Please see dev-bin/docker-perl for building and pushing this base
# layer

ENV DEBIAN_FRONTEND=noninteractive \
    # Disable network testing: Test::RequiresInternet
    NO_NETWORK_TESTING=1

# libssl1.0-dev is required, because Crypt::OpenSSL::{X509,RSA,VerifyX509} do not
# support libssl1.1 yet.
# It can be removed once
# https://github.com/dsully/perl-crypt-openssl-x509/issues/53 and related bugs
# for the other projects are fixed.
#
RUN groupadd -g 1001 zaaksysteem \
    && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem

COPY cpanfile /opt/zaaksysteem/cpanfile

RUN apt-get update \
  && apt-get --no-install-recommends -y install \
      libpq-dev \
      uuid-dev \
      locales \
      xmlsec1 \
      unzip \
      libssl1.0-dev \
      libmagic-dev \
      poppler-utils \
  && localedef -i nl_NL -c -f UTF-8 -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
# Class::C3 installed because there is some dependency resolution thing
# with Catalyst::Plugin::Params::Profile in the cpanfile.
  && cpanm Class::C3 \
# List::MoreUtils::XS installed outside of cpanfile due to dependency
# loophole: https://rt.cpan.org/Public/Bug/Display.html?id=122875
# cpanfile deps are not installed in order and running it twice is
# stupid enough, but trice or more would be considered ugly
  && cpanm List::MoreUtils::XS \
# GnuPG::Interface requires patches which are found in the bug report,
# but haven't been merged upstream, so we don't require the testsuite
# right now: https://rt.cpan.org/Public/Bug/Display.html?id=102651
  && cpanm --notest GnuPG::Interface \
  && ( cpanm --installdeps /opt/zaaksysteem || ( cat ~/.cpanm/work/*/build.log && false )) \
  && apt-get purge -yqq libssl1.0-dev libmagic-dev uuid-dev libpq-dev \
  && apt-get --no-install-recommends -y install libpq5 \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm
