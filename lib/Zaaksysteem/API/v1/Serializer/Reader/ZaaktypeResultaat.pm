package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeResultaat;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::Model::DB::ZaaktypeResultaten' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $resultaat) = @_;

    return {
        resultaat_id             => $resultaat->id,
        type                     => $resultaat->resultaat,
        archive_procedure        => scalar($resultaat->ingang),
        period_of_preservation   => scalar($resultaat->bewaartermijn),
        type_of_dossier          => scalar($resultaat->dossiertype),
        label                    => scalar($resultaat->label),
        selection_list           => scalar($resultaat->selectielijst),
        type_of_archiving        => scalar($resultaat->archiefnominatie),
        comments                 => scalar($resultaat->comments),
        external_reference       => scalar($resultaat->external_reference),
        trigger_archival         => scalar($resultaat->trigger_archival),
        selection_list_number    => $resultaat->properties->{selectielijst_nummer},
        process_type_number      => $resultaat->properties->{procestype_nummer},
        process_type_name        => $resultaat->properties->{procestype_naam},
        process_type_description => $resultaat->properties->{procestype_omschrijving},
        process_type_explanation => $resultaat->properties->{procestype_toelichting},
        process_type_generic     => $resultaat->properties->{procestype_generiek},
        process_type_object      => $resultaat->properties->{procestype_object},
        origin                   => $resultaat->properties->{herkomst},
        process_term             => $resultaat->properties->{processtermijn},
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
