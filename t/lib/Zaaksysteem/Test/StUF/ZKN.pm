package Zaaksysteem::Test::StUF::ZKN;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::StUF::ZKN - Test the plugin modules of ZS::StUF::ZKN

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::ZKN

=cut

use UUID::Tiny ':std';
use IO::All;

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Test::XML qw(:all);

use BTTW::Tools qw(dump_terse);
use BTTW::Tools::RandomData qw(generate_bsn);

use Zaaksysteem::XML::Generator::StUF0310;
use Zaaksysteem::StUF::0310::Processor;

sub _get_model {
    my $model = Zaaksysteem::StUF::0310::Processor->new(
        betrokkene_model  => mock_one(),
        object_model      => mock_one(),
        record            => mock_one(),
        schema            => mock_one(),
        interface         => mock_one(),
        municipality_code => '1332',
        @_,
    );
}

sub test_get_natuurlijk_persoon {

    my $model = _get_model();

    my $bsn = generate_bsn();
    lives_ok(
        sub {

            my $override
                = override(
                'Zaaksysteem::StUF::0310::Processor::_assert_stuf_interface'
                    => sub { mock_one(id => 42) });
            $override->replace(
                'Zaaksysteem::StUF::0310::Processor::_find_bsn' =>
                    sub { return });
            $override->replace(
                'Zaaksysteem::BR::Subject::search' => sub { return 1 });
            $override->replace(
                'Zaaksysteem::BR::Subject::remote_import' => sub { return 1 }
            );

            $model->_get_natuurlijk_persoon($bsn);
        },
        '_get_natuurlijk_persoon works fine'
    );

}

sub test_write_case {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $bsn       = generate_bsn();
    my $case      = mock_case();

    my $now  = DateTime->now();
    my $data = {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Lk01',
            entiteittype     => 'ZAK',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
        parameters => {
            mutatiesoort      => 'T',
            indicatorOvername => 'V',
        },
        object => [
            {
                identificatie => { _ => sprintf('%05d', $case->id) },
                omschrijving  => 'Foo',
                startdatum         => { _ => $now->strftime('%Y%m%d') },
                registratiedatum   => { _ => $now->strftime('%Y%m%d') },
                zaakniveau         => 1,
                deelzakenIndicatie => "N",

                isVan => {
                    entiteittype => 'ZAKZKT',
                    gerelateerde => {
                        entiteittype => 'ZKT',
                        omschrijving => 'Onbekend',
                        code         => 100,
                        ingangsdatum => { _ => '20160101' },
                    },
                },
                heeftAlsInitiator => {
                    gerelateerde =>
                        { natuurlijkPersoon => { 'inp.bsn' => $bsn, } },
                },
            },
        ],
    };

    my $method = 'write_case';

    my $xml = $generator->$method(writer => $data,);

    # ZOMG, how many XSD's do we need?
    my $xml_schema = XML::Compile::Schema->new(
        [

            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/0301/stuf0301mtom.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_ent_basis.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_simpleTypes.xsd
                share/wsdl/stuf/bg0310/entiteiten/bg0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/gml/bag-gml.xsd
                share/wsdl/stuf/xlink/xlinks.xsd
                share/wsdl/stuf/xmlmime/xmlmime.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_bg0310_ent.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_ent_basis.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/entiteiten/zkn0310_stuf_simpleTypes.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_ent_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_mutatie.xsd
                share/wsdl/stuf/zkn0310/mutatie/zkn0310_msg_stuf_mutatie.xsd
            )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'zakLk01',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

sub test_creeer_zaak_id {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();
    my $bsn       = generate_bsn();

    my $now  = DateTime->now();
    my $data = {
        stuurgegevens => {
            zender           => { applicatie => "Zaaksysteem", },
            ontvanger        => { applicatie => "Zaaksysteem", },
            berichtcode      => 'Di02',
            functie          => 'genereerZaakidentificatie',
            referentienummer => '',
            tijdstipBericht => $now->strftime('%Y%m%d%H%M%S%03N'),
        },
    };

    my $method = 'generate_case_id';

    my $xml = $generator->$method(writer => $data,);

    # ZOMG, how many XSD's do we need?
    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/wsdl/stuf/0301/stuf0301.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_stuf_zs-dms.xsd
                share/wsdl/stuf/zkn0310/zs-dms/zkn0310_msg_zs-dms.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/sector/zkn/0310',
        type      => 'genereerZaakIdentificatie_Di02',
        testname  => 'Craft creeerZaak_Lk01 message',
    );
}

sub _get_xpath_soap_action_ok {
    my ($model, $xml, $soap_action) = @_;
    my ($xpc, @actions) = $model->get_xpath_and_soap_action($xml);

    is(
        $actions[0],
        $soap_action,
        "SOAP action is correct"
    );

    return $xpc;
}


sub test_write_case_document {

    my $model = _get_model();

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN document-edcLk01.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

    my %args = ();
    my $file = mock_one(
        update_metadata => sub {
            %args = %{ $_[0] };
            return 1;
        }
    );

    my $object = ($xpc->findnodes('ZKN:object'))[-1];
    $model->_update_metadata($xpc, $object, $file);

    cmp_deeply(
        \%args,
        {
            description       => 'StUF-ZKN Document met dingen',
            document_category => 'Aangifte',
        },
        "Metadata is updated correctly with NEN compliant document categories",
    );

    {
        note "NEN non complaincy categories";
        my $xml = io->catfile(
            qw(t data StUF 3.10 ZKN document-edcLk01-nonNEN.xml)
        )->slurp;

        my $xpc = _get_xpath_soap_action_ok($model, $xml,
            '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01');

        my $object = ($xpc->findnodes('ZKN:object'))[-1];
        $model->_update_metadata($xpc, $object, $file);

        cmp_deeply(
            \%args,
            { description => 'StUF-ZKN Document met dingen', },
            "Metadata is updated correctly with NEN non-compliant document categories",
        );
    }
}

sub test_write_case_document_lock {

    my $model = _get_model(
        schema    => mock_one(
            update_file => sub {
                my $args = shift;

                is($args->{is_restore}, 0, 'is_restore flag to update_file is 0');
                is($args->{original_name}, 'bestandsnaam.txt', 'Filename is bestandsnaam.txt');
                is($args->{subject}, 31337, 'subject is passed in');
            },
            as_object => 31337,
        ),
    );

    my $xml = io->catfile(
        qw(t data StUF 3.10 ZKN updateZaakdocument_Di02.xml)
    )->slurp;

    my $xpc = _get_xpath_soap_action_ok($model, $xml,
        '{http://www.egem.nl/StUF/sector/zkn/0310}updateZaakdocument_Di02');

    my $response = $model->write_case_document_lock($xpc);

    my $parsed_response = XML::LibXML->load_xml(string => $response);

    is(
        $parsed_response->documentElement->localname,
        'Bv02Bericht',
        'Return XML root element name is correct'
    );
    is(
        $parsed_response->documentElement->namespaceURI,
        'http://www.egem.nl/StUF/StUF0301',
        'Return XML root element namespace URI is correct'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
