package Zaaksysteem::Auth::Credential::Token;

use Moose;

=head1 NAME

Zaaksysteem::Auth::Credential::Token - Authenticate users using an
OTP/single-use token

=head1 DESCRIPTION

=cut

use BTTW::Tools;

# Constuctor args list->map transformer
around BUILDARGS => sub {
    my ($orig, $class, $config, $app, $realm) = @_;

    return $class->$orig(
        app => $app,
        config => $config,
        realm => $realm
    );
};

=head1 METHODS

=head2 authenticate

Implements L<Catalyst::Plugin::Authentication> interface for authenticating
a user.

=cut

sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $config = assert_profile($realm->config, profile => {
        required => { interface => 'Str' }
    })->valid;

    my $invitation = $c->model('Session::Invitation')->validate($authinfo->{ token });

    unless (defined $invitation) {
        throw('auth/token/token_invalid', 'No token found, or token expired.');
    }

    my $interface = $c->model('DB::Interface')->find_by_module_name(
        $config->{ interface }
    );

    unless (defined $interface) {
        throw('auth/token/interface_not_found', sprintf(
            'Unable to find required interface by module name "%s"',
            $config->{ interface }
        ));
    }

    my $user = $realm->find_user($c, {
        source => $interface->id,
        subject_id => $invitation->subject->id
    });

    unless (defined $user) {
        throw('auth/token/user_not_found', sprintf(
            'Invitation user could not be found by UUID "%s"',
            $invitation->subject->id
        ));
    }

    return $user;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
