import check from 'api-check';

export default check({
	verbose: false,
	disabled: !ENV.IS_DEV,
});
