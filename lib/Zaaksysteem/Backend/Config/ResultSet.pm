package Zaaksysteem::Backend::Config::ResultSet;

use Moose;

with 'MooseX::Log::Log4perl';

BEGIN { extends 'DBIx::Class::ResultSet'; }

use JSON::XS;
use List::Util qw(uniq);

use constant STANDARD_PARAMETERS => qw/
    users_can_change_password
    signature_upload_role
    allocation_notification_template_id
    pdf_annotations_public
    requestor_search_extension_active
    requestor_search_extension_href
    requestor_search_extension_name
/;

use constant ADVANCED_PARAMETERS => qw/
    jodconverter_url
    public_manpage
    enable_stufzkn_simulator
    disable_dashboard_customization
/;

sub _decode_config_item {
    my ($self, $name, $item) = @_;

    return $item if $name ne 'custom_relation_roles';

    my $decoded = JSON::XS->new->utf8(0)->decode($item || '[]');

    # This is processed by "veldoptie.tt", which is very picky about input values
    if (@$decoded) {
        return $decoded;
    }
    else {
        return "";
    }
}

sub get_value {
    my ($self, $parameter) = @_;

    die "need parameter" unless $parameter;

    my $row = $self->search({ parameter => $parameter })->first;

    if ($row) {
        my $val = $row->value;

        return $self->_decode_config_item($parameter, $val);
    }

    return;
}

sub get {
    my $self = shift;
    my $retval = undef;

    eval {
        $retval = $self->get_value(@_);
    };

    return $retval;
}

sub save {
    my ($self, $params, $advanced, $apply_defaults) = @_;

    $apply_defaults //= 1;

    my $config;
    if ($apply_defaults) {
        $config = $self->apply_defaults($params, $advanced);
    }
    else {
        $config = $params;
    }

    while (my ($key, $value) = each %$config) {
        next if $key eq 'advanced'; # advanced thingy needs to go bye bye

        if ($key eq 'custom_relation_roles') {
            if (ref($value) ne 'ARRAY') {
                $value = [$value];
            }

            $value = [ grep { $_ } uniq @$value ];

            $value = JSON::XS->new->utf8(1)->encode($value);
        }

        my $row = $self->find_or_create({parameter => $key, advanced => $advanced});

        if ($row) {
            $row->value($value);
            $row->update;
        }
    }
}

sub get_all {
    my ($self, $advanced) = @_;

    my @settings = $self->search({advanced => $advanced})->all;

    my $config = { map { $_->parameter => $_->value } @settings };

    if (exists $config->{custom_relation_roles}) {
        $config->{custom_relation_roles} = $self->_decode_config_item(
            'custom_relation_roles',
            $config->{custom_relation_roles}
        );
    }

    return $self->apply_defaults($config, $advanced);
}

sub apply_defaults {
    my ($self, $config, $advanced) = @_;

    $config->{$_} ||= '' for ($advanced ? ADVANCED_PARAMETERS : STANDARD_PARAMETERS);

    return $config;
}

=head2 get_customer_config

Convenience method to get all customer_info data from the database.

=cut

sub get_customer_config {
    my $self = shift;

    my $cache = $self->result_source->schema->cache;

    if (exists $cache->{customer_info}) {
        return $cache->{customer_info};
    }

    my $config = $self->search({ parameter => { 'ILIKE' => 'customer_info_%' } });

    while (my $item = $config->next) {
        my $key = $item->parameter;
        $key =~ s/^customer_info_//;
        $cache->{customer_info}{$key} = $item->value;
    }
    $self->result_source->schema->cache($cache);
    return $cache->{customer_info} // {};
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 apply_defaults

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 get_all

TODO: Fix the POD

=cut

=head2 get_value

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

