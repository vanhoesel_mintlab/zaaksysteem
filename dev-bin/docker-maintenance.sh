#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

CURRENT_BACKEND_REVISION=$(head -n1 "${DIR}/docker/Dockerfile.backend" | awk -F: '{print $2}')

echo "Stopping and rebuilding containers"
docker-compose stop
docker-compose pull
docker-compose build --pull

OLD_IMAGES=$(docker images --format '{{.Tag}}' registry.gitlab.com/zaaksysteem/zaaksysteem-perl | grep -v "$CURRENT_REVISION")
if [ -n "$OLD_IMAGES" ]; then
    docker rmi $OLD_IMAGES
fi

docker image prune
