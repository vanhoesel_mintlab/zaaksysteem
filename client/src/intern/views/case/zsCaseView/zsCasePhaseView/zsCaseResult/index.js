import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import capitalize from 'lodash/capitalize';
import shortid from 'shortid';
import './styles.scss';

export default
	angular.module('zsCaseResult', [
		composedReducerModule
	])
		.directive('zsCaseResult', [ 'composedReducer', ( composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					results: '&',
					result: '&',
					onResultChange: '&',
					canChange: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this;

					ctrl.getResultOptions = composedReducer({ scope }, ctrl.result, ctrl.results)
						.reduce( ( result, results ) => {

							return (results || []).map(
								resultOption => {

									let preservation =
											resultOption.period_of_preservation < 365 ?
												`${Math.round(resultOption.period_of_preservation / 30)} maanden`
												: (resultOption.period_of_preservation >= 99999 ?
													'Bewaren'
													: `${Number((Math.floor(resultOption.period_of_preservation / 365 * 2) / 2).toFixed(1))} jaar`),
										help =
											[
												{
													label: 'Resultaattype-generiek',
													value: capitalize(resultOption.type)
												},
												{
													label: 'Selectielijst',
													value: 	resultOption.selection_list
												},
												{
													label: 'Procestype-nummer',
													value: 	resultOption.process_type_number
												},
												{
													label: 'Procestype-naam',
													value: 	resultOption.process_type_name
												},
												{
													label: 'Procestype-object',
													value: 	resultOption.process_type_object
												},
												{
													label: 'Herkomst',
													value: 	resultOption.origin
												},
												{
													label: 'Archiefnominatie (waardering)',
													value: resultOption.type_of_archiving
												},
												{
													label: 'Procestermijn',
													value: resultOption.process_term
												},
												{
													label: 'Bewaartermijn',
													value: preservation
												}
											];

									return {
										id: shortid(),
										value: resultOption.type,
										label: resultOption.label || capitalize(resultOption.type),
										selected: result === resultOption.type,
										help:
											help.map(
												row => `<b>${row.label}</b>: ${row.value || row.value === 0 ? row.value : '-' }`
											).join('<br/>')
									};
								}
							);

						})
						.data;

					ctrl.hasResult = ( ) => !!ctrl.result();

					ctrl.clearResult = ( ) => {
						ctrl.onResultChange( { $result: null });
					};

					ctrl.handleResultChange = ( result ) => {
						ctrl.onResultChange({ $result: result });
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
