package Zaaksysteem::Test::SAML2;

use Zaaksysteem::Test;

use IO::All;
use Zaaksysteem::SAML2;
use Zaaksysteem::SAML2::Protocol::Assertion;

sub test_eherkenning {

    my $saml = Zaaksysteem::SAML2->new();

    isa_ok($saml, "Zaaksysteem::SAML2");

    my %test_files = (
        '1.5'                  => '51902672000021881022',
        '1.7-vestigingsnummer' => '51902672000021881022',
        '1.7'                  => '51902672',
        #'1.9' => '123456789',
    );

    foreach my $name (keys %test_files) {

        my $xml = io->catfile(qw(t data eherkenning),  "$name.xml")->slurp;

        my $assertion = Zaaksysteem::SAML2::Protocol::Assertion->new_from_xml(
            xml => $xml,
        );

        isa_ok($assertion, "Zaaksysteem::SAML2::Protocol::Assertion");


        my $kvk;

        my $ok = lives_ok(
            sub {
                $kvk = $saml->_get_eherkenning_identifier($assertion->attributes);
            },
            "We have a valid $name xml"
        );

        if ($ok) {
            is($kvk, $test_files{$name},
                "Assertion has correct identifier $test_files{$name} for eHerkenning $name"
            );
        }
        else {
            note explain $assertion;
        }

    }

}

1;

__END__

=head1 NAME

Zaaksysteem::Test::SAML2 - Test ZS::SAML2 logic

=head1 DESCRIPTION

Test SAML2 implementations

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::SAML2

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
